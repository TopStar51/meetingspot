<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends Base_Controller {

    public function index()
    {
    	$view_params = array();
    	$view_params['header_data'] = $this->frontend_model->get_header_data('about');
        $this->load_view('about', $view_params);
    }
}