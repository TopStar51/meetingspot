<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends Base_Controller {

    public function index()
    {
    	$view_params['header_data'] = $this->frontend_model->get_header_data('contacts');
    	$this->load_view('contacts', $view_params);
    }
}