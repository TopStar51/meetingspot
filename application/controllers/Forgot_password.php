<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password extends Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	function index() {

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

		if($this->form_validation->run() == FALSE) {

           	$this->load_view('forgot_password');
		} 

		else {
			$resp = array();

			$email = $this->input->post('email');
			$user_info = $this->Base_Model->get_info_with_where('users', array('email'=>$email));
			if(empty($user_info)) {
	            $this->session->set_flashdata('msg','Your email does not exist.');
				$this->load_view('forgot_password');
	        }
	        else if($user_info['status'] == INACTIVE){
	        	$this->session->set_flashdata('msg','Your account was not approved.');
				$this->load_view('forgot_password');
	        } else {

	        	$config = array(
		            'protocol'  => 'smtp',
		            'smtp_host' => 'ssl://smtp.googlemail.com',
		            'smtp_port' => 465,
		            'smtp_user' => 'lazarkolarov@gmail.com',
		            'smtp_pass' => 'passion51',
		            'mailtype'  => 'html',
		            'charset'   => 'utf-8',
		            'wordwrap'  => TRUE
		        );
	        	$message = "
					<html>
					<head>
						<title>Password Reset</title>
					</head>
					<body>
						<h3>Hello, ".$user_info['first_name']."&nbsp;".$user_info['last_name']."</h3>
						<p>We heard you need a password reset.</p>
						<p>Click the link below and you'll be redirected to a secure site from which you can set a new password</p>
						<h4><a href='".site_url()."/reset_password/index/".$user_info['id']."'>Reset Password</a></h4>
					</body>
					</html>
				";
				$this->email->initialize($config);
			    $this->email->set_newline("\r\n");
			    $this->email->from($config['smtp_user'], 'Meetingspot');
			    $this->email->to($email);
			    $this->email->subject('Password Reset Email');
			    $this->email->message($message);

				//sending email
			    if($this->email->send()){
			    	$this->session->set_flashdata('msg','Please confirm your email to reset your password.');
			    }
			    else{
			    	$this->session->set_flashdata('msg', 'Email Server Issue');
			    }
			    
				$this->load_view('forgot_password');	
	        }
		}
		
	}
	
}