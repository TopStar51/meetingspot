<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Base_Controller {

    public function index()
    {
    	$view_params = array();
    	$view_params['categories'] = $this->Base_Model->get_list('categories');
    	$view_params['top_locations'] = $this->frontend_model->get_locations_by_search('', array(),'',3);
    	$view_params['header_data'] = $this->frontend_model->get_header_data('home');
    	$this->load_view('home', $view_params);
    }
}