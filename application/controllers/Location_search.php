<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location_search extends Base_Controller {

	private $location_count = 5;
	private $zoom_val = 10;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('location_model');
	}

	public function index()
	{
		$req = $this->input->get();

		$location_name = '';
		$category_id = '';
		$address_info = array();
		$view_params = array();

		if(!empty($req['location_name'])) 
			$location_name = $req['location_name'];
		if(!empty($req['category_id']))
			$category_id = $req['category_id'];
		if(!empty($req['address'])) {
			$view_params['center_latlng'] = getLatLong($req['address']);
			$view_params['zoom'] = $this->zoom_val;
		}
		else {
			$view_params['center_latlng'] = array(
				'latitude' => DEF_CENTER_LAT,
				'longitude' => DEF_CENTER_LON
			);
			$view_params['zoom'] = DEF_ZOOM;
		}

		if(!empty($req['city'])) $address_info['city'] = $req['city'];
		if(!empty($req['state'])) $address_info['state'] = $req['state'];
		if(!empty($req['zip'])) $address_info['zip'] = $req['zip'];

		$view_params['locations'] = $this->frontend_model->get_locations_by_search($location_name, $address_info, $category_id);
		$this->load_view('location_search', $view_params);
	}
}
