<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location_single extends Base_Controller {

	private $show_review_limit = 3;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('location_model');
		$this->load->model('reservation_model');
	}
	public function index($location_id)
	{
		$view_params['location_info'] = $this->location_model->get_detail($location_id);
		$view_params['working_hours'] = $this->Base_Model->get_list('working_hours', 0, array('location_id' => $location_id)); 
		$view_params['weekdays'] = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
		$view_params['reviews'] = $this->frontend_model->get_reviews_by_location($location_id, $this->show_review_limit);

		$view_params['review_count'] = $this->Base_Model->get_count_value('reviews', array('location_id'=>$location_id, 'status'=>ACTIVE));
		if($view_params['review_count'] > 0) 
			$view_params['review_avg'] = round($this->Base_Model->get_sum_value('reviews', 'rating', array('location_id'=>$location_id, 'status'=>ACTIVE))/$view_params['review_count']);
		else
			$view_params['review_avg'] = 0;
		$view_params['room_count'] = $this->Base_Model->get_count_value('rooms', array('location_id'=>$location_id, 'status'=>ACTIVE));
		$view_params['rooms'] = $this->Base_Model->get_list('rooms', 0, array('location_id'=>$location_id, 'status'=>ACTIVE));

		$view_params['reservations'] = $this->frontend_model->get_upcoming_reservations($location_id);

		$view_params['groups'] = $this->Base_Model->get_list('groups', 0, array('user_id'=>$this->user_id));
		$this->load_view('location_single', $view_params);
	}
}
