<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('customer_model');
		$this->load->library('form_validation');
	}

	function index() {

		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required');
        $this->form_validation->set_rules('address', 'Address1', 'trim|required');
        //$this->form_validation->set_rules('address2', 'Address2', 'trim|required');
        $this->form_validation->set_rules('city', 'City', 'trim|required');
        $this->form_validation->set_rules('state', 'State', 'trim|required');
        $this->form_validation->set_rules('zip', 'Zip', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
		$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'required|matches[password]');
		if($this->form_validation->run() == FALSE) {

            $this->load_view('register');
		} 
		else {

			$req = $this->input->post();

			$email = $req['email'];
			$username = $req['username'];
			$password = $req['password'];

			unset($req['password_confirm']);
			$req['password'] = md5($req['password']);

			$where = "( username='".$username."' OR email='".$email."' )";
			$user_info = $this->Base_Model->get_info_with_where('users', $where);

			if(!empty($user_info)) {
				$this->session->set_flashdata('msg','Your username or email already taken');
				$this->load_view('register');
				return;
			}

			//generate simple random code
			$set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$code = substr(str_shuffle($set), 0, 12);

			$req['activation_code'] = $code;
			$user_id = $this->Base_Model->save_info('users', $req);

			$config = array(
	            'protocol'  => 'smtp',
	            'smtp_host' => 'ssl://smtp.googlemail.com',
	            'smtp_port' => 465,
	            'smtp_user' => SMTP_USER,
	            'smtp_pass' => SMTP_PASS,
	            'mailtype'  => 'html',
	            'charset'   => 'utf-8',
	            'wordwrap'  => TRUE
	        );
			$message = "
				<html>
				<head>
					<title>Verification Code</title>
				</head>
				<body>
					<h2>Welcome to Meetingspot.</h2>
					<p>Hello, ".$req['first_name']."&nbsp;".$req['last_name'].".&nbsp;Thank you for registering to our service.</p>
					<p>Your Account:</p>
					<p>Email: ".$email."</p>
					<p>Password: ".$password."</p>
					<p>Please click the link below to activate your account.</p>
					<h4><a href='".site_url()."/register/activate/".$user_id."/".$code."'>Activate Now</a></h4>
				</body>
				</html>
			";

			$this->email->initialize($config);
		    $this->email->set_newline("\r\n");
		    $this->email->from($config['smtp_user'], 'Meetingspot');
		    $this->email->to($email);
		    $this->email->subject('Signup Verification Email');
		    $this->email->message($message);

			//sending email
		    if($this->email->send()){
		    	$this->session->set_flashdata('msg','Please confirm your email to activate your account.');
		    }
		    else{
		    	$this->session->set_flashdata('msg', $this->email->print_debugger());
		    }
		    
			$this->load_view('register');			
		}
	}


	function activate() {
		$id =  $this->uri->segment(3);
		$code = $this->uri->segment(4);

		$customer = $this->Base_Model->get_info('users', $id);

		if($customer['activation_code'] == $code) {
			$update['id'] = $id;
			$update['status'] = ACTIVE;
			$update['email_verified_at'] = date('Y-m-d H:i:s');
			$this->Base_Model->update_info('users', $update);

			$this->session->set_flashdata('msg', 'User activated successfully. Please login with new account');
		}
		else{
			$this->session->set_flashdata('msg', 'Cannot activate account. Activation code didnt match');
		}
		redirect('register');
	}
	
}