<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation extends Base_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('reservation_model');
	}

	function save_reservation(){
		$req = $this->input->post();
		$resp = array();

		$location_id = $req['location_id'];
		$res_date = date('Y-m-d', strtotime($req['res_date']));
		$start_time = date('H:i:s', strtotime($req['start_time']));
		$end_time = date('H:i:s', strtotime($req['end_time']));

		$res_day = Intval(date('N', strtotime($res_date))) - 1;
		$working_hour = $this->Base_Model->get_info_with_where('working_hours', array('weekday'=>$res_day, 'location_id'=>$location_id));
		if($working_hour['status'] == CLOSED) {
			$resp['status'] = 'failed';
			$resp['msg'] = 'This location is closed at that time';
			$this->load_json($resp);
			return;
		} else {
			if($working_hour['open_hour'] > $start_time || $working_hour['close_hour'] < $end_time) {
				$resp['status'] = 'failed';
				$resp['msg'] = 'Your reserved time is not within working hour. Please look at the working hours again.';
				$this->load_json($resp);
				return;
			}
			$res_available = $this->reservation_model->check_reservation_by_date($res_date, $start_time, $end_time);

			if($res_available) {
				$resp['status'] = 'failed';
				$resp['msg'] = 'Already exist the reservations within your time interval. Please check the reserved times of this room.';
				$this->load_json($resp);
				return;
			}

			$insert_data = array();
			if($req['is_group'] == '1') {
				if(!empty($req['group_id'])) 
					$insert_data['group_id'] = $req['group_id'];
				else {
					$new_group = array(
						'user_id'=>$this->user_id,
						'group_name'=>$req['group_name'],
						'address'=>$req['address'],
						'phone'=>$req['phone'],
						'website'=>$req['website'],
						'facebook'=>$req['facebook'],
						'nature_of_group'=>$req['nature_of_group'],
						'min_age'=>$req['min_age']
					);
					$insert_data['group_id'] = $this->Base_Model->save_info('groups', $new_group);
				}
			} 

			$max_res_number = $this->Base_Model->get_max_value('reservations','res_number');
			if(empty($max_res_number))
				$max_res_number = START_VAL;
			else
				$max_res_number += 1;

			$insert_data['room_id'] = $req['room_id'];
			$insert_data['user_id'] = $this->user_id;
			$insert_data['res_number'] = $max_res_number;
			$insert_data['guest_number'] = $req['guest_number'];
			$insert_data['res_date'] = $res_date;
			$insert_data['start_time'] = $start_time;
			$insert_data['end_time'] = $end_time;
			$insert_data['note'] = $req['note'];

			$this->Base_Model->save_info('reservations', $insert_data); 
			$resp['status'] = 'success';
			$resp['msg'] = 'Your reservation was successfully submitted.';
			$this->load_json($resp);
			return;
		}
		
	}
}