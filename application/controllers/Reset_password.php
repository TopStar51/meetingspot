<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset_password extends Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	function index(){
		$user_id = $this->uri->segment(3);
		$view_params['user_id'] = $user_id;

		$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
		$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'required|matches[password]');

		if($this->form_validation->run() == FALSE) {
           	$this->load_view('reset_password', $view_params);
		} else {
			$req = $this->input->post();
			$update['id'] = $user_id;
			$update['password'] = md5($req['password']);
			$this->Base_Model->update_info('users', $update);

			$this->session->set_flashdata('msg','Your password succesfully updated.');
			$this->load_view('reset_password', $view_params);
		}
		
	}
}