<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends Base_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index() {
		$this->load_view('signin');
	}

	public function login(){
		$req = $this->input->post();
		$resp = array();

		$where = "username='".$req['username']."' OR email='".$req['username']."'";
		$user = $this->Base_Model->get_info_with_where('users', $where);
		if(empty($user)) {
			$resp['state'] = 'failure';
			$resp['msg'] = 'The username or email is incorrect'; 
			echo json_encode($resp);
			return;

		} else if($user['password'] != md5($req['password'])) {
			$resp['state'] = 'failure';
			$resp['msg'] = 'The password is incorrect'; 
			echo json_encode($resp);
			return;

		} else if($user['status'] == INACTIVE) {
			$resp['state'] = 'failure';
			$resp['msg'] = 'Your account is not activated'; 
			echo json_encode($resp);
			return;

		} else {
			$this->user_session_create($user);
	        if($user['type'] == CUSTOMER) {		        	
		        $resp['state'] = 'success';
		        $resp['url'] = 'customer/profile';
	        } else {
	        	$resp['state'] = 'success';
		        $resp['url'] = 'owner/location';
	        }

	        echo json_encode($resp);
		}
	}

}