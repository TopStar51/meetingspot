<?php
/**
 * Created by PhpStorm.
 * User: Elvis
 * Date: 22/02/2019
 * Time: 11:58
 */

class Category extends Base_Controller
{
    function ajax_category_list() {
        $req = $this->input->get();
        $categories = $this->Base_Model->get_list('categories', 0, 'name like \'%'.$req['q'].'%\'', 'name', 'asc', 'id, name');//order by name
        $this->load_json(array(
            'items' => $categories
        ));
    }
}