<?php
/**
 * Created by PhpStorm.
 * User: Elvis
 * Date: 21/02/2019
 * Time: 00:46
 */

class Customer extends Base_Controller
{
    function index() {
        $this->list_view();
    }

    function list_view() {

        $this->load_js('assets/admin/global/scripts/datatable.js');
        $this->load_js('assets/admin/global/plugins/datatables/datatables.min.js');
        $this->load_js('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->load_css('assets/admin/global/plugins/bootstrap-select/css/bootstrap-select.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-select/js/bootstrap-select.js');
        $this->load_admin_view('customer/list_view');
    }

    function add() {
        $this->edit(0);
    }

    function edit($customerID) {
        //customerID == 0  --> add customer
        $contentData = array();
        if ($customerID) {
            $contentData['customer'] = $this->Base_Model->get_info($this->user_table, $customerID);
            if (empty($contentData['customer'])) {
                //means there's not customer with given id
                redirect('admin/customer');
            }
        }
        $this->load_css('assets/admin/global/plugins/bootstrap-select/css/bootstrap-select.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-select/js/bootstrap-select.js');
        $this->load_admin_view('customer/edit', $contentData);
    }

    function save_customer() {
        $req = $this->input->post();
        //check for duplicate
        $dupExist = $this->Base_Model->get_info_with_where($this->user_table, array('email' => $req['email']));
        $error = '';
        if ($dupExist && (!isset($req['id']) || $dupExist['id'] != $req['id'])) {
            $error = 'Email duplicated';
        } else {
            //then save
            $customer = $req;
            unset($customer['confirm_password']);//unnecessary field -> remove
            $this->Base_Model->save($this->user_table, $customer);
        }
        //if success -> go to list
        if ($error == '') {
            redirect('admin/customer');
        } else {
            //load edit view
            $contentData['customer'] = $req;
            $contentData['error'] = $error;

            $this->load_admin_view('customer/edit', $contentData);
        }
    }


    public function delete_customer($customerID)
    {
        //check before delete, customer is used for ...?
        $this->Base_Model->delete_info($this->user_table, $customerID);
        $this->load_json(array('status' => true));
    }

    function get_table_data() {
        $this->load->model('customer_model', 'customer');
        $req = $this->input->post();
        $records = array();
        $records['data'] = $this->customer->get_table_data($req);
        $records['recordsTotal'] = $records['recordsFiltered'] = $this->customer->get_table_count($req);

        $this->load_json($records);

    }
}