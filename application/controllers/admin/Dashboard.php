<?php

class Dashboard extends Base_Controller {

    function index() {

        $contentData = array();
        //owner, reservation, customer

        $contentData['owner'] = array(
            'total' => $this->Base_Model->get_count_value($this->user_table, array('type' => OWNER)),
            'approved' => $this->Base_Model->get_count_value($this->user_table, array('type' => OWNER, 'status' => ACTIVE)),
            'pending' => $this->Base_Model->get_count_value($this->user_table, array('type' => OWNER, 'status' => INACTIVE))
        );
        $contentData['location'] = array(
            'total' => $this->Base_Model->get_count_value('locations', ''),
            'approved' => $this->Base_Model->get_count_value('locations', array('status' => ACTIVE)),
            'pending' => $this->Base_Model->get_count_value('locations', array('status' => INACTIVE))
        );
        $contentData['reservation'] = array(
            'total' => $this->Base_Model->get_count_value('reservations', ''),
            'confirmed' => $this->Base_Model->get_count_value('reservations', array('status' => CONFIRMED)),
            'cancelled' => $this->Base_Model->get_count_value('reservations', array('status' => CANCELLED)),
            'pending' => $this->Base_Model->get_count_value('reservations', array('status' => PENDING))
        );
        $contentData['customer'] = array(
            'total' => $this->Base_Model->get_count_value($this->user_table, array('type' => CUSTOMER)),
            'active' => $this->Base_Model->get_count_value($this->user_table, array('type' => CUSTOMER, 'status' => ACTIVE)),
            'inactive' => $this->Base_Model->get_count_value($this->user_table, array('type' => CUSTOMER, 'status' => INACTIVE))
        );
        $this->load_admin_view('dashboard', $contentData);

    }

}