<?php

class Design extends Base_Controller
{

	private $tb_page = 'pages';
    private $tb_header = 'header_data';
    private $tb_footer = 'footer_data';

	function __construct() {
		parent::__construct();
	}

	function page() {
		$this->load_js('assets/admin/global/scripts/datatable.js');
        $this->load_js('assets/admin/global/plugins/datatables/datatables.min.js');
        $this->load_js('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->load_css('assets/admin/global/plugins/bootstrap-select/css/bootstrap-select.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-select/js/bootstrap-select.js');
        $this->load_admin_view('design/page');
	}


    function get_table_page_data() {
    	$this->load->model('page_model');
        $req = $this->input->post();
        $records = array();
        $records['data'] = $this->page_model->get_table_data($req);
        $records['recordsTotal'] = $records['recordsFiltered'] = $this->page_model->get_table_count($req);

        $this->load_json($records);
    }

    function delete_page($page_id) {
    	$where['id'] = $page_id;
    	$this->Base_Model->delete_info_with_where($this->tb_page, $where);
    	$data['status'] = true;
    	$this->load_json($data);
    }

    function save_page() {
    	$req = $this->input->post();

    	if (empty($req['is_visible'])) {
            $req['is_visible'] = '0';
        } else {
            $req['is_visible'] = '1';
        }

        if(empty($req['id'])) {
        	$this->Base_Model->save_info($this->tb_page, $req);
		} else {
			$this->Base_Model->update_info($this->tb_page, $req);
		}

        $data['state'] = 'success';
        $this->load_json($data);
    }

    function header() {
        $this->load_css('assets/admin/global/plugins/dropzone/dropzone.min.css');
        $this->load_css('assets/admin/global/plugins/dropzone/basic.min.css');
        $this->load_js('assets/admin/global/plugins/dropzone/dropzone.min.js');
        $view_params['pages'] = $this->Base_Model->get_list($this->tb_page);
    	$this->load_admin_view('design/header', $view_params);
    }

    function get_form_data() {
        $req = $this->input->post();

        $data['pageInfo'] = $this->Base_Model->get_info_with_where($this->tb_header, 'page_id='.$req['page_id']);
        $data['state'] = 'success';
        $this->load_json($data);
    }

    function footer($type) {
        $where['type'] = $type;
        $view_params['content'] = $this->Base_Model->get_info_with_where($this->tb_footer, $where);
        $view_params['type'] = $type;
        $this->load_admin_view('design/footer/'.$type, $view_params);
    }

    function upload_header_image(){
        $uploadResp = $this->upload_image('front/header', 'file');

        $data = array();
        $data['uploadResp'] = $uploadResp;
        $this->load_json($data);
    }

    function update_header_data(){
        $req = $this->input->post();

        $req['show_title'] = empty($req['show_title']) ? 'off' : 'on';
        $req['show_cover'] = empty($req['show_cover']) ? 'off' : 'on';

        $header_info = $this->Base_Model->get_info_with_where('header_data', 'page_id='.$req['page_id']);
        if(empty($header_info)) {
            $this->Base_Model->save_info('header_data', $req);
        } else {
            $req['id'] = $header_info['id'];
            $this->Base_Model->update_info('header_data', $req);
        }

        $data['state'] = 'success';
        $this->load_json($data);
    }

    function update_footer_data() {
        $req = $this->input->post();

        $type = $req['type'];
        unset($req['type']);

        $footer_data = $this->Base_Model->get_info_with_where($this->tb_footer, 'type="'.$type.'"');

        $update_info = array();
        $update_info['id'] = $footer_data['id'];

        if(!empty($footer_data['data'])) {
            $footerData = json_decode($footer_data['data']);

            foreach ($req as $key => $value) {
                $footerData->$key = $value;
            }
        }

        $update_info['data'] = json_encode($footerData);
        $this->Base_Model->update_info($this->tb_footer, $update_info);

        $data['state'] = 'success';
        $this->load_json($data);

    }

    function upload_image($uploadDir, $fieldName, $checkExist = true) {

        $retVal = array();
        $retVal['state'] = 'success';

        if (!file_exists('./upload/'.$uploadDir)) {
            mkdir('./upload/'.$uploadDir, 0777, true);
        }

        //upload file
        $config['upload_path'] = './upload/'.$uploadDir;
        $config['allowed_types'] = 'bmp|jpg|png|jpeg';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '10240'; //10 MB

        $this->load->library('upload', $config);

        if (isset($_FILES[$fieldName]['name'])) {
            if (0 < $_FILES[$fieldName]['error']) {
                $retVal['msg'] = 'Error during file upload' . $_FILES[$fieldName]['error'];
            } else {
                if (!$this->upload->do_upload($fieldName)) {
                    $retVal['state'] = 'failed';
                    $retVal['msg'] = $this->upload->display_errors();
                } else {
                    $retVal['state'] = 'success';
                    $retVal['uploadData'] = $this->upload->data();
                }
            }
        } else {
            if ($checkExist) {
                $retVal['state'] = 'failed';
                $retVal['msg'] = 'Please choose a file';
            }
        }

        return $retVal;
    }
}