<?php

class Group extends Base_Controller
{
    private $tb_group = 'groups';

    function index() {
        $this->list_view();
    }

    function list_view() {

        $this->load_js('assets/admin/global/scripts/datatable.js');
        $this->load_js('assets/admin/global/plugins/datatables/datatables.min.js');
        $this->load_js('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->load_css('assets/admin/global/plugins/bootstrap-select/css/bootstrap-select.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-select/js/bootstrap-select.js');
        $this->load_admin_view('group/list_view');
    }

    function add() {
        $this->edit(0);
    }

    function edit($group_id) {
        //customerID == 0  --> add customer
        $contentData = array();
        if ($group_id) {
            $contentData['group'] = $this->Base_Model->get_info($this->tb_group, $group_id);
            if (empty($contentData['group'])) {
                //means there's not customer with given id
                redirect('admin/group');
            }
        }
        $contentData['customers'] = $this->Base_Model->get_list('users', 0, array('status'=>ACTIVE, 'type'=>CUSTOMER));
        $this->load_admin_view('group/edit', $contentData);
    }

    function get_table_data() {
        $this->load->model('group_model', 'group');
        $req = $this->input->post();
        $records = array();
        $records['data'] = $this->group->get_table_data($req);
        $records['recordsTotal'] = $records['recordsFiltered'] = $this->group->get_table_count($req);

        $this->load_json($records);

    }

    function save_group() {

        $req = $this->input->post();
        
        $this->Base_Model->save('groups', $req);

        $contentData['group'] = $req;
        $contentData['customers'] = $this->Base_Model->get_list('users', 0, array('status'=>ACTIVE, 'type'=>CUSTOMER));
        $this->load_admin_view('group/edit', $contentData);
        
    }

    function delete_group($group_id)
    {
        //check before delete, owner is used for ...?
        $this->Base_Model->delete_info('groups', $group_id);
        $this->load_json(array('status' => true));
    }
}