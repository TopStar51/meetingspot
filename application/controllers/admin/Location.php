<?php
/**
 * Created by PhpStorm.
 * User: Elvis
 * Date: 21/02/2019
 * Time: 00:46
 */

class Location extends Base_Controller
{
    function index() {
        $this->list_view();
    }

    function list_view() {

        $this->load_js('assets/admin/global/scripts/datatable.js');
        $this->load_js('assets/admin/global/plugins/datatables/datatables.min.js');
        $this->load_js('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->load_css('assets/admin/global/plugins/bootstrap-select/css/bootstrap-select.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-select/js/bootstrap-select.js');
        $this->load_admin_view('location/list');
    }

    function add() {
        $this->edit(0);
    }

    function edit($locationID) {
        //locationID == 0  --> add location
        $contentData = array();
        if ($locationID) {
            $this->load->model('location_model', 'location');
            $contentData['location'] = $this->location->get_location_by_id($locationID);
            $contentData['working_hours'] = $this->location->working_hours($locationID);
            if (empty($contentData['location'])) {
                //means there's not location with given id
                redirect('admin/location');
            }
        }

       
        $contentData['example_hours'] = array(
            array('day' => '0', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1'),
            array('day' => '1', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1'),
            array('day' => '2', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1'),
            array('day' => '3', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1'),
            array('day' => '4', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1'),
            array('day' => '5', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1'),
            array('day' => '6', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1')
        );

        $contentData['food_serves'] = array(
            array('id' => '0', 'name' => 'At the ordering counter'),
            array('id' => '1', 'name' => 'Buffet'),
        );

        //time picker
        $this->load_css('assets/admin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js');
        //switch
        $this->load_css('assets/admin/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->load_js('assets/admin/pages/scripts/components-bootstrap-switch.min.js');
        //wizard
        $this->load_js('assets/admin/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js');
        //dropzone
        $this->load_css('assets/admin/global/plugins/dropzone/dropzone.min.css');
        $this->load_js('assets/admin/global/plugins/dropzone/dropzone.min.js');

        $this->load_css('assets/admin/global/plugins/bootstrap-select/css/bootstrap-select.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-select/js/bootstrap-select.min.js');

        $this->load_css('assets/admin/custom/custom.css');

        $this->load_admin_view('location/edit', $contentData);
    }

    function save_location() {
        $this->load->model('location_model', 'location');
        $this->load->helper('string');
        $req = $this->input->post();

        $location = $req;
//        exit(json_encode($location));
        unset($location['confirm_password']);//unnecessary field -> remove

        //save thumb, gallery, video
        if (!empty($_FILES['thumb']['name'])) {
            if (!file_exists('./upload/location/'))
                mkdir('./upload/location/', 0777, true);

            $upload_path = './upload/location/';
            $file_name = md5(random_string('alnum', 16));
            $file_ext = substr($_FILES['thumb']['name'], strpos($_FILES['thumb']['name'], "."));;
            move_uploaded_file($_FILES['thumb']['tmp_name'], $upload_path . $file_name . $file_ext);
            $location['thumb_image'] = $file_name . $file_ext;
        }

        if (!empty($_FILES['video']['name'])) {
            if (!file_exists('./upload/location/'))
                mkdir('./upload/location/', 0777, true);
            $upload_path = './upload/location/';
            $file_name = md5(random_string('alnum', 16));
            $file_ext = substr($_FILES['video']['name'], strpos($_FILES['video']['name'], "."));;
            move_uploaded_file($_FILES['video']['tmp_name'], $upload_path . $file_name . $file_ext);
            $location['video'] = $file_name . $file_ext;
        }

        if (!isset($location['gallery']) || !is_array($location['gallery']))
            $location['gallery'] = array();

        if (!empty($_FILES['gallery']['name'][0])) {
            if (!file_exists('./upload/location/'))
                mkdir('./upload/location/', 0777, true);
            foreach ($_FILES['gallery']['name'] as $key => $val) {
                $upload_path = './upload/location/';
                $file_name = md5(random_string('alnum', 16));
                $file_ext = substr($_FILES['gallery']['name'][$key], strpos($_FILES['gallery']['name'][$key], "."));
                move_uploaded_file($_FILES['gallery']['tmp_name'][$key], $upload_path . $file_name . $file_ext);
                array_push($location['gallery'], $file_name . $file_ext);
            }
        }

        if (empty($location['category_id'])) unset($location['category_id']);
        if (empty($location['owner_id'])) unset($location['owner_id']);
        $working_hours = $location['working_hour'];
        unset($location['working_hour']);
        $location['gallery'] = json_encode($location['gallery']);
        if (!isset($location['id'])) {
            $location['progress'] = 11;//if admin registers a location, progress is full
        }
        //in case of address changed?
        //$location = array_merge($location, $location['address']);

        $result = $this->Base_Model->save('locations', $location);

        if (!isset($location['id'])) {
            $location['id'] = $result;
        }

        foreach ($working_hours as $key => $value) {
            $value['weekday'] = $key;
            if (isset($location['id']))
                $value['location_id'] = $location['id'];
            else
                $value['location_id'] = $result;
            $this->location->save_working_hour($value);
        }

        //if success -> go to list
        $this->load_json(array(
            'state' => 'success'
        ));
    }

    public function delete_location($locationID)
    {
        //check before delete, location is used for ...?
        $this->Base_Model->delete_info('locations', $locationID);
        $this->load_json(array('status' => true));
    }

    function get_table_data() {
        $this->load->model('location_model', 'location');
        $req = $this->input->post();
        $records = array();
        $records['data'] = $this->location->get_table_data($req);
        $records['recordsTotal'] = $records['recordsFiltered'] = $this->location->get_table_count($req);

        $this->load_json($records);
    }
}