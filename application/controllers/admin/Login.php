<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Base_Controller {

    public function index() {

        if ($this->type==ADMIN && $this->logged_in) {
            redirect('admin/dashboard');
        }

        $req = $this->input->post();
        $contentData = $req;
        
        if (isset($req['email']) && isset($req['password'])) {
            //then tries to login
            $where['email'] = $req['email'];
            $where['password'] = md5($req['password']);
            $where['type'] = ADMIN;

            $adminInfo = $this->Base_Model->get_info_with_where('users', $where);
            if (empty($adminInfo)) {
                $contentData['failed'] = true;
            } else {

                $this->user_session_create($adminInfo);

                redirect('admin/dashboard');
            }
        }
        $this->load->view('admin/login/index', $contentData);
    }

    function forget() {
        $this->load->view('admin/login/forget');
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('admin/login');
    }
}