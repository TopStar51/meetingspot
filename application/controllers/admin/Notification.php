<?php
/**
 * Created by PhpStorm.
 * User: Elvis
 * Date: 2019-03-25
 * Time: 오후 4:50
 */

class Notification extends Base_Controller
{
    function index() {
        $this->load_js('assets/admin/global/scripts/datatable.js');
        $this->load_js('assets/admin/global/plugins/datatables/datatables.min.js');
        $this->load_js('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');

        $this->load_admin_view('notification');
    }

    function ajax_table() {
        $this->notification->read_all();

        $req = $this->input->post();
        $req['whom'] = 0;

        $records = array();
        $records['data'] = $this->notification->admin_table_data($req);
        $records['recordsTotal'] = $records['recordsFiltered'] = $this->notification->get_count();

        $this->load_json($records);
    }
}