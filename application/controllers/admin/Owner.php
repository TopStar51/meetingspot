<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Owner extends Base_Controller {

    function index() {
        $this->list_view();
    }

    public function list_view()
    {
        $view_params = array();
        $view_params['owners'] = $this->Base_Model->get_list($this->user_table, 0, array('type'=>OWNER));

        $this->load_js('assets/admin/global/scripts/datatable.js');
        $this->load_js('assets/admin/global/plugins/datatables/datatables.min.js');
        $this->load_js('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->load_css('assets/admin/global/plugins/bootstrap-select/css/bootstrap-select.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-select/js/bootstrap-select.min.js');

        $this->load_admin_view('owner/list', $view_params);
    }

    public function get_table_data() {
        $req = $this->input->post();
        $this->load->model('owner_model', 'owner');
        $records = array();
        $records['data'] = $this->owner->get_table_data($req);
        $records['recordsTotal'] = $records['recordsFiltered'] = $this->owner->get_table_count($req);

        $this->load_json($records);
    }

    public function add()
    {
        $this->edit(0);
    }

    public function edit($ownerID)
    {
        $contentData = array();
        if ($ownerID) {
            $contentData['owner'] = $this->Base_Model->get_info($this->user_table, $ownerID);

            if (!$contentData['owner']) {
                redirect('admin/owner');
            }
        }
        $this->load_css('assets/admin/global/plugins/bootstrap-select/css/bootstrap-select.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-select/js/bootstrap-select.js');
        $this->load_admin_view('owner/edit', $contentData);
    }

    public function save_owner()
    {
        $req = $this->input->post();
        //check for duplicate
        $dupExist = $this->Base_Model->get_info_with_where($this->user_table, array('email' => $req['email']));
        $error = '';
        if ($dupExist && (!isset($req['id']) || $dupExist['id'] != $req['id'])) {
            $error = 'Email duplicated';
        } else {
            //then save
            $owner = $req;
            $owner['type'] = OWNER;
            unset($owner['confirm_password']);//unnecessary field -> remove
            $this->Base_Model->save('users', $owner);
        }
        //if success -> go to list
        if ($error == '') {
            redirect('admin/owner');
        } else {
            //load edit view
            $contentData['owner'] = $req;
            $contentData['error'] = $error;

            $this->load_css('assets/admin/global/plugins/bootstrap-select/css/bootstrap-select.css');
            $this->load_js('assets/admin/global/plugins/bootstrap-select/js/bootstrap-select.js');
            $this->load_admin_view('owner/edit', $contentData);
        }
    }

    public function delete_owner($ownerID)
    {
        //check before delete, owner is used for ...?
    	$this->Base_Model->delete_info($this->user_table, $ownerID);
    	$this->load_json(array('status' => true));
    }

    function ajax_owner_list() {
        $req = $this->input->get();

        $this->db->group_start();
        $this->db->like('last_name', $req['q'], 'both');
        $this->db->or_like('first_name', $req['q'], 'both');
        $this->db->group_end();
        $this->db->where('type', OWNER);
        $owners = $this->Base_Model->get_list($this->user_table, 0, '', "owner", 'asc', "id, CONCAT(first_name, ' ', last_name) owner");//order by name
        $this->load_json(array(
            'items' => $owners
        ));
    }
}