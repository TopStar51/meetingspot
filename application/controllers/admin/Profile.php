<?php
/**
 * Created by PhpStorm.
 * User: Elvis
 * Date: 21/02/2019
 * Time: 00:13
 */

class Profile extends Base_Controller
{
    function index() {
        $req = $this->input->post();
        $contentData = array();

        $admin = $this->Base_Model->get_info($this->user_table, $this->user_id);
        $contentData['show_passwords'] = false;
        if (empty($req)) {//not saving
            $contentData['admin'] = $admin;
        } else {
            //check for current password
            if (isset($req['cur_password'])) {
                $contentData['admin'] = array_merge($admin, $req);
                //update password
                if ($admin['password'] != md5($req['cur_password'])) {
                    $contentData['show_passwords'] = true;
                    $contentData['error'] = 'Current password doesn\'t match.';
                } else {
                    $req['id'] = $this->user_id;
                    unset($req['cur_password']);
                    unset($req['confirm_password']);
                    $req['password'] = md5($req['password']);
                    $this->Base_Model->update_info($this->user_table, $req);
                    $contentData['msg'] = 'Updated Successfully';
                }
            } else {
                $contentData['admin'] = $req;
                //check for email duplicate
                $dupExist = $this->Base_Model->get_info_with_where($this->user_table, array('email' => $req['email'], 'id !=' => $this->user_id));
                if ($dupExist) {
                    $contentData['error'] = 'Email already using.';
                } else {
                    $req['id'] = $this->user_id;
                    $this->Base_Model->update_info($this->user_table, $req);
                    $contentData['msg'] = 'Updated Successfully';
                }
            }
        }

        $this->load_admin_view('profile', $contentData);
    }
}