<?php
/**
 * Created by PhpStorm.
 * User: Elvis
 * Date: 21/02/2019
 * Time: 02:42
 */

class Reservation extends Base_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('reservation_model', 'reservation');
    }

    function index() {
        $this->list_view();
    }

    function list_view() {
        $this->load_js('assets/admin/global/scripts/datatable.js');
        $this->load_js('assets/admin/global/plugins/datatables/datatables.min.js');
        $this->load_js('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->load_admin_view('reservation/list');
    }

    function detail($reservationID) {
        $contentData = array();
        $reservation = $this->reservation->get_reservation_detail($reservationID);
        if (!$reservation){
            //empty reservation, means wrong reservationID
            redirect('reservation');
        }

        $this->load->model('location_model', 'location');

        $contentData['reservation'] = $reservation;
        $contentData['customer'] = $this->Base_Model->get_info($this->user_table, $reservation['user_id']);
        $contentData['location'] = $this->location->get_detail($reservation['location_id']);

        $this->load_admin_view('reservation/detail', $contentData);
    }

    function get_table_data() {
        
        $req = $this->input->post();
        $records = array();
        $records['data'] = $this->reservation->get_table_data($req);
        $records['recordsTotal'] = $records['recordsFiltered'] = $this->reservation->get_table_count($req);

        $this->load_json($records);

    }

    public function delete_reservation($reservationID)
    {
        //check before delete, reservation is used for ...?
        $this->Base_Model->delete_info('reservations', $reservationID);
        $this->load_json(array('status' => true));
    }
}