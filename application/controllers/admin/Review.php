<?php
/**
 * Created by PhpStorm.
 * User: Elvis
 * Date: 21/02/2019
 * Time: 02:57
 */

class Review extends Base_Controller
{
    function __construct() {
        parent::__construct();

        $this->load->model("review_model", "review");
    }

    function index() {
        $this->list_view();
    }

    function list_view() {
        $this->load_js('assets/admin/global/scripts/datatable.js');
        $this->load_js('assets/admin/global/plugins/datatables/datatables.min.js');
        $this->load_js('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->load_admin_view('review/list');
    }

    function detail($reviewID) {
        $contentData = array();
        $contentData['review'] = $this->review->get_detail($reviewID);
        if (!$contentData['review']) {
            //wrong reviewID
            redirect('admin/review');
        }

        $this->load_css('assets/admin/global/plugins/bootstrap-select/css/bootstrap-select.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-select/js/bootstrap-select.js');
        $this->load_css('assets/admin/custom/icon&star.css');
        $this->load_admin_view('review/detail', $contentData);
    }

    function get_table_data() {
        $this->load->model('review_model', 'review');
        $req = $this->input->post();
        $records = array();
        $records['data'] = $this->review->get_table_data($req);
        $records['recordsTotal'] = $records['recordsFiltered'] = $this->review->get_table_count($req);

        $this->load_json($records);
    }


    public function delete_review($reviewID)
    {
        //check before delete, review is used for ...?
        $this->Base_Model->delete_info('reviews', $reviewID);
        $this->load_json(array('status' => true));
    }
}