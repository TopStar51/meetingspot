<?php
/**
 * Created by PhpStorm.
 * User: Midas
 * Date: 2019-03-06
 * Time: 오전 9:12
 */

class Room extends Base_Controller
{
    function index() {
        $contentData = array();

        $this->load_js('assets/admin/global/scripts/datatable.js');
        $this->load_js('assets/admin/global/plugins/datatables/datatables.min.js');
        $this->load_js('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->load_css('assets/admin/global/plugins/bootstrap-select/css/bootstrap-select.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-select/js/bootstrap-select.js');

        $this->load_admin_view('room', $contentData);
    }

    function ajax_rooms_table() {
        $this->load->model('room_model', 'room');
        $req = $this->input->post();
        $records = array();
        $records['data'] = $this->room->get_table_data($req);
        $records['recordsTotal'] = $records['recordsFiltered'] = $this->room->get_table_count($req);

        $this->load_json($records);
    }

    function create_room() {
        $this->edit(0);
    }

    function edit($id = 0) {
        $this->load->model('location_model', 'location');
        $this->load->model('Room_model');

        $contentData = array();
        $contentData['wall_types'] = array(
            array('id' => '0', 'name' => 'Cement'),
            array('id' => '1', 'name' => 'Drywall'),
            array('id' => '2', 'name' => 'Wood'),
            array('id' => '3', 'name' => 'Divider')
        );
        $contentData['table_formats'] = array(
            array('id' => '0', 'name' => 'Circle'),
            array('id' => '1', 'name' => 'Rectangle'),
        );
        $contentData['table_types'] = array(
            array('id' => '0', 'name' => 'Dining'),
            array('id' => '1', 'name' => 'Conference'),
        );
        $contentData['food_serves'] = array(
            array('id' => '0', 'name' => 'At the ordering counter'),
            array('id' => '1', 'name' => 'Buffet'),
        );

        //$contentData['locations'] = $this->location->get_all_locations($this->user_id);
        $contentData['locations'] = $this->Base_Model->get_list('locations', 0, array('status'=>ACTIVE));
        if ($id == 0) {
            $contentData['title'] = 'Rooms/Register Room';
        } else {
            $data = $this->Room_model->get_room_by_id($id, $this->user_id);
            $contentData['room'] = $data;

            $contentData['title'] = 'Rooms/Edit Room';
        }

        //$this->load_css('assets/owner/css/edit.css');
        $this->load_css('assets/admin/custom/custom.css');
        $this->load_css('assets/admin/global/plugins/bootstrap-select/css/bootstrap-select.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-select/js/bootstrap-select.js');

        $this->load_admin_view('room/edit', $contentData);
    }

    function ajax_location_list() {
        
    }

    function ajax_save() {
        $roomData = $this->input->post();
        $this->Base_Model->save('rooms', $roomData);
        $this->load_json(array(
            'state' => 'success'
        ));
    }

    function ajax_delete() {
        $id = $this->input->post('id');
        $this->Base_Model->delete_info('rooms', $id);
        $this->load_json(array(
            'status' => true
        ));
    }
}
