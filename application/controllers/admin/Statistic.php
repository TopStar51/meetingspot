<?php
/**
 * Created by PhpStorm.
 * User: Elvis
 * Date: 21/02/2019
 * Time: 02:59
 */

class Statistic extends Base_Controller
{
    function index() {
        $this->load_js('assets/admin/global/plugins/amcharts/amcharts/amcharts.js');
        $this->load_js('assets/admin/global/plugins/amcharts/amcharts/serial.js');

        $this->load_css('assets/admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css');
        $this->load_js('assets/admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');
        $this->load_js('assets/admin/pages/scripts/components-date-time-pickers.min.js');

        $this->load_admin_view('statistic');
    }

    function get_statistic() {
        $this->load->model("customer_model", 'customer');
        $this->load->model("location_model", 'location');
        $this->load->model("reservation_model", 'reservation');
        $req = $this->input->post();
        $resp = array();
        $resp['status'] = true;
        $resp['customer'] = $this->customer->get_statistic($req['date']);
        $resp['location'] = $this->location->get_statistic($req['date']);
        $resp['reservation'] = $this->reservation->get_statistic($req['date']);
        //year, month
        $this->load_json($resp);
    }
}