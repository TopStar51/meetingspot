<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_group extends Base_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		$view_params['page_title'] = 'Add Group';
		$this->load_customer_view('add_group', $view_params);
	}

	function ajax_save(){
		$req = $this->input->post();
		$req['user_id'] = $this->user_id;
		if(empty($req['id'])) {
			$this->Base_Model->save_info('groups', $req);
		} else {
			$this->Base_Model->update_info('groups', $req);
		}
		$this->load_json(array(
			'status' => true,
			'url'=>'/customer/mygroup'
		));
	}
}
