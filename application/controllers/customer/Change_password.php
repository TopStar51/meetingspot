<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_password extends Base_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$view_params['page_title'] = 'Change Password';
		$this->load_customer_view('change_password', $view_params);
	}

	function ajax_save() {
        $curPassword = $this->input->post('cur_password');
        $newPassword = $this->input->post('new_password');
        $confirmPassword = $this->input->post('confirm_password');

        //check if curPassword is correct
        $this->load->model('customer_model', 'customer');

        if (!$this->customer->check_password($this->user_id, $curPassword)) {
            $this->load_json(array(
                'status' => false,
                'msg' => 'Current password doesn\'t match'
            ));
            return;
        }

        $this->customer->update_password($this->user_id, $newPassword);
        $this->load_json(array(
            'status' => true
        ));
    }
}
