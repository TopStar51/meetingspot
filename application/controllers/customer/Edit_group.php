<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit_group extends Base_Controller {

	function __construct(){
		parent::__construct();
	}

	function index($group_id){
		$view_params['page_title'] = 'Edit Group';
		$view_params['group_info'] = $this->Base_Model->get_info('groups', $group_id);
		$this->load_customer_view('add_group', $view_params);
	}

	function ajax_save(){
		$req = $this->input->post();
		$req['user_id'] = $this->user_id;
		$this->Base_Model->save_info('groups', $req);

		$this->load_json(array(
			'status' => true,
			'url'=>'/customer/mygroup'
		));
	}
}
