<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mygroup extends Base_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		$view_params['page_title'] = 'My Groups';
		$view_params['group_info'] = $this->Base_Model->get_info_with_where('groups', array('user_id'=>$this->user_id));
		$this->load_customer_view('mygroup', $view_params);
	}

	function del_group(){
		$group_id = $this->input->post('group_id');
		$update['id'] = $group_id;
		$update['is_delete'] = '1';
		$this->Base_Model->update_info('groups', $update);

		$resp = array();
		$resp['state'] = 'success';
		$resp['msg'] = 'Deleted it!';
		$this->load_json($resp);
	}
}
