<?php
/**
 * Created by PhpStorm.
 * User: Elvis
 * Date: 2019-03-26
 * Time: 오후 8:08
 */

class Notification extends Base_Controller
{
    function index() {
        $view_params['page_title'] = 'Notifications';
        $this->notification->read_all($this->user_id);
        $this->load_customer_view('notification', $view_params);
    }

    function ajax_pagination() {
        $this->load->library('script_pagination');

        $input = $this->input->post();

        $cur_page = isset($input['cur_page']) ? $input['cur_page'] : 0;
        $uno = $this->user_id;

        $displayLength = 10;
        $start = $cur_page;

        if (!isset($start)) {
            $start = 0;
        }

        $filter['start'] = $start;
        $filter['limit'] = $displayLength;

        $notificationList = $this->notification->get_notifications($uno, $start, $displayLength);
        $reviewCount = $this->notification->get_count($uno);

        $config['base_url'] = 'javascript:set_notification_page()';
        $config['total_rows'] = $reviewCount;
        $config['per_page'] = $displayLength;
        $config['num_links'] = 2;
        $config['full_tag_open'] = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['next_link'] = '<i class="fa fa-caret-right"></i>';
        $config['prev_link'] = '<i class="fa fa-caret-left"></i>';
        $config['cur_tag_open'] = '<a class="current-page">';
        $config['cur_tag_close'] = '</a>';
        $config['cur_page'] = $cur_page;

        $this->script_pagination->initialize($config);
        $link_html = $this->script_pagination->create_links();
        $link_html = preg_replace('/javascript:set_notification_page\(\)\/(\d*)/i', 'javascript:set_notification_page(\1)', $link_html);

        $view_params = array();
        $view_params['links'] = $link_html;
        $view_params['notifications'] = $notificationList;

        $data = array();
        $data['htmlData'] = '';
        $data['status'] = true;

        $data['htmlData'] .= $this->load->view('front/customer/notification/_pagination', $view_params, TRUE);

        $this->load_json($data);
    }
}