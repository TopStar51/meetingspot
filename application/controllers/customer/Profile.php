<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Base_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$view_params['page_title'] = 'Edit Profile';
		$view_params['customer'] = $this->Base_Model->get_info($this->user_table, $this->user_id);
		$this->load_customer_view('profile', $view_params);
	}

	public function ajax_save() {
		$profile = $this->input->post();
		$profile['id'] = $this->user_id;
		$this->Base_Model->update_info('users', $profile);
		$customer = $this->Base_Model->get_info('users', $this->user_id);
		$this->user_session_create($customer);

		$this->load_json(array(
			'status' => true
		));
	}
}
