<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation extends Base_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('reservation_model', 'reservation');
	}

	function index()
	{
		$view_params['page_title'] = 'Reservation';
		$this->load_customer_view('reservation', $view_params);
	}

	function ajax_pagination() {
		$this->load->model('reservation_model', 'reservation');
		$this->load->library('script_pagination');

        $input = $this->input->post();

        $cur_page = isset($input['cur_page']) ? $input['cur_page'] : 0;
        $uno = $this->user_id;

        $displayLength = 4;
        $start = $cur_page;

        if (!isset($start)) {
            $start = 0;
        }

        $filter['start'] = $start;
        $filter['limit'] = $displayLength;

        $reservations = $this->reservation->get_reservations($uno, $start, $displayLength);
        $review_count = $this->reservation->get_reservation_count('', $uno);

        $config['base_url'] = 'javascript:set_reservation_page()';
        $config['total_rows'] = $review_count;
        $config['per_page'] = $displayLength;
        $config['num_links'] = 2;
        $config['full_tag_open'] = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['next_link'] = '<i class="fa fa-caret-right"></i>';
        $config['prev_link'] = '<i class="fa fa-caret-left"></i>';
        $config['cur_tag_open'] = '<a class="current-page">';
        $config['cur_tag_close'] = '</a>';
        $config['cur_page'] = $cur_page;

        $this->script_pagination->initialize($config);
        $link_html = $this->script_pagination->create_links();
        $link_html = preg_replace('/javascript:set_reservation_page\(\)\/(\d*)/i', 'javascript:set_reservation_page(\1)', $link_html);

        $view_params = array();
        $view_params['links'] = $link_html;
		$view_params['reservations'] = $reservations;

        $data = array();
        $data['htmlData'] = '';
        $data['status'] = true;

        $data['htmlData'] .= $this->load->view('front/customer/reservation/_pagination', $view_params, TRUE);

        $this->load_json($data);
	}

	function add() {
	    $this->edit(0);
    }

	function edit($id) {
	    $this->load->model("reservation_model", "reservation");

	    $contentData = array();
	    $contentData['page_title'] = 'Reservation';
	    $contentData['rooms'] = $this->Base_Model->get_list('rooms');
	    $contentData['groups'] = $this->Base_Model->get_list('groups', 0, array('user_id' => $this->user_id));
	    if ($id) {
            $contentData['reservation'] = $this->Base_Model->get_info('reservations', $id);
        }

	    $this->load_customer_view('reservation/edit', $contentData);
    }

    function ajax_post() {
	    $reservation = $this->input->post();

	    $reservation['user_id'] = $this->user_id;

        //get location's owner
        $this->load->model('room_model', 'room');
        $ownerID = $this->room->get_owner($reservation['room_id']);

        if (!$ownerID) {
            //there's no owner for this reservation's room
            $this->load_json(array(
                'status' => false,
                'msg' => 'Wrong room information'
            ));
        }

	    $resID = $this->reservation->save_reservation($reservation);

        if (isset($reservation['id'])) {
            $this->notification->add($reservation['user_id'], 'reservations', 'UPDATE', $resID, 'modified', $ownerID);
        } else {
            $this->notification->add($reservation['user_id'], 'reservations', 'ADD', $resID, 'reserved', $ownerID);
        }

	    $this->load_json(array(
	        'status' => true
        ));
    }
}
