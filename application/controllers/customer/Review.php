<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends Base_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$view_params['page_title'] = 'Review';
		$this->load_customer_view('review', $view_params);
	}

	function ajax_pagination() {
		$this->load->model('review_model', 'review');
		$this->load->library('script_pagination');

        $input = $this->input->post();

        $cur_page = isset($input['cur_page']) ? $input['cur_page'] : 0;
        $uno = $this->user_id;

        $displayLength = 4;
        $start = $cur_page;

        if (!isset($start)) {
            $start = 0;
        }

        $filter['start'] = $start;
        $filter['limit'] = $displayLength;

        $review_list = $this->review->get_review($uno, $start, $displayLength);
        $review_count = $this->review->get_my_review_count($uno);

        $config['base_url'] = 'javascript:set_review_page()';
        $config['total_rows'] = $review_count;
        $config['per_page'] = $displayLength;
        $config['num_links'] = 2;
        $config['full_tag_open'] = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['next_link'] = '<i class="fa fa-caret-right"></i>';
        $config['prev_link'] = '<i class="fa fa-caret-left"></i>';
        $config['cur_tag_open'] = '<a class="current-page">';
        $config['cur_tag_close'] = '</a>';
        $config['cur_page'] = $cur_page;

        $this->script_pagination->initialize($config);
        $link_html = $this->script_pagination->create_links();
        $link_html = preg_replace('/javascript:set_review_page\(\)\/(\d*)/i', 'javascript:set_review_page(\1)', $link_html);

        $view_params = array();
        $view_params['links'] = $link_html;
        $view_params['reviews'] = $review_list;

        $data = array();
        $data['htmlData'] = '';
        $data['status'] = true;

        $data['htmlData'] .= $this->load->view('front/customer/review/_pagination', $view_params, TRUE);

        $this->load_json($data);
	}
}
