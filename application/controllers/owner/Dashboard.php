<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:34 AM
 */

class Dashboard extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Location_model');
        $this->load->model('Reservation_model');
        $this->load->model('Review_model');
        $this->load->model('Room_model');
    }

    public function index()
    {
        $contentData = array();
        //owner, reservation, customer
        $contentData['title'] = 'Dashboard';
        $contentData['location'] = array(
            'approved' => $this->Location_model->get_location_count(ACTIVE, $this->user_id),
            'pending' => $this->Location_model->get_location_count(INACTIVE, $this->user_id)
        );

        $contentData['room'] = $this->Room_model->get_room_count(ACTIVE, $this->user_id);

        $contentData['reservation'] = array(
            'total' => $this->Reservation_model->get_reservation_count('', $this->user_id),
            'confirmed' => $this->Reservation_model->get_reservation_count(CONFIRMED, $this->user_id),
            'cancelled' => $this->Reservation_model->get_reservation_count(CANCELLED, $this->user_id),
            'pending' => $this->Reservation_model->get_reservation_count(PENDING, $this->user_id)
        );
        $contentData['customer'] = array(
            'total' => $this->Review_model->get_review_count('', $this->user_id)
        );

        $this->load_css('assets/owner/css/dashboard.css');
        $this->load_owner_view('dashboard', $contentData);
    }
}