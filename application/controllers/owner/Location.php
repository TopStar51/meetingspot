<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:34 AM
 */

class Location extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Location_model');
        $this->load->helper('string');
    }

    public function index()
    {
        $view_params['title'] = 'Location';
        $this->load_owner_view('location', $view_params);
    }

    public function ajax_table() {
        $this->retPostList($this->Location_model->get_all_locations($this->user_id), true);
    }

    public function new_location() {
        $this->edit(0);
    }

    public function edit($id) {
        $categories = $this->Location_model->get_list($this->Location_model->_table_catagory);

        $contentData = array();
        if ($this->session->flashdata('need_location_registration')) {
            $contentData['alert_owner'] = true;
            $this->session->set_flashdata('need_location_registration', false);
        }

        $contentData['example_hours'] = array(
            array('day' => '0', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1'),
            array('day' => '1', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1'),
            array('day' => '2', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1'),
            array('day' => '3', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1'),
            array('day' => '4', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1'),
            array('day' => '5', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1'),
            array('day' => '6', 'open' => '12:00 AM', 'close' => '11:59 PM', 'status' => '1')
        );
        $contentData['weekdays'] = $weekdays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $contentData['categories'] = $categories;

        if ($id == 0) {
            $contentData['title'] = 'Locations/Register Location';
        } else {
            $data = $this->Location_model->get_location_by_id($id, $this->user_id);
            $contentData['location'] = $data;
            $contentData['working_hours'] = $this->Location_model->working_hours($id);

            $contentData['title'] = 'Locations/Edit Location';
        }

        $this->load_css('assets/owner/css/location_detail.css');
        $this->load_css('assets/admin/custom/custom.css');
        $this->load_owner_view('location/edit', $contentData);
    }

    public function ajax_delete() {
        $data = $this->input->post();

        $this->Location_model->delete($data['id'], $this->user_id);

        $this->notification->add($this->user_id, 'locations', 'DELETE', $data['id'], 'deleted');

        echo '1';
    }

    function save_location()
    {
        $this->load->model('location_model', 'location');
        $req = $this->input->post();

        $location = $req;
        unset($location['confirm_password']);//unnecessary field -> remove

        //save thumb, gallery, video
        if (!empty($_FILES['thumb_image']['name'])) {
            if (!file_exists('./upload/location/'))
                mkdir('./upload/location/', 0777, true);

            $upload_path = './upload/location/';
            $file_name = md5(random_string('alnum', 16));
            $file_ext = substr($_FILES['thumb_image']['name'], strpos($_FILES['thumb_image']['name'], "."));;
            move_uploaded_file($_FILES['thumb_image']['tmp_name'], $upload_path . $file_name . $file_ext);
            $location['thumb_image'] = $file_name . $file_ext;
        }

        if (!empty($_FILES['video']['name'])) {
            if (!file_exists('./upload/location/'))
                mkdir('./upload/location/', 0777, true);
            $upload_path = './upload/location/';
            $file_name = md5(random_string('alnum', 16));
            $file_ext = substr($_FILES['video']['name'], strpos($_FILES['video']['name'], "."));;
            move_uploaded_file($_FILES['video']['tmp_name'], $upload_path . $file_name . $file_ext);
            $location['video'] = $file_name . $file_ext;
        }

        if (!isset($location['gallery']) || !is_array($location['gallery']))
            $location['gallery'] = array();

        if (!empty($_FILES['gallery']['name'][0])) {
            if (!file_exists('./upload/location/'))
                mkdir('./upload/location/', 0777, true);
            foreach ($_FILES['gallery']['name'] as $key => $val) {
                $upload_path = './upload/location/';
                $file_name = md5(random_string('alnum', 16));
                $file_ext = substr($_FILES['gallery']['name'][$key], strpos($_FILES['gallery']['name'][$key], "."));
                move_uploaded_file($_FILES['gallery']['tmp_name'][$key], $upload_path . $file_name . $file_ext);
                array_push($location['gallery'], $file_name . $file_ext);
            }
        }

        $location['owner_id'] = $this->user_id;
        $location = array_merge($location, getLatLong($location['address']));

        $working_hours = $location['working_hour'];
        unset($location['working_hour']);
        $location['gallery'] = json_encode($location['gallery']);
        $location['progress'] = 11;
        $result = $this->Base_Model->save('locations', $location);

        if (isset($location['id'])) $method = 'UPDATE';
        else $method = 'ADD';
        $this->notification->add($this->user_id, 'locations', $method, $result, 'registered');

        if (!isset($location['id']))
            $location['id'] = $result;

        foreach ($working_hours as $key => $value) {
            $value['weekday'] = $key;
            if (isset($location['id']))
                $value['location_id'] = $location['id'];
            else
                $value['location_id'] = $result;
            $this->location->save_working_hour($value);
        }

        //if success -> go to list
        $this->load_json(array(
            'state' => 'success'
        ));
    }

    function upload() {}
}