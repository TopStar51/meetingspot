<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login1 extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Owner_model');
    }

    public function index()
    {
        if($this->type == OWNER && $this->logged_in) 
            redirect('owner/dashboard');
        $this->load->view('owner/login/index');
        $this->load->view('owner/login/jslink');
    }

    public function ajax_login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $response = array();
        $response['state'] = 'failed';

        if (empty($email) || empty($password)) {
            $response['message'] = 'Email or Password is Empty.';
            return $this->load_json($response);
        }

        $where = array();
        $where['userid'] = $email;
        $where['password'] = md5($password);
        $where['type'] = OWNER;

        $user = $this->Owner_model->get_owner_info_with_where($where);

        if (empty($user)) {
            $response['message'] = 'Wrong Email or Password.';
            return $this->load_json($response);
        } 
        if ($user['status'] == INACTIVE) {
            $response['message'] = 'Your account is not activated';
            return $this->load_json($response);
        }

        $this->user_session_create($user);

        $response['state'] = 'success';
        //check if has completed location or not
        $this->load->model('location_model', 'location');
        if ($this->location->has_one($user['id'])) {
            $response['url'] = 'owner/dashboard';
        } else {
            $location = $this->location->mine($user['id']);
            if ($location) $response['url'] = 'owner/location/edit/'.$location['id'];
            else $response['url'] = 'owner/location/new_location';
        }

        $this->load_json($response);
    }

    public function sign_up() {
        $post = $this->input->post();
        $params = array(
            'first_name' => $post['first_name'], 
            'last_name' => $post['last_name'], 
            'email' => $post['email'], 
            'phone' => $post['phone'], 
            'address' => $post['address'], 
            'password' => md5($post['password']), 
            'city' => $post['city'], 
            'state' => $post['state'], 
            'zip' => $post['zip'],
            'type' => OWNER, 
            'status' =>INACTIVE, 
            'created_at' =>date('y-m-d h:m:s'), 
            'updated_at' =>date('y-m-d h:m:s'));

        $result = $this->Owner_model->user_sign_up($params);

        if ($result == 'success') {
            $ownerID = $this->db->insert_id();
            $this->notification->add($ownerID, 'users', 'ADD', $ownerID, 'Owner', 0);

            $this->load_json(array(
                'status' => true
            ));
        } else {
            $this->load_json(array(
                'status' => false,
                'msg' => $result
            ));
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/owner/login');
    }
}