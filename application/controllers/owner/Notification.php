<?php
/**
 * Created by PhpStorm.
 * User: Elvis
 * Date: 2019-03-26
 * Time: 오후 6:23
 */

class Notification extends Base_Controller
{
    function index() {
        $this->notification->read_all($this->user_id);
        $view_params['title'] = 'Notifications';
        $this->load_owner_view('notification', $view_params);
    }

    function ajax_table() {
        $params = $this->input->post();
        $params['whom'] = $this->user_id;

        $this->retPostList($this->notification->owner_table_data($params));
    }
}