<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:34 AM
 */

class Profile extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $view_params = array();
        //owner, reservation, customer
        $view_params['title'] = 'Profile';
        $view_params['owner'] = $this->Base_Model->get_info($this->user_table, $this->user_id);
        $this->load_owner_view('profile', $view_params);
    }

    public function ajax_save() {
        $data = $this->input->post();
        if ($data['password'] == '')
            unset($data['password']);
        else
            $data['password'] = md5($data['password']);
        unset($data['confirm_password']);

        $data['id'] = $this->user_id;

        $info = $this->Base_Model->get_info_with_where($this->user_table, 'id != '. $data['id']. ' and email = "'. $data['email'] .'"');

        if (is_array($info)) {
            echo json_encode(array('status' => 'error', 'message' => 'email duplicate'));
        } else {
            $this->Base_Model->save($this->user_table, $data);

            $data['type'] = OWNER;
            $this->user_session_create($data);
            
            echo json_encode(array('status' => 'success', 'message' => 'success'));
        }
    }
}