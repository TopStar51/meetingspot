<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:34 AM
 */

class Reservation extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Reservation_model');
    }

    public function index()
    {
        $view_params['title']= 'Reservations';
        //$view_params['status'] = $status;
        $this->load_owner_view('reservation', $view_params);
    }

    public function ajax_table()
    {
        $query = $this->input->get('query');
        if (!is_array($query)) {
            $query = array();
        }
        $query['owner'] = $this->user_id;
        $this->retPostList($this->Reservation_model->get_all_reservation($query));
    }

    public function detail($id = 0)
    {
        $contentData = array();
        $contentData['reservation_data'] = $this->Reservation_model->get_reservation_by_id($id, $this->user_id);

        if (count($contentData['reservation_data']) == 0)
            redirect('/owner/reservation');

        $contentData['title'] = 'Reservations/Details';
        $this->load_owner_view('reservation_detail', $contentData);
    }

    public function ajax_approve() {
        $data = $this->input->post();

        $data = $this->Reservation_model->get_reservation_by_id($data['id'], $this->user_id);

        if (count($data) == 0)
            redirect('/owner/reservation');

        $this->Reservation_model->approve_reservation($data['id'], $this->user_id);

        echo 'success';
    }

    function pending() {
        $this->index('PENDING');
    }

    function confirmed() {
        $this->index('CONFIRMED');
    }

    function cancelled() {
        $this->index('CANCELLED');
    }

    function ajax_reject() {
        $reservationID = $this->input->post('id');

        $result = $this->Reservation_model->reject($reservationID, $this->user_id);

        if ($result == 'success') {
            $this->load_json(array(
                'status' => true
            ));
        } else {
            $this->load_json(array('msg' => $result, 'status' => false));
        }
    }
}