<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:34 AM
 */

class Review extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Review_model');
    }

    public function index()
    {
        $view_params['title'] = 'Reviews';
        $this->load_owner_view('review', $view_params);
    }

    public function ajax_table()
    {
        $this->retPostList($this->Review_model->get_all_review($this->user_id), true);
    }

    public function detail($id = 0)
    {
        $detail = $this->Review_model->get_review_by_id($id, $this->user_id);

        if (count($detail) == 0)
            redirect('/owner/review');

//        $this->title = 'Reviews/Details';
        $this->load_owner_view('review/edit',
            array(
            'review_data' => $detail,
            'title' => 'Reviews/Details'
        ));
    }
}