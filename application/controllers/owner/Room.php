<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:34 AM
 */

class Room extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('room_model');
        $this->load->helper('string');
    }

    public function index()
    {
        $view_params['title'] = 'Room';
        $this->load_owner_view('room', $view_params);
    }

    public function ajax_table() {
        $this->retPostList($this->room_model->get_all_rooms($this->user_id), true);
    }

    public function new_room() {
        $this->edit(0);
    }

    public function edit($id) {
        $this->load->model('location_model', 'location');

        // $rooms = $this->room_model->get_list($this->room_model->_table);

        $contentData = array();
        $contentData['wall_types'] = array(
            array('id' => '0', 'name' => 'Cement'),
            array('id' => '1', 'name' => 'Drywall'),
            array('id' => '2', 'name' => 'Wood'),
            array('id' => '3', 'name' => 'Divider')
        );
        $contentData['table_formats'] = array(
            array('id' => '0', 'name' => 'Circle'),
            array('id' => '1', 'name' => 'Rectangle'),
        );
        $contentData['table_types'] = array(
            array('id' => '0', 'name' => 'Dining'),
            array('id' => '1', 'name' => 'Conference'),
        );
        
        $contentData['food_serves'] = array(
            array('id' => '0', 'name' => 'At the ordering counter'),
            array('id' => '1', 'name' => 'Buffet'),
        );

        // $contentData['rooms'] = $rooms;
        $contentData['locations'] = $this->location->get_all_locations($this->user_id, array('status'=>ACTIVE));

        if ($id == 0) {
            $contentData['title'] = 'Rooms/Register Room';
        } else {
            $data = $this->room_model->get_room_by_id($id, $this->user_id);
            $contentData['room'] = $data;

            $contentData['title'] = 'Rooms/Edit Room';
        }

        //  $this->load_css('assets/owner/css/edit.css');
        $this->load_css('assets/admin/custom/custom.css');
        $this->load_owner_view('room/edit', $contentData);
    }

    function ajax_delete() {
        $data = $this->input->post();

        $this->room_model->delete($data['id'], $this->user_id);

        echo '1';
    }

    function ajax_save()
    {
        $req = $this->input->post();

        // exit(json_encode($req));

        /*  $room = $req;

        //save thumb, gallery, video
        if (!empty($_FILES['thumb_image']['name'])) {
            if (!file_exists('./upload/room/'))
                mkdir('./upload/room/', 0777, true);

            $upload_path = './upload/room/';
            $file_name = md5(random_string('alnum', 16));
            $file_ext = substr($_FILES['thumb_image']['name'], strpos($_FILES['thumb_image']['name'], "."));;
            move_uploaded_file($_FILES['thumb_image']['tmp_name'], $upload_path . $file_name . $file_ext);
            $room['thumb_image'] = $file_name . $file_ext;
        }

        if (!isset($room['gallery']) || !is_array($room['gallery']))
            $room['gallery'] = array();

        if (!empty($_FILES['gallery']['name'][0])) {
            if (!file_exists('./upload/room/'))
                mkdir('./upload/room/', 0777, true);
            foreach ($_FILES['gallery']['name'] as $key => $val) {
                $upload_path = './upload/room/';
                $file_name = md5(random_string('alnum', 16));
                $file_ext = substr($_FILES['gallery']['name'][$key], strpos($_FILES['gallery']['name'][$key], "."));
                move_uploaded_file($_FILES['gallery']['tmp_name'][$key], $upload_path . $file_name . $file_ext);
                array_push($room['gallery'], $file_name . $file_ext);
            }
        }

        $room['gallery'] = json_encode($room['gallery']);*/
        foreach ($req as $key => $value) {
            if ($value == 'true') {
                $req[$key] = 1;
            }
        }

        if (isset($req['id']) && empty($req['id'] == ''))
            unset($req['id']);

        $result = $this->room_model->save('rooms', $req);
        $roomID = $this->db->insert_id();

        if (isset($req['id'])) $method = 'UPDATE';
        else $method = 'ADD';
        $this->notification->add($this->user_id, 'rooms', $method, $roomID, 'registered');

        //if success -> go to list
        $this->load_json(array(
            'state' => 'success'
        ));
    }
}