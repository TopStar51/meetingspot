<?php

class Base_Controller extends CI_Controller
{
    public $custom_css_list = array();
    public $custom_js_list = array();
    public $view_list = array();
    public $path = array();

    public $type;
    public $logged_in;
    public $user_id;
    public $user_name;

    public $user_table = 'users';
    public $page_table = 'pages';
    public $footer_table = 'footer_data';

	public function __construct()
	{
		parent::__construct();

        $this->load->model('Base_Model');
        $this->load->model('frontend_model');
        $this->load->model('notification_model', 'notification');
        
        $this->load->helper('gf_func_helper');

        $this->path[0] = strtolower($this->uri->segment(1));
        $this->path[1] = strtolower($this->uri->segment(2));
        $this->path[2] = strtolower($this->uri->segment(3));
        $this->path[3] = strtolower($this->uri->segment(4));

        $this->logged_in = false;
        if (!empty($this->session->userdata('logged_in')) && ($this->session->userdata('logged_in') == true)) 
        {
            $this->type = $this->session->userdata('type');
            $this->logged_in = $this->session->userdata('logged_in');
            $this->user_id = $this->session->userdata('id');
            $this->user_name = $this->session->userdata('first_name');
        }

        switch ($this->path[0]) {
            case 'admin':
                if ($this->path[1] != 'login' && ($this->type != ADMIN || $this->logged_in == false))
                    redirect('admin/login');
                break;
            case 'owner':
                if ($this->path[1] != 'login' && ($this->type != OWNER || $this->logged_in == false))
                    redirect('signin');
                break;
            case 'customer':
                if ($this->path[1] != 'login' && ($this->type != CUSTOMER || $this->logged_in == false))
                    redirect('signin');
                break;
            default:
                break;
        }

        if ($this->type == OWNER && $this->logged_in && $this->path[0] == 'owner') {
            if ($this->path[1] == 'login') return;
            //if user doesn't have location completed, then user must input a location
            $this->load->model('location_model', 'location');
            if (!$this->location->has_one($this->user_id) && ($this->path[1] != 'location' || $this->path[2] == 'index')) {
                $location = $this->location->mine($this->user_id);
                //alert user to complete location registration
                $this->session->set_flashdata('need_location_registration', true);
                if ($location) redirect('owner/location/edit/'.$location['id']);
                else redirect('owner/location/new_location');
            }
        }
	}

    public function __call($method, $arguments) {
        if ($method === 'load_css') {
            $this->custom_css_list[] = $arguments[0];
        } elseif ($method === 'load_js') {
            $this->custom_js_list[] = $arguments[0];
        } else {
            die("<p>" . $method . " doesn't exist</p>");
        }
    }

    protected function load_view($view_name, $content_data = array())
    {
        $content_data['path'] = $this->path;
        $content_data['user_name'] = $this->user_name;

        $content_data['type'] = $this->type;
        $content_data['menus'] = $this->Base_Model->get_list($this->page_table, 0, array('is_visible'=>'1'));

        $content_data['footer_data'] = $this->frontend_model->get_footer_data();

        $this->load->view('front/partial/header', $content_data);
        $this->load->view('front/'.$view_name.'/index', $content_data);
        $this->load->view('front/partial/footer', array('jslink'=>$view_name.'/jslink'));
    }

    protected function load_customer_view($view_name, $view_params = array())
    {
        $view_params['path'] = $this->path;
        $view_params['type'] = $this->type;
        $view_params['user_name'] = $this->user_name;
        $view_params['menus'] = $this->Base_Model->get_list($this->page_table, 0, array('is_visible'=>'1'));
        $view_params['pendingCount'] = $this->notification->pending_count($this->user_id);
        
        $this->view_list[] = array('front/customer/'.$view_name.'/index', $view_params);
        $view_params['footer_data'] = $this->frontend_model->get_footer_data();

        $this->load->view('front/partial/header', $view_params);
        $this->load->view('front/customer/layout', $view_params);
        $this->load->view('front/partial/footer', array('jslink'=>'customer/'.$view_name.'/jslink'));
    }

    protected function load_owner_view($view_name, $view_params = array())
    {
        $this->view_list[] = array('owner/'.$view_name.'/index', $view_params);

        $view_params['path'] = $this->path;
        $view_params['user_name'] = $this->user_name;
        $view_params['pendingCount'] = $this->notification->pending_count($this->user_id);
        $view_params['pendingFive'] = $this->notification->last_five($this->user_id);
        $view_params['menus'] = array(
            array(
                'icon' => 'flaticon-line-graph',
                'text' => 'Dashboard',
                'path' => 'dashboard'
            ),
            array(
                'icon' => 'flaticon-placeholder-1',
                'text' => 'My Locations',
                'path' => 'location'
            ),
            array(
                'icon' => 'flaticon-map',
                'text' => 'My Rooms',
                'path' => 'room'
            ),
            array(
                'icon' => 'flaticon-calendar',
                'text' => 'Reservations',
                'path' => 'reservation',
            ),
            array(
                'icon' => 'flaticon-interface-1',
                'text' => 'Reviews',
                'path' => 'review'
            ),
            array(
                'icon' => 'flaticon-music-2',
                'text' => 'Notifications',
                'path' => 'notification'
            )
            // array(
            //  'icon' => 'flaticon-line-graph',
            //  'text' => 'Messages',
            //  'path' => 'message'
            //)
        );
        $view_params['jslink'] = $view_name.'/jslink';
        $this->load->view('owner/partial/layout', $view_params);
    }

    protected function load_admin_view($view_name, $view_params = array()) {
        $this->view_list[] = array('admin/'.$view_name.'/index', $view_params);

        $view_params['path'] = $this->path;
        $view_params['pendingCount'] = $this->notification->pending_count();
        $view_params['pendingFives'] = $this->notification->last_five();
        $view_params['menus'] = array(
            array(
                'title' => 'Dashboard',
                'path' => 'dashboard',
                'icon' => 'fa fa-home'
            ),
            array(
                'title' => 'Owners',
                'path' => 'owner',
                'icon' => 'fa fa-user'
            ),
            array(
                'title' => 'Locations',
                'path' => 'location',
                'icon' => 'fa fa-map-marker'
            ),
            array(
                'title' => 'Rooms',
                'path' => 'room',
                'icon' => 'fa fa-map'
            ),
            array(
                'title' => 'Customers',
                'path' => 'customer',
                'icon' => 'fa fa-street-view'
            ),
            array(
                'title' => 'Groups',
                'path' => 'group',
                'icon' => 'fa fa-users'
            ),
            array(
                'title' => 'Reservations',
                'path' => 'reservation',
                'icon' => 'fa fa-ticket'
            ),
            array(
                'title' => 'Reviews',
                'path' => 'review',
                'icon' => 'fa fa-tags'
            ),
            array(
                'title' => 'Statistics',
                'path' => 'statistic',
                'icon' => 'fa fa-line-chart'
            ),
            array(
                'title' => 'Design',
                'path' => 'design',
                'icon' => 'fa fa-clone',
                'submenu' => array(
                    array(
                        'text' => 'List of Pages',
                        'path' => 'page'
                    ),
                    array(
                        'text' => 'Page Headers',
                        'path' => 'header'
                    ),
                    array(
                        'text' => 'Page Footer',
                        'path' => 'footer',
                        'submenu' => array(
                            array(
                                'text' => 'About Us Text',
                                'path' => 'about_us_text'
                            ), 
                            array(
                                'text' => 'Subscribe Text',
                                'path' => 'subscribe_text',
                            ),
                            array(
                                'text' => 'Contact Info',
                                'path' => 'contact_info'
                            ),
                            array(
                                'text' => 'Social Info',
                                'path' => 'social_info'
                            ),
                            array(
                                'text' => 'Footer Text',
                                'path' => 'footer_text'
                            )
                        )
                    )
                )
            ),
            array(
                'title' => 'Notifications',
                'path' => 'notification',
                'icon' => 'fa fa-bell'
            )
        );
        $view_params['jslink'] = $view_name.'/jslink';

        $this->load->view('admin/partial/layout', $view_params);
    }

    protected function user_session_create($user)
    {
        $this->session->set_userdata(array(
            'id' => $user['id'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'email' => $user['email'],
            'type' => $user['type'],
            'logged_in' => true
        ));
    }

    protected function retPostList($list, $filter_status = true)
    {
        $datatable = array_merge(array('pagination' => array(), 'sort' => array(), 'query' => array()), $_REQUEST);

        $data = $list;

        // search filter by keywords
        if ($filter_status) {
            $filter = isset($datatable['query']['generalSearch']) && is_string($datatable['query']['generalSearch'])
                ? $datatable['query']['generalSearch'] : '';
            if (!empty($filter)) {
                $data = array_filter($list, function ($a) use ($filter) {
                    return (boolean)preg_grep("/$filter/i", (array)$a);
                });
                unset($datatable['query']['generalSearch']);
            }
        }

        $page = !empty($datatable['pagination']['page']) ? (int)$datatable['pagination']['page'] : 1;
        $perpage = !empty($datatable['pagination']['perpage']) ? (int)$datatable['pagination']['perpage'] : -1;

        $pages = 1;
        $total = count($list);


        if ($perpage > 0) {
            $pages = ceil($total / $perpage); // calculate total pages
            $page = max($page, 1); // get 1 page when $_REQUEST['page'] <= 0
            $page = min($page, $pages); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ($page - 1) * $perpage;
            if ($offset < 0) {
                $offset = 0;
            }
            $data = array_slice($data, $offset, $perpage, true);
        }

        $meta = array(
            'page' => $page,
            'pages' => $pages,
            'perpage' => $perpage,
            'total' => $total,
        );
        $result = array(
            'meta' => $meta,
            'data' => $data,
        );

        echo json_encode($result, JSON_PRETTY_PRINT);
    }

	protected function load_json($data)
    {
        echo json_encode($data);
    }
}