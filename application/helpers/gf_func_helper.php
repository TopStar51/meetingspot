<?php

function gf_html_load_js($js)
{
    echo '<script src="'.base_url($js).'" type="text/javascript"></script>';
}

function gf_html_load_css($css)
{
    echo '<link rel="stylesheet" href="'.base_url($css).'">';
}

function getLatLong($address){
    $data = array();
    $data['latitude'] = '';
    $data['longitude'] = '';

    if(!empty($address)){
        //Formatted address
        $formattedAddr = str_replace(' ','+',$address);
        //Send request and receive json data by address
        $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false&key='.GOOGLE_MAP_API_KEY); 
        $output = json_decode($geocodeFromAddr);
        //Get latitude and longitute from json data
        $data['latitude']  = $output->results[0]->geometry->location->lat; 
        $data['longitude'] = $output->results[0]->geometry->location->lng;
        //Return latitude and longitude of the given address
    }

    return $data;
}

function notice_text($notification) {
    $itemName = '';
    //when users, check if owner or customer
    if ($notification['table_name'] == 'users') {
        if ($notification['method'] == 'ADD') {
            return 'New '.$notification['note'].' Registered';
        }
    }

    if ($notification['table_name'] == 'reservations') $itemName = 'Reservation';
    else if ($notification['table_name'] == 'locations') $itemName = 'Location';
    else if ($notification['table_name'] == 'rooms') $itemName = 'Room';

    return $notification['user'].' '.$notification['note'].' '.$itemName;
}

function notice_menu($notification) {
    if ($notification['table_name'] == 'users') {
        return strtolower($notification['note']);
    }
    return substr($notification['table_name'], 0, strlen($notification['table_name']) -1);
}
?>