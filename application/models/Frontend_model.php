<?php

class Frontend_model extends CI_Model
{
	private $tb_location = 'locations';
	private $tb_category = 'categories';
	private $tb_review = 'reviews';
	private $tb_reservation = 'reservations';
	private $tb_room = 'rooms';
	private $tb_user = 'users';
	private $tb_header = 'header_data';
	private $tb_footer = 'footer_data';
	private $tb_page = 'pages';

	function __construct() {
		parent::__construct();
	}

	function get_locations_by_search($location_name = '', $address_info = array(), $category_id = '', $limit = 0){
		$this->db->select('l.*, c.name as category, COUNT(r.id) as review_count, ROUND(AVG(r.rating)) as review_avg');
		$this->db->from($this->tb_location.' l');
		$this->db->join($this->tb_category.' c', 'c.id = l.category_id', 'left');
		$this->db->join($this->tb_review.' r', 'r.location_id = l.id', 'left');

		if(!empty($location_name))
			$this->db->like('l.name', $location_name);

		if(!empty($category_id))
			$this->db->where('category_id', $category_id);

		if(!empty($address_info))
			$this->db->where($address_info);

		if($limit > 0)
			$this->db->limit($limit);
		$this->db->where('l.status', ACTIVE);
		$this->db->where('l.is_delete', '0');

		//ToDo list
		//review status, is_delete consideration

		$this->db->group_by('l.id');
		$this->db->order_by('review_avg', 'DESC');
		
		return $this->db->get()->result_array();
	}

	function get_upcoming_reservations($location_id) {
		$this->db->select('res.*');
		$this->db->from($this->tb_reservation.' res');
		$this->db->join($this->tb_room.' r', 'r.id = res.room_id', 'left');

		$this->db->where('r.location_id', $location_id);
		$this->db->where('res.status', PENDING);
		$this->db->where('res.is_delete', '0');

		$this->db->where('start_time >', date('Y-m-d'));
		$this->db->order_by('start_time', 'ASC');
		return $this->db->get()->result_array();
	}

	function get_reviews_by_location($location_id, $limit = 0) {
		$this->db->select('r.*, CONCAT(u.first_name, " ", u.last_name) as customer');
		$this->db->from($this->tb_review.' r');
		$this->db->join($this->tb_user.' u', 'u.id = r.customer_id', 'left');

		$this->db->where('r.is_delete', '0');
		$this->db->where('r.status', ACTIVE);
		$this->db->where('r.location_id', $location_id);
		if($limit > 0)
			$this->db->limit($limit, 0);
		$this->db->order_by('r.rating', 'DESC');

		return $this->db->get()->result_array();
	}

	function get_header_data($page_name) {
		$this->db->select('*');
		$this->db->from($this->tb_header.' h');
		$this->db->join($this->tb_page.' p', 'p.id = h.page_id', 'left');

		$this->db->where('h.is_delete', '0');
		$this->db->where('p.is_delete', '0');

		$this->db->where('p.name', $page_name);

		return $this->db->get()->row_array();
	}

	function get_footer_data() {
		$this->db->select('*');
		$this->db->from($this->tb_footer);
		$this->db->where('is_delete', '0');
		$result = $this->db->get()->result_array();

		$data = array();
		foreach ($result as $key => $value) {
			$data[$value['type']] = $value['data'];
		}

		return $data;
	}
}