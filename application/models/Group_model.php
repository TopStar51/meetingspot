<?php

class Group_model extends Base_Model
{

    private function prepare_get($params)
    {
        $this->db->select('g.id')
            ->select('CONCAT(first_name, " ", last_name) as customer')
            ->select('group_name')
            ->select('g.address')
            ->select('g.phone')
            ->select('g.website')
            ->select('g.facebook')
            ->select('g.created_at')
            ->from('groups g')
            ->join('users u', 'u.id = g.user_id', 'left')
            ->where('g.is_delete', '0')
            ->where('u.is_delete', '0');
        if (!empty($params['s_search'])) {
            $this->db->group_start();
            foreach (array('group_name', 'address') as $likeField) {
                $this->db->or_like($likeField, $params['s_search'], 'both');
            }
            $this->db->group_end();
        }

        /*if (isset($params['status']) && $params['status'] != '') {
            $this->db->where('status', $params['status']);
        }*/
    }

    /*function get_customer_info($username)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->group_start();
        $this->db->where('email', $username)
                ->or_where('username', $username);
        $this->db->group_end();
        $this->db->where('type', CUSTOMER);
        $this->db->where('is_delete', '0');
        return $this->db->get()->row_array();
    }*/

    function get_table_data($params)
    {
        $this->prepare_get($params);
        if (isset($params['length'])) {
            $this->db->limit($params['length'], $params['start']);
        }

        $groups = $this->db->get()->result_array();
        $result = array();
        $row = $params['start'];
        foreach ($groups as $group) {
            $record = array();
            $row += 1;//row number

            //$record[] = $row;
            foreach ($group as $key => $value) {
                if($key == 'id') continue;
                /*if ($key == 'status') {
                    if ($value == INACTIVE) {
                        $value = '<span style="color: red">Inactive</span>';
                    } else {
                        $value = '<span style="color: green">Active</span>';
                    }
                }*/
                $record[] = $value;
            }
            $record[] = '<a href="' . site_url('admin/group/edit/' . $group['id']) . '" class="btn btn-sm green table-button">Edit</a>
                        <div style="margin: 5px;"></div>
                        <button onclick="delete_group(' . $group['id'] . ')" class="btn btn-sm red table-button">Delete</button>';
            $result[] = $record;
        }

        return $result;
    }

    function get_table_count($params)
    {
        $this->prepare_get($params);
        return $this->db->count_all_results();
    }

/*    function prepare_statistic($month)
    {
        $this->db->select('count(1) as count');
        $this->db->select('DATE_FORMAT(created_at, \'%Y-%m-%d\') as date');
        $this->db->from('users');
        $this->db->where('created_at < STR_TO_DATE("'.$month.'-01", "%Y-%m-%d") + INTERVAL 1 MONTH');
        $this->db->where('created_at >= STR_TO_DATE("'.$month.'-01", "%Y-%m-%d")');
        $this->db->group_by('DATE_FORMAT(created_at, \'%Y-%m-%d\')');
        $this->db->order_by('created_at');

        return $this->db->get()->result_array();
    }*/

/*    function get_statistic($month)
    {
        $reslist = $this->prepare_statistic($month);
        $result = array();

        $begindate = strtotime($month . '-01');
        $i = 0;

        for ($date = $begindate; $date < strtotime('+ 1 month', $begindate) && $date < time(); $date = strtotime('+ 1 day', $date)) {

            $strtime = date('Y-m-d', $date);

            if ($i < count($reslist) && $reslist[$i]['date'] == $strtime) {
                $result[] = array('date' => $strtime, 'count' => $reslist[$i]['count']);
                $i ++;
            } else {
                $result[] = array('date' => $strtime, 'count' => 0);
            }
        }

        return $result;
    }

    function check_password($user_id, $password) {
        $condition = array(
            'id' => $user_id,
            'password' => md5($password)
        );
        $userInfo = $this->get_info_with_where('users', $condition);
        if (!$userInfo) {
            return false;
        }
        return true;
    }

    function update_password($user_id, $password) {
        $updateInfo = array(
            'id' => $user_id,
            'password' => md5($password)
        );
        $this->update_info('users', $updateInfo);
    }*/
}