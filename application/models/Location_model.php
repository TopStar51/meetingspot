<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:18 AM
 */

class Location_model extends Base_Model
{
    public $_table;
    public $_table_catagory;

    public function __construct()
    {
        parent::__construct();
        $this->_table = 'locations';
        $this->_table_catagory = 'categories';
        $this->_table_review = 'reviews';
    }

    function get_all_locations($owner = 0, $where = array(), $limit = 0, $start = 0) {

        $this->db->select('id, location_id, rating')->from($this->_table_review)->where('is_delete', '0')->where('status', ACTIVE);

        $category_join = '('.$this->db->get_compiled_select().')';
        $this->db->select('@pos :=@pos + 1 pos', false);
        $this->db->select('a.*');
        $this->db->select('b.name category');
        $this->db->select('COUNT(c.id) as review_count, ROUND(AVG(c.rating)) as review_avg');

        $this->db->from('(SELECT @pos := 0) r');
        $this->db->from($this->_table . ' a');
        $this->db->join($this->_table_catagory . ' b', 'a.category_id = b.id', 'left');
        $this->db->join($category_join.' c', 'c.location_id = a.id', 'left');
        
        if ($owner > 0)
            $this->db->where('a.owner_id', $owner);
        if (!empty($where))
            $this->db->where($where);

        $this->db->where('a.is_delete', '0');
        $this->db->where('b.is_delete', '0');

        $this->db->group_by('a.id');
        $this->db->order_by('review_avg', 'desc');

        if ($limit > 0)
            $this->db->limit($limit, $start);
//        exit($this->db->get_compiled_select());
        return $this->db->get()->result_array();
    }

    function get_location_count($status = '', $owner = 0) {
        $this->db->select('count(1) count');

        $this->db->from($this->_table);

        if ($status != '')
            $this->db->where('status', $status);

        if ($owner > 0)
            $this->db->where('owner_id', $owner);

        $this->db->where('is_delete', '0');

        return ($this->db->get()->row_array())['count'];
    }

    private function prepare_get($params) {
        $owner_join = 'users';
//        if (isset($params['s_search'])) {
//            $params['owner'] = $params['s_search'];
//        }
        if (!empty($params['owner'])) {
            $this->db->select('id')
                ->select('CONCAT(first_name, " ", last_name) as owner, email')
                ->from('users')
                ->where('type', OWNER)
                ->like('owner_name', $params['owner'], 'both');
            $owner_join = '('.$this->db->get_compiled_select().')';
//            exit($owner_join);
        }

        $category_join = 'categories';
        if (!empty($params['category'])) {
            $this->db->select('id')
                ->select('name')
                ->from('categories')
                ->like('name', $params['category'], 'both');
            $category_join = '('.$this->db->get_compiled_select().')';
        }

        $this->db->select('l.id')
            ->select('l.thumb_image')
            ->select('l.name')
            ->select('CONCAT(o.first_name, " ", o.last_name) as owner')
            ->join($owner_join.' o', 'o.id = l.owner_id')
            ->join($category_join.' c', 'c.id = l.category_id')
            ->select('c.name as category')
            ->select('l.email')
            ->select('l.phone')
            ->select('l.city')
            ->select('l.state')
            ->select('l.created_at')
            ->select('l.status')
            ->from('locations l')
            ->where('l.is_delete', '0');

        if (!empty($params['s_search'])) {
            $this->db->group_start();
            foreach (array('first_name', 'last_name', 'l.email', 'l.name') as $likeField) {
                $this->db->or_like($likeField, $params['s_search'], 'both');
            }
            $this->db->group_end();
        }

        if (isset($params['status']) && $params['status'] != '') {
            $this->db->where('status', $params['status']);
        }

        //  exit($this->db->get_compiled_select());
    }

    function get_table_data($params) {
        $this->prepare_get($params);
        if (isset($params['length'])) {
            $this->db->limit($params['length'], $params['start']);
        }

        $locations = $this->db->get()->result_array();
        $result = array();
        $row = $params['start'];
        foreach ($locations as $location) {
            $record = array();
            $row += 1;//row number

            // $record[] = $row;
            foreach ($location as $key => $value) {
                if ($key == 'id') continue;
                if ($key == 'status') {
                    if ($value == INACTIVE) {
                        $value = '<span style="color: red">Pending</span>';
                    } else {
                        $value = '<span style="color: green">Approved</span>';
                    }
                } else if ($key == 'thumb_image') {
                    $value = '<img src="'.base_url('upload/location/'.$value).'" class="img-pic">';
                }
                $record[] = $value;
            }
            $record[] = '<a href="'.site_url('admin/location/edit/'.$location['id']).'" class="btn btn-sm green table-button">Edit</a>
                        <div style="margin: 5px;"></div>
                        <button onclick="delete_location('.$location['id'].')" class="btn btn-sm red table-button">Delete</button>';

            $result[] = $record;
        }

        return $result;
    }

    function get_table_count($params) {
        $this->prepare_get($params);
        return $this->db->count_all_results();
    }

    function get_detail($locationID) {
        $this->prepare_get(array());
        $this->db->select('l.email');
        $this->db->select('l.address');
        $this->db->select('l.owner_id');
        $this->db->select('l.category_id');
        $this->db->select('l.website_url');
        $this->db->select('l.menu_url');
        $this->db->select('l.zip');
        $this->db->select('l.latitude');
        $this->db->select('l.longitude');
        $this->db->select('l.video');
        $this->db->select('l.description');
        $this->db->select('l.gallery');
        $this->db->select('l.water, l.soft_drink, l.alcoholic_drink, l.mandatory_order');
        $this->db->where('l.id', $locationID);
        return $this->db->get()->row_array();
    }

    function save_location($data) {

        unset($data['open_hour']);
        unset($data['close_hour']);
        unset($data['amenity']);

        if (isset($data['id']) && $data['id'] == '')
            unset($data['id']);

        return $this->save($this->_table, $data);
    }

    function delete($id, $owner) {
        $this->db->where('owner_id', $owner);
        $this->db->where('id', $id);

        $this->db->set('is_delete', '1');

        return $this->db->update($this->_table);
    }

    function get_location_by_id($id, $owner = 0) {
        $this->db->select('a.*');

        $this->db->from($this->_table . ' a');
            $this->db->join($this->_table_catagory . ' b', 'a.category_id = b.id', 'left');
        $this->db->select('b.name category');
            $this->db->join('users u', 'u.id = a.owner_id');
        $this->db->select('u.last_name owner');
        $this->db->where('a.id', $id);
        // $this->db->where('a.owner_id', $owner);

        if ($owner > 0)
            $this->db->where('a.owner_id', $owner);

        $this->db->where('a.is_delete', '0');
        $this->db->where('b.is_delete', '0');

        return $this->db->get()->row_array();
    }

    function working_hours($locationID) {
        $result = $this->get_list('working_hours', 0, array('location_id' => $locationID));
        $working_hours = array();
        foreach ($result as $working_hour) {
            $working_hours[$working_hour['weekday']] = $working_hour;
        }
        return $working_hours;
    }

    function save_working_hour($info) {
        //update if working hour exists, else insert it
        $oldInfo = $this->get_info_with_where('working_hours', array('weekday' => $info['weekday'], 'location_id' => $info['location_id']));
        if (!empty($oldInfo)) {
            $info['id'] = $oldInfo['id'];
        }

        $this->save('working_hours', $info);
    }

    function prepare_statistic($month)
    {
        $this->db->select('count(1) as count');
        $this->db->select('DATE_FORMAT(created_at, \'%Y-%m-%d\') as date');
        $this->db->from('locations');
        $this->db->where('created_at < STR_TO_DATE("'.$month.'-01", "%Y-%m-%d") + INTERVAL 1 MONTH');
        $this->db->where('created_at >= STR_TO_DATE("'.$month.'-01", "%Y-%m-%d")');
        $this->db->group_by('DATE_FORMAT(created_at, \'%Y-%m-%d\')');
        $this->db->order_by('DATE_FORMAT(created_at, \'%Y-%m-%d\')');

        return $this->db->get()->result_array();
    }

    function get_statistic($month)
    {
        $reslist = $this->prepare_statistic($month);
        $result = array();

        $begindate = strtotime($month . '-01');
        $i = 0;

        for ($date = $begindate; $date < strtotime('+ 1 month', $begindate) && $date < time(); $date = strtotime('+ 1 day', $date)) {

            $strtime = date('Y-m-d', $date);

            if ($i < count($reslist) && $reslist[$i]['date'] == $strtime) {
                $result[] = array('date' => $strtime, 'count' => $reslist[$i]['count']);
                $i ++;
            } else {
                $result[] = array('date' => $strtime, 'count' => 0);
            }
        }

        return $result;
    }

    function has_one($ownerID) {
        $completedCount = $this->db->where('owner_id', $ownerID)->where('progress',  11)->from('locations')->count_all_results();
        return ($completedCount > 0);
    }

    function mine($ownerID) {
        return $this->db->where('owner_id', $ownerID)->get('locations')->row_array();
    }
}