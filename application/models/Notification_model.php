<?php
/**
 * Created by PhpStorm.
 * User: Elvis
 * Date: 2019-03-25
 * Time: 오후 6:29
 */

class Notification_model extends Base_Model
{
    function last_five($whom = 0) {
        //if it's admin, $whom = 0
        $this->prepare_get($whom);
        $this->db->where('is_read', 0);
        $this->db->limit(5);
        return $this->db->get()->result_array();
    }

    function prepare_get($whom = 0) {
        $this->db->select('n.*')
            ->select('CONCAT(u.first_name, \' \', u.last_name) user')
            ->where('to_whom', $whom)
            ->from('notifications n')
            ->join('users u', 'u.id = n.user_id')
            ->order_by('id', 'desc');
    }

    function add($user_id, $table_name, $method, $item_id, $note, $to_whom = 0) {
        //to whom is 0 when it's admin's notification
        $notification = array(
            'user_id' => $user_id,
            'table_name' => $table_name,
            'method' => $method,
            'item_id' => $item_id,
            'to_whom' => $to_whom,//who should get noticed? if owner's reservation is modified, this is owner_id
            'note' => $note,
            'created_at' => date('Y-m-d H:i:s')
        );
        $this->db->insert('notifications', $notification);
    }

    function check_read($notice_id) {
        $this->db->update('notifications', array('is_read' => 1), array('id' => $notice_id));
    }

    function pending_count($whom = 0) {
        return $this->db->where('to_whom', $whom)
            ->where('is_read', 0)
            ->from('notifications')->count_all_results();
    }

    function read_all($whom = 0) {
        $this->db->set('is_read', 1)->where('to_whom', $whom)->update('notifications');
    }

    function admin_table_data($params) {
//        $this->db->select('@pos :=@pos + 1 pos', false);
        $this->prepare_get($params['whom']);
        if (isset($params['length'])) {
            $this->db->limit($params['length'], $params['start']);
        }

        $notifications = $this->db->get()->result_array();
        $records = array();
        $row = $params['start'];
        foreach ($notifications as $notification) {
            $row += 1;
            $records[] = array(
                $row,
                notice_text($notification),
                '<a href="'.site_url('admin/').notice_menu($notification).'" class="btn btn-sm btn-info">Show</a>',
                date('Y-m-d H:i', strtotime($notification['created_at']))
            );
        }

        return $records;
    }

    function get_count($whom = 0) {
        $this->db->where('to_whom', $whom);
        $this->db->from('notifications');
        return $this->db->count_all_results();
    }

    function owner_table_data($params) {
        $this->prepare_get($params['whom']);

        $notifications = $this->db->get()->result_array();
        $row = 0;
        foreach ($notifications as &$notification) {
            $row +=1;
            $notification['pos'] = $row;
            $notification['action'] = '<a href="'.site_url('owner/').notice_menu($notification).'" class="btn btn-sm btn-info">Show</a>';
            $notification['content'] = notice_text($notification);
            $notification['created_at'] = date('Y-m-d H:i', strtotime($notification['created_at']));
        }

        return $notifications;
    }

    function get_notifications($whom, $start, $length) {
        $this->prepare_get($whom);
        $this->db->limit($length, $start);

        $notifications = $this->db->get()->result_array();
        foreach ($notifications as &$notification) {
            $notification['url'] = site_url('customer/').notice_menu($notification);
            $notification['content'] = notice_text($notification);
        }

        return $notifications;
    }
}
