<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:18 AM
 */

class Owner_model extends Base_Model
{
    public $_table;

    public function __construct()
    {
        parent::__construct();
        $this->_table = 'users';
    }

    /**
     * @param $params
     */
    public function user_sign_up($params){
        //check if same email exists
        $dupExist = $this->db->select('id')->from($this->_table)->where('email', $params['email'])->get()->row_array();

        if ($dupExist) return 'Email duplicated';

        $this->db->set($params);
        $this->db->insert($this->_table);

        $owner_id = $this->db->insert_id();

        if ($params['type'] == OWNER)
            $notes = 'Owner';
        else
            $notes = 'User';
        $this->notification->add(0, 'users', 'ADD', $owner_id, $notes);

        return 'success';
    }

    public function get_owner_info_with_where($where) {
        $this->db->group_start();
        $this->db->where('username', $where['userid']);
        $this->db->or_where('email', $where['userid']);
        unset($where['userid']);
        $this->db->group_end();
        return $this->get_info_with_where($this->_table, $where);
    }

    private function prepare_get($params) {
        $this->db->select('id')
            ->select('first_name')
            ->select('last_name')
            ->select('email')
            ->select('phone')
            ->select('created_at')
            ->select('status')
            ->from('users')
            ->where('type', OWNER)
            ->where('is_delete', '0');

        if (!empty($params['s_search'])) {
            $this->db->group_start();
            foreach (array('first_name', 'last_name', 'email') as $likeField) {
                $this->db->or_like($likeField, $params['s_search'], 'both');
            }
            $this->db->group_end();
        }

        if (isset($params['status']) && $params['status'] != '') {
            $this->db->where('status', $params['status']);
        }
    }

    function get_table_data($params) {
        $this->prepare_get($params);
        if (isset($params['length'])) {
            $this->db->limit($params['length'], $params['start']);
        }

        $owners = $this->db->get()->result_array();
        $result = array();
        $row = $params['start'];
        foreach ($owners as $owner) {
            $record = array();
            $row += 1;//row number

//            $record[] = $row;
            foreach ($owner as $key => $value) {
                if ($key == 'id') continue;
                if ($key == 'status') {
                    if ($value == INACTIVE) {
                        $value = '<span style="color: red">Pending</span>';
                    } else {
                        $value = '<span style="color: green">Approved</span>';
                    }
                }
                $record[] = $value;
            }
            $record[] = '<a href="'.site_url('admin/owner/edit/'.$owner['id']).'" class="btn btn-sm green table-button">Edit</a>
                        <div style="margin: 5px;"></div>
                        <button onclick="delete_owner('.$owner['id'].')" class="btn btn-sm red table-button">Delete</button>';

            $result[] = $record;
        }

        return $result;
    }

    function get_table_count($params) {
        $this->prepare_get($params);
        return $this->db->count_all_results();
    }

}