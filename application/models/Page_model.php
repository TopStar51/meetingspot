<?php

class Page_model extends Base_Model
{
	private function prepare_get($params)
    {
    	$this->db->select('id, name, menu_name, url, is_visible');
		$this->db->from('pages');
		$this->db->where('is_delete', '0');
		if (!empty($params['s_search'])) {
            $this->db->group_start();
            foreach (array('name', 'menu_name') as $likeField) {
                $this->db->or_like($likeField, $params['s_search'], 'both');
            }
            $this->db->group_end();
        }
    }

	function get_table_data($params)
	{

		$this->prepare_get($params);
        if (isset($params['length'])) {
            $this->db->limit($params['length'], $params['start']);
        }

        $pages = $this->db->get()->result_array();
        $result = array();
        $row = $params['start'];
        foreach ($pages as $key => $page) {
        	$record = array();
        	$row +=1;
        	foreach ($page as $key => $value) {
        		switch ($key) {
        			case 'is_visible':
        				if ($value == INACTIVE) {
	                        $value = '<span style="color: red">Invisible</span>';
	                    } else {
	                        $value = '<span style="color: green">Visible</span>';
	                    }
        				break;
        			case 'menu_name':
        				$value = '<span class="label label-md label-success">'.$value.'</span>';
        				break;
        			case 'url':
        				$value = '<a href="'.$value.'" target="_blank">'.$value.'</a>';
        				break;
        			default:
        				break;
        		}
        		$record[] = $value;
        	}

        	$record[] = '<a class="btn btn-sm green table-button btn_edit_page" data-id="'.$page['id'].'" data-name="'.$page['name'].'" data-menu_name="'.$page['menu_name'].'" data-url="'.$page['url'].'" data-is_visible="'.$page['is_visible'].'">Edit</a><div style="margin: 5px;"></div>
                        <button onclick="delete_page('.$page['id'].')" class="btn btn-sm red table-button">Delete</button>';

            $result[] = $record;
        }

        return $result;
	}

	function get_table_count($params)
    {
        $this->prepare_get($params);
        return $this->db->count_all_results();
    }
}