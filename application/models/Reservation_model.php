<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:18 AM
 */

class Reservation_model extends Base_Model
{
    public $_table;
    public $_table_location;
    public $_table_user;
    public $_table_category;
    public $_table_room;

    public function __construct()
    {
        parent::__construct();
        $this->_table = 'reservations';
        $this->_table_location = 'locations';
        $this->_table_room = 'rooms';
        $this->_table_user = 'users';
        $this->_table_category = 'categories';
    }

    private function prepare_get($params)
    {
        $this->db->select('r.id')
            ->select('r.res_number')
            ->join('users c', 'c.id = r.user_id')
            ->select('c.first_name user')
            ->join('rooms ro', 'ro.id = r.room_id')
            ->join('locations l', 'l.id = ro.location_id')
            ->select('l.name location')
            ->select('ro.name room')
            ->select('r.guest_number')
            ->select('r.res_date')
            ->select('r.start_time')
            ->select('r.end_time')
            ->select('r.status')
            ->from('reservations r')
            ->where('r.is_delete', '0');

        if (!empty($params['s_search'])) {
            $this->db->group_start();
            foreach (array('c.first_name', 'l.name') as $likeField) {
                $this->db->or_like($likeField, $params['s_search'], 'both');
            }
            $this->db->group_end();
        }

        if (isset($params['status']) && $params['status'] != '') {
            $this->db->where('status', $params['status']);
        }

        // exit($this->db->get_compiled_select());
    }

    function get_table_data($params)
    {
        $this->prepare_get($params);
        if (isset($params['length'])) {
            $this->db->limit($params['length'], $params['start']);
        }

        $reservations = $this->db->get()->result_array();
        $result = array();
        $row = $params['start'];
        foreach ($reservations as $reservation) {
            $record = array();
            $row += 1;//row number

            //            $record[] = $row;
            foreach ($reservation as $key => $value) {
                if ($key == 'id') continue;
                if ($key == 'status') {
                    if ($value == INACTIVE) {
                        $value = '<span style="color: red">Pending</span>';
                    } else {
                        $value = '<span style="color: green">Confirmed</span>';
                    }
                }
                $record[] = $value;
            }
            $record[] = '<a href="' . site_url('admin/reservation/detail/' . $reservation['id']) . '" class="btn btn-sm green table-button">Detail</a>
                    <div style="margin: 5px;"></div>
                    <button onclick="delete_reservation(' . $reservation['id'] . ')" class="btn btn-sm red table-button">Delete</button>';

            $result[] = $record;
        }

        return $result;
    }

    function get_table_count($params)
    {
        $this->prepare_get($params);
        return $this->db->count_all_results();
    }

    function get_detail($reservationID)
    {
        $this->prepare_get(array());
        $this->db->where('r.id', $reservationID);
        return $this->db->get()->row_array();
    }

    function get_all_reservation($query)
    {
//        $reservationStatus = array('PENDING' => '0', 'CONFIRMED' => '1', 'CANCELLED' => '2');
        $this->db->select('@pos :=@pos + 1 pos', false);
        $this->db->select('b.first_name customer');
        $this->db->select('a.*');
        $this->db->select('c.name room, l.name location');

        $this->db->from('(SELECT @pos := 0) r');
        $this->db->from($this->_table . ' a');
        $this->db->join($this->_table_user . ' b', 'a.user_id = b.id', 'left');
        $this->db->join($this->_table_room . ' c', 'a.room_id = c.id', 'left');
        $this->db->join($this->_table_location. ' l', 'l.id = c.location_id', 'left');

        if (isset($query['owner'])) {
            $this->db->where('l.owner_id', $query['owner']);
        }
        if (isset($query['customer'])) {
            $this->db->where('a.user_id', $query['customer']);
        }
        if (isset($query['status'])) {
            $this->db->where('a.status', $query['status']);
        }
        //$this->db->where('a.status', $reservationStatus[$status]);

        //        if ($owner > 0)
        //            $this->db->where('c.user_id', $owner);

        $this->db->where('a.is_delete', '0');
        $this->db->where('b.is_delete', '0');
        $this->db->where('c.is_delete', '0');
        $this->db->order_by('a.status', 'ASC');
        return $this->db->get()->result_array();
    }

    function get_reservation_by_id($id, $owner = 0)
    {

        $this->db->select('a.*, c.name as room_name');
        $this->db->select('b.first_name customer');
        $this->db->select('b.email email');
        $this->db->select('b.phone phone');
        $this->db->select('l.name location');
        $this->db->select('b.address');
        $this->db->select('d.first_name owner');
        $this->db->select('e.name category');

        $this->db->from($this->_table . ' a');
        $this->db->join($this->_table_user . ' b', 'a.user_id = b.id', 'left');
        $this->db->join($this->_table_room . ' c', 'a.room_id = c.id', 'left');
        $this->db->join($this->_table_location.' l', 'l.id = c.location_id');
        $this->db->join($this->_table_user . ' d', 'l.owner_id = d.id', 'left');
        $this->db->join($this->_table_category . ' e', 'l.category_id = e.id', 'left');

        //        if ($owner > 0)
        //            $this->db->where('c.user_id', $owner);

        $this->db->where('a.is_delete', '0');
        $this->db->where('b.is_delete', '0');
        $this->db->where('c.is_delete', '0');
        // $this->db->where('d.is_delete', '0');
        $this->db->where('e.is_delete', '0');
        $this->db->where('a.id', $id);

        return $this->db->get()->row_array();
    }

    private function check_mine($id, $ownerID) {
        $reservationData = $this->get_info($this->_table, $id);

        if (!$reservationData) {
            return array(
                'status' => false,
                'msg' => 'Reservation doesn\'t exist'
            );
        }
        //check if it's user's location's reservation
        $ownerData = $this->db->select('owner_id')->from('locations l')->join('rooms r', 'r.location_id = l.id', 'right')->where('r.id', $reservationData['room_id'])->get()->row_array();
        if ($ownerData['owner_id'] != $ownerID) {
            return array(
                'status' => false,
                'msg' => "It's not your reservation"
            );
        }

        return array(
            'status' => true,
            'data' => $reservationData
        );
    }

    function approve_reservation($id, $ownerID)
    {
        $check = $this->check_mine($id, $ownerID);
        if (!$check['status']) {
            return $check['msg'];
        }
        $reservationData = $check['data'];

        $this->db->where('id', $id);
        $this->db->set('status', CONFIRMED);
        $this->db->update($this->_table);

        $this->notification->add($ownerID, 'reservations', 'UPDATE', $id, 'approved', $reservationData['user_id']);

        return 'success';
    }

    function reject($id, $ownerID) {
        $check = $this->check_mine($id, $ownerID);
        if (!$check['status']) {
            return $check['msg'];
        }
        $reservationData = $check['data'];

        $this->db->update($this->_table, array('status' => CANCELLED), array('id' => $id));

        $this->notification->add($ownerID, 'reservations', 'UPDATE', $id, 'rejected', $reservationData['user_id']);

        return 'success';
    }

    function get_reservation_count($status = '', $owner = 0)
    {
        $this->db->from($this->_table . ' a');
        $this->db->join($this->_table_room . ' b', 'a.room_id = b.id');
        $this->db->join($this->_table_location. ' c', 'c.id = b.location_id');

        if ($status != '')
            $this->db->where('a.status', $status);

        if ($owner > 0)
            $this->db->where('c.owner_id', $owner);

        $this->db->where('a.is_delete', '0');
        $this->db->where('b.is_delete', '0');
        $this->db->where('c.is_delete', '0');

        return $this->db->count_all_results();
    }

    function prepare_statistic($month)
    {
        $this->db->select('count(1) as count');
        $this->db->select('DATE_FORMAT(created_at, \'%Y-%m-%d\') as date');
        $this->db->from('reservations');
        $this->db->where('created_at < STR_TO_DATE("'.$month.'-01", "%Y-%m-%d") + INTERVAL 1 MONTH');
        $this->db->where('created_at >= STR_TO_DATE("'.$month.'-01", "%Y-%m-%d")');
        $this->db->group_by('DATE_FORMAT(created_at, \'%Y-%m-%d\')');
        $this->db->order_by('DATE_FORMAT(created_at, \'%Y-%m-%d\')');

        return $this->db->get()->result_array();
    }

    function get_statistic($month)
    {
        $reslist = $this->prepare_statistic($month);
        $result = array();

        $begindate = strtotime($month . '-01');
        $i = 0;

        for ($date = $begindate; $date < strtotime('+ 1 month', $begindate) && $date < time(); $date = strtotime('+ 1 day', $date)) {

            $strtime = date('Y-m-d', $date);

            if ($i < count($reslist) && $reslist[$i]['date'] == $strtime) {
                $result[] = array('date' => $strtime, 'count' => $reslist[$i]['count']);
                $i ++;
            } else {
                $result[] = array('date' => $strtime, 'count' => 0);
            }
        }

        return $result;
    }

    function get_reservations($uno, $start, $displayLength) {
        $this->prepare_get(array());
        $this->db->join('groups g', 'g.id = r.group_id', 'left');
        $this->db->select('g.group_name group');
        $this->db->select('r.created_at');
        $this->db->select('ro.name room');
        $this->db->select('ro.location_id');
        $this->db->where('r.user_id', $uno);
        $this->db->limit($displayLength, $start);
        $this->db->order_by('r.created_at', 'desc');
        return $this->db->get()->result_array();
    }

    function get_reservation_detail($res_id) {
        $this->db->select('*');
        $this->db->from($this->_table.' res');
        $this->db->join($this->_table_room.' ro', 'ro.id = res.room_id', 'left');
        $this->db->where('res.id', $res_id);

        return $this->db->get()->row_array();
    }

    function check_reservation_by_date($res_date, $start_time, $end_time)
    {
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->where('res_date', $res_date);
        $this->db->group_start();
            $this->db->group_start();
                $this->db->where('start_time >=', $start_time);
                $this->db->where('start_time <', $end_time);
            $this->db->group_end();
            $this->db->or_group_start();
                $this->db->where('end_time >', $start_time);
                $this->db->where('end_time <=', $end_time);
            $this->db->group_end();
        $this->db->group_end();
        $result = $this->db->get()->result_array();
        return !empty($result) ? true : false;
    }

    function save_reservation($reservation) {
        if (isset($reservation['id'])) {
            $this->update_info('reservations', $reservation);
            return $reservation['id'];
        } else {
            $this->save_info('reservations', $reservation);
            return $this->db->insert_id();
        }
    }
}