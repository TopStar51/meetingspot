<?php
/**
 * Created by PhpStorm.
 * User: Elvis
 * Date: 21/02/2019
 * Time: 03:03
 */

class Review_model extends Base_Model
{
    public $_table;
    public $_table_user;
    public $_table_location;

    public function __construct()
    {
        parent::__construct();
        $this->_table = 'reviews';
        $this->_table_user = 'users';
        $this->_table_location = 'locations';
    }

    function get_detail($reviewID) {
        return
            $this->db->from('reviews r')
            ->join('locations l', 'l.id = r.location_id')
            ->join('users c', 'c.id = r.customer_id')
            ->select('l.name location')
            ->select('r.res_number')
            ->select('c.first_name author')
            ->select('r.rating')
            ->select('r.comment')
            ->select('r.status')
            ->where('r.id', $reviewID)
            ->get()->row_array();
    }

    private function prepare_get($params) {
        $this->db->select('r.id')
            ->select('r.res_number')
            ->join('users c', 'c.id = r.customer_id')
            ->select('c.first_name customer')
            ->join('locations l', 'l.id = r.location_id')
            ->select('l.name location')
            ->select('r.rating')
            ->select('r.status')
            ->from('reviews r')
            ->where('r.is_delete', '0');

        if (!empty($params['s_search'])) {
            $this->db->group_start();
            foreach (array('c.first_name', 'l.name') as $likeField) {
                $this->db->or_like($likeField, $params['s_search'], 'both');
            }
            $this->db->group_end();
        }

        if (isset($params['status']) && $params['status'] != '') {
            $this->db->where('status', $params['status']);
        }
    }

    function get_table_data($params) {
        $this->prepare_get($params);
        if (isset($params['length'])) {
            $this->db->limit($params['length'], $params['start']);
        }

        $reviews = $this->db->get()->result_array();
        $result = array();
        $row = $params['start'];
        foreach ($reviews as $review) {
            $record = array();
            $row += 1;//row number

//            $record[] = $row;
            foreach ($review as $key => $value) {
                if ($key == 'id') continue;
                if ($key == 'status') {
                    if ($value == INACTIVE) {
                        $value = '<span style="color: red">Pending</span>';
                    } else {
                        $value = '<span style="color: green">Confirmed</span>';
                    }
                }
                $record[] = $value;
            }
            $record[] = '<a href="'.site_url('admin/review/detail/'.$review['id']).'" class="btn btn-sm green table-button">Detail</a>
                    <div style="margin: 5px;"></div>
                    <button onclick="delete_review('.$review['id'].')" class="btn btn-sm red table-button">Delete</button>';

            $result[] = $record;
        }

        return $result;
    }


    function get_table_count($params) {
        $this->prepare_get($params);
        return $this->db->count_all_results();
    }

    function get_all_review($owner = 0) {

        $this->db->select('@pos :=@pos + 1 pos', false);
        $this->db->select('a.*');
        $this->db->select('b.first_name author');
        $this->db->select('c.name location');

        $this->db->from('(SELECT @pos := 0) r');
        $this->db->from($this->_table . ' a');
        $this->db->join($this->_table_user . ' b', 'a.customer_id = b.id', 'left');
        $this->db->join($this->_table_location . ' c', 'a.location_id = c.id', 'left');

        if ($owner > 0)
            $this->db->where('c.owner_id', $owner);

        $this->db->where('a.is_delete', '0');
        $this->db->where('b.is_delete', '0');
        $this->db->where('c.is_delete', '0');

        return $this->db->get()->result_array();
    }

    function get_review_by_id($id, $owner = 0) {
        $this->db->select('a.*');
        $this->db->select('b.first_name author');
        $this->db->select('c.name location');

        $this->db->from($this->_table . ' a');
        $this->db->join($this->_table_user . ' b', 'a.customer_id = b.id', 'left');
        $this->db->join($this->_table_location . ' c', 'a.location_id = c.id', 'left');

        $this->db->where('a.is_delete', '0');
        $this->db->where('b.is_delete', '0');
        $this->db->where('c.is_delete', '0');
        $this->db->where('a.id', $id);

        if ($owner > 0)
            $this->db->where('c.owner_id', $owner);

        return $this->db->get()->row_array();
    }

    function get_review_count($status = '', $owner = 0)
    {
        $this->db->select('count(1) count');

        $this->db->from($this->_table . ' a');
        $this->db->join($this->_table_location . ' b', 'a.location_id = b.id');

        if ($status != '')
            $this->db->where('a.status', $status);

        if ($owner > 0)
            $this->db->where('b.owner_id', $owner);

        $this->db->where('a.is_delete', '0');
        $this->db->where('b.is_delete', '0');

        return ($this->db->get()->row_array())['count'];
    }

    function get_review($uno, $start, $displayLength) {
        $this->db->join('locations l', 'l.id = r.location_id');
        $this->db->select('l.name location');
        $this->db->select('r.id')
                ->select('r.res_number')
                ->select('r.rating')
                ->select('comment')
                ->select('r.created_at')
                ->where('customer_id', $uno);
        $this->db->limit($displayLength, $start);
        return $this->db->get('reviews r')->result_array();
    }

    function get_my_review_count($uno) {
        $this->db->where('customer_id', $uno);
        return $this->db->from('reviews')->count_all_results();
    }
}
