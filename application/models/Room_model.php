<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:18 AM
 */

class Room_model extends Base_Model
{
    public $_table;
    public $_table_location;
    // public $_table_category = "categories";

    public function __construct()
    {
        parent::__construct();
        $this->_table = 'rooms';
        $this->_table_room = 'rooms';
        $this->_table_location = 'locations';
    }

    function get_all_rooms($owner = 0) {

        $this->db->select('@pos :=@pos + 1 pos', false);
        $this->db->select('a.*');

        $this->db->from('(SELECT @pos := 0) r');
        $this->db->from($this->_table . ' a');
        $this->db->join('locations l', 'l.id = a.location_id');
        $this->db->select('l.name location');

        $this->db->where('a.is_delete', '0');
        if($owner > 0)
            $this->db->where('l.owner_id', $owner);
        return $this->db->get()->result_array();
    }

    function get_room_count($status = '', $owner = 0) {
        $this->db->select('count(1) count');

        $this->db->from($this->_table.' r');
        $this->db->join($this->_table_location.' l', 'l.id = r.location_id', 'left');
        if ($status != '')
            $this->db->where('r.status', $status);

        if ($owner > 0)
            $this->db->where('owner_id', $owner);

        $this->db->where('r.is_delete', '0');
        $this->db->where('l.is_delete', '0');
        return ($this->db->get()->row_array())['count'];
    }

    private function prepare_get($params) {
        $owner_join = 'users';
        if (!empty($params['owner'])) {
            $this->db->select('id')
                ->select('last_name')
                ->from('users')
                ->where('type', OWNER)
                ->like('last_name', $params['owner'], 'both');
            $owner_join = '('.$this->db->get_compiled_select().')';
        }

        // $category_join = 'categories';
        // if (!empty($params['category'])) {
        //     $this->db->select('id')
        //         ->select('name')
        //         ->from('categories')
        //         ->like('name', $params['category'], 'both');
        //     $category_join = '('.$this->db->get_compiled_select().')';
        // }

        $this->db->select('r.id')
            ->select('r.name')
            ->select('r.capacity')
                ->join('locations l', 'l.id = r.location_id')
            ->select('l.name location')
            ->select('r.created_at')
            ->from('rooms r')
            ->where('r.is_delete', '0');

        if (!empty($params['s_search'])) {
            $this->db->group_start();
            foreach (array('name') as $likeField) {
                $this->db->or_like($likeField, $params['s_search'], 'both');
            }
            $this->db->group_end();
        }

        if (isset($params['status']) && $params['status'] != '') {
            $this->db->where('status', $params['status']);
        }

//        exit($this->db->get_compiled_select());
    }

    function get_table_data($params) {
        $this->prepare_get($params);
        if (isset($params['length'])) {
            $this->db->limit($params['length'], $params['start']);
        }

        $rooms = $this->db->get()->result_array();
        $result = array();
        $row = $params['start'];
        foreach ($rooms as $room) {
            $record = array();
            $row += 1;//row number

//            $record[] = $row;
            foreach ($room as $key => $value) {
                if ($key == 'id') continue;
                $record[] = $value;
            }
            $record[] = '<a href="'.site_url('admin/room/edit/'.$room['id']).'" class="btn btn-sm green table-button">Edit</a>                        <div style="margin: 5px;"></div>
                        <button onclick="delete_room('.$room['id'].')" class="btn btn-sm red table-button">Delete</button>';

            $result[] = $record;
        }

        return $result;
    }

    function get_table_count($params) {
        $this->prepare_get($params);
        return $this->db->count_all_results();
    }

    function get_detail($roomID) {
        $this->prepare_get(array());
        $this->db->select('l.address');
        $this->db->select('l.owner_id');
//        $this->db->select('l.category_id');
        $this->db->select('l.website_url');
        $this->db->select('l.menu_url');
        $this->db->select('l.zip');
        $this->db->select('l.latitude');
        $this->db->select('l.longitude');
        $this->db->select('l.video');
        $this->db->select('l.description');
        $this->db->select('l.gallery');
        $this->db->where('l.id', $roomID);
        return $this->db->get()->row_array();
    }

    function save_room($data) {
        return $this->save($this->_table, $data);
    }

    function delete($id, $owner) {
        // $this->db->where('', $owner);
        $this->db->where('id', $id);

        $this->db->set('is_delete', '1');

        return $this->db->update($this->_table);
    }

    function get_room_by_id($id, $owner) {
        $this->db->select('a.*');
//        $this->db->select('b.name category');

        $this->db->from($this->_table . ' a');
//        $this->db->join($this->_table_category . ' b', 'a.category_id = b.id', 'left');

        $this->db->where('a.id', $id);

//        if ($owner > 0)
//            $this->db->where('a.owner_id', $owner);

        $this->db->where('a.is_delete', '0');
//        $this->db->where('b.is_delete', '0');

        return $this->db->get()->row_array();
    }

    function prepare_statistic($month)
    {
        $this->db->select('count(1) as count');
        $this->db->select('DATE_FORMAT(created_at, \'%Y-%m-%d\') as date');
        $this->db->from('rooms');
        $this->db->where('created_at < STR_TO_DATE("'.$month.'-01", "%Y-%m-%d") + INTERVAL 1 MONTH');
        $this->db->where('created_at >= STR_TO_DATE("'.$month.'-01", "%Y-%m-%d")');
        $this->db->group_by('DATE_FORMAT(created_at, \'%Y-%m-%d\')');
        $this->db->order_by('created_at');

        return $this->db->get()->result_array();
    }

    function get_statistic($month)
    {
        $reslist = $this->prepare_statistic($month);
        $result = array();

        $begindate = strtotime($month . '-01');
        $i = 0;

        for ($date = $begindate; $date < strtotime('+ 1 month', $begindate) && $date < time(); $date = strtotime('+ 1 day', $date)) {

            $strtime = date('Y-m-d', $date);

            if ($i < count($reslist) && $reslist[$i]['date'] == $strtime) {
                $result[] = array('date' => $strtime, 'count' => $reslist[$i]['count']);
                $i ++;
            } else {
                $result[] = array('date' => $strtime, 'count' => 0);
            }
        }

        return $result;
    }

    function get_owner($roomID) {
        $ownerID = $this->db->select('owner_id')
            ->from('locations l')
            ->join('rooms r', 'r.location_id = l.id', 'right')
            ->where('r.id', $roomID)
            ->get()->row_array();
        if ($ownerID) {
            return $ownerID['owner_id'];
        }

        return 0;
    }
}