<script>
    $(document).ready(function() {
        $('.selectpicker').selectpicker();

        $('.form-customer').submit(function(e) {
            if ($('input[name="confirm_password"]').val() != $('input[name="password"]').val()) {
                //notice admin, password doesn't match
                $('.alert-danger span').html('Password doesn\'t match.');
                $('.alert-danger').show();
                e.preventDefault();
            }
        });
    })
</script>