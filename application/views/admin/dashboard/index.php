<h1 class="page-title"> Admin Dashboard</h1>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="<?=site_url('admin/owner')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$owner['total']?>"><?=$owner['total']?></span>
                </div>
                <div class="desc"> Total Owners </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="<?=site_url('admin/owner')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$owner['approved']?>"><?=$owner['approved']?></span>
                </div>
                <div class="desc"> Approved </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="<?=site_url('admin/owner')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$owner['pending']?>"><?=$owner['pending']?></span>
                </div>
                <div class="desc"> Pending </div>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="<?=site_url('admin/location')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$location['total']?>"><?=$location['total']?></span>
                </div>
                <div class="desc"> Total Locations </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="<?=site_url('admin/location')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$location['approved']?>"><?=$location['approved']?></span>
                </div>
                <div class="desc"> Approved </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="<?=site_url('admin/location')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$location['pending']?>"><?=$location['pending']?></span>
                </div>
                <div class="desc"> Pending </div>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="<?=site_url('admin/reservation')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$reservation['total']?>"><?=$reservation['total']?></span>
                </div>
                <div class="desc"> Total Reservations </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="<?=site_url('admin/reservation')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$reservation['confirmed']?>"><?=$reservation['confirmed']?></span>
                </div>
                <div class="desc"> Confirmed </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 yellow" href="<?=site_url('admin/reservation')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$reservation['cancelled']?>"><?=$reservation['cancelled']?></span>
                </div>
                <div class="desc"> Cancelled </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="<?=site_url('admin/reservation')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$reservation['pending']?>"><?=$reservation['pending']?></span>
                </div>
                <div class="desc"> Pending </div>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="<?=site_url('admin/customer')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$customer['total']?>"><?=$customer['total']?></span>
                </div>
                <div class="desc"> Total Customers </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="<?=site_url('admin/customer')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$customer['active']?>"><?=$customer['active']?></span>
                </div>
                <div class="desc"> Active </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="<?=site_url('admin/customer')?>">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?=$customer['inactive']?>"><?=$customer['inactive']?></span>
                </div>
                <div class="desc"> Inactive </div>
            </div>
        </a>
    </div>
</div>