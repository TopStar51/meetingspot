<script>
var handleValidation = function() {
    // for more info visit the official plugin documentation:
    // http://docs.jquery.com/Plugins/Validation

    var frm_about_text = $('#frm_about_text');
    var error1 = $('.alert-danger', frm_about_text);

    frm_about_text.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            data: {
                required: true
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit
            error1.show();
            App.scrollTo(error1, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            if (cont) {
                cont.after(error);
            } else {
                element.after(error);
            }
        },

        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            error1.hide();
        }
    });
};

$(document).ready(function() {
    handleValidation();
    $('#btn_submit').click(function () {
            if (!$("#frm_about_text").valid()) return;
            var type = '<?=$type;?>';
            var data = $("#about_text").val();

            process_ajax(
                'btn_submit',
                '<?=site_url('admin/design/update_footer_data')?>',
                {type: type, text: data},
                function (resp) {
                    show_toastr("success", "Set successfully", "Site Data Setting");
                },
                'Adding News Error'
            );
        });
});
</script>