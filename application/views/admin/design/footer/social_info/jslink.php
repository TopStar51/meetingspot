<script>
var handleValidation = function() {
    // for more info visit the official plugin documentation:
    // http://docs.jquery.com/Plugins/Validation

    var frm_social_info = $('#frm_social_info');
    var error1 = $('.alert-danger', frm_social_info);

    frm_social_info.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        messages: {
            
        },
        rules: {
            facebook: {
                required: true,
            }, 
            twitter: {
                required: true,
            },
            instagram: {
                required: true
            },
            whatsapp: {
                required: true
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit
            error1.show();
            App.scrollTo(error1, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            if (cont) {
                cont.after(error);
            } else {
                element.after(error);
            }
        },

        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            error1.hide();
        }
    });
};

$(document).ready(function() {
    handleValidation();
    $('#btn_submit').click(function () {
            if (!$("#frm_social_info").valid()) return;
            var type = '<?=$type;?>';
            var form_data = $("#frm_social_info").serializeArray();
            form_data.push({
                "name": "type",
                "value": type
            });
            process_ajax(
                'btn_submit',
                '<?=site_url('admin/design/update_footer_data')?>',
                form_data,
                function (resp) {
                    show_toastr("success", "Set successfully", "Site Data Setting");
                },
                'Adding News Error'
            );
        });
});
</script>