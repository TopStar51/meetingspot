<style>
    .gallery{ width:100%; float:left;}
    .gallery ul{ margin:0; padding:0; list-style-type:none;}
    .gallery ul li{ padding:7px; border:2px solid #ccc; float:left; margin:10px 7px; background:none; width:auto; height:auto;}
</style>

<h1 class="page-title">Set Page Header</h1>
<div class="row">
	<div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-equalizer font-red-sunglo"></i>
                    <span class="caption-subject font-red-sunglo bold uppercase">Form</span>
                    <span class="caption-helper"></span>
                </div>
                <div class="tools">
                    <a href="" class="collapse"> </a>
                    <a href="" class="reload"> </a>
                    <a href="" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-3">
                    	<ul class="nav nav-tabs tabs-left">
                            <?php foreach ($pages as $key=>$page) { ?>
                                <li class="<?=($key == 0) ? "active" : ""?>">
                                    <a href="#tab_<?=$page['id']?>" data-page_id="<?=$page['id']?>" data-toggle="tab" aria-expanded="<?=($key == 0) ? "true" : "false"?>"> <?=$page['name']?> </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-9">
                    	<div class="tab-content">
                    		<?php foreach ($pages as $key=>$page) { ?>
                    		<div class="tab-pane fade <?=($key == 0) ? "active in" : ""?>" id="tab_<?=$page['id']?>">
                    			<div class="tabbable-custom tabs-below">
                                    <div class="tab-content">
                                    	<div class="tab-pane active tab_header_detail" id="tab_<?=$page['id']?>" data-page_id="<?=$page['id']?>">
	                                        <form action="#" id="frm_set_header_<?=$page['id']?>">
	                                            <div class="form-body">
	                                                <div class="alert alert-danger display-hide">
	                                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
	                                                <div class="alert alert-success display-hide">
	                                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>

	                                                <input type="text" name="page_id" value="<?=$page['id']?>" style="display: none;">
	                                                <div class="row">
	                                                    <div class="col-md-12">
	                                                        <div class="form-group">
	                                                            <div class="mt-checkbox-inline">
	                                                                <label class="mt-checkbox mt-checkbox-outline">
	                                                                    <input type="checkbox" name="show_title" checked> Show Title
	                                                                    <span></span>
	                                                                </label>
	                                                                <label class="mt-checkbox mt-checkbox-outline">
	                                                                    <input type="checkbox" name="show_cover" checked> Show Cover
	                                                                    <span></span>
	                                                                </label>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                                <div class="row">
	                                                    <div class="col-md-12">
	                                                        <div class="form-group">
	                                                            <label class="control-label">Title</label>
	                                                            <input type="text" class="form-control" name="title">
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                                <div class="row row_images" style="display: none;" data-page_id="<?=$page['id']?>">
	                                                    <input type="text selected_image" name="cover_data" style="display: none;" data-page_id="<?=$page['id']?>">
	                                                    <div class="col-md-12">
	                                                        <div class="form-group">
	                                                            <a type="button" class="btn blue btn-block btn-outline" data-toggle="modal" id="btn_show_dropzone_modal" href="#modal_upload_image">
	                                                                Upload New Image
	                                                            </a>
	                                                        </div>
	                                                    </div>
	                                                    <div class="col-md-12">
                                                            <div class="gallery">
                                                                <ul class="reorder-gallery" data-page_id="<?=$page['id']?>">
                                                                </ul>
                                                            </div>
                                                        </div>
	                                                </div>
	                                            </div>

	                                            <div class="form-actions">
	                                                <div class="row">
	                                                    <div class="col-md-12">
	                                                        <a type="button" class="btn green pull-right btn_submit" data-page_id="<?=$page['id']?>">Submit</a>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </form>
	                                    </div>
                                    </div>
                                </div>
                    		</div>	
                    		<?php } ?>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_upload_image" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Upload Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo site_url('admin/design/upload_header_image'); ?>" class="dropzone dropzone-file-area" id="dropZoneArea" style="width: 700px;">
                            <h3 class="sbold">Drop files here or click to upload</h3>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a type="button" class="btn dark btn-outline" data-dismiss="modal">Close</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
