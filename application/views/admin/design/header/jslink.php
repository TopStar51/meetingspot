<script>
	var curPageNo = <?=$pages[0]['id'] ?>;

	var initComponents = function() {
		Dropzone.options.dropZoneArea = {
			dictDefaultMessage: "",
			maxFiles: 1,
			success: function(file, response){
				var fileName = JSON.parse(response).uploadResp.uploadData.file_name;
				var formId = 'frm_set_header_' + curPageNo;
				$("#"+formId+" input[name=cover_data]").val(fileName);

				var htmlData = '';
				htmlData += '<li id="'+ fileName +'" class="ui-sortable-handle">'
                    + '<a href="javascript:void(0);">'
                    + '<img src="<?php echo base_url(); ?>upload/front/header/'+fileName+'" alt="" style="width: 300px; height: 200px">'
                    + '</a></li>';
                $('.reorder-gallery[data-page_id="'+curPageNo+'"]').html(htmlData);
			}
		};
	};

	var actionProcess = function() {
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            curPageNo = $(e.target).data('page_id');
            load_form_data(curPageNo);
        });

        $('#modal_upload_image').on('show.bs.modal', function () {
            Dropzone.forElement("#dropZoneArea").removeAllFiles(true);
        });
	}

	var load_form_data = function(pageNo) {
		var url = "<?=site_url('admin/design/get_form_data') ?>";
		process_ajax(
			'',
			url,
			{page_id: pageNo}
			,
			function(resp){
				if(resp.state = 'success') {
					var pageInfo = resp.pageInfo;
					var formId = 'frm_set_header_' + pageNo;
					if(pageInfo) {
						$("#"+formId+" input[name=show_title]").prop('checked', (pageInfo.show_title == 'on') ? true : false);
                        $("#"+formId+" input[name=show_cover]").prop('checked', (pageInfo.show_cover == 'on') ? true : false);
                        $("#"+formId+" input[name=title]").val(pageInfo.title);
                        $('.row_images[data-page_id="'+pageNo+'"]').show();
                        $("#"+formId+" input[name=cover_data]").val(pageInfo.cover_data);

                        var htmlData = '';
                        htmlData += '<li id="'+ pageInfo.cover_data +'" class="ui-sortable-handle">'
	                        + '<a href="javascript:void(0);">'
	                        + '<img src="<?php echo base_url(); ?>upload/front/header/'+ pageInfo.cover_data +'" alt="" style="width: 300px; height: 200px">'
	                        + '</a></li>';
	                    $('.reorder-gallery[data-page_id="'+curPageNo+'"]').html(htmlData);
					} else {
						$("#"+formId+" input[name=show_title]").prop('checked', true);
                        $("#"+formId+" input[name=show_cover]").prop('checked', true);

                        $("#"+formId+" input[name=title]").val('');

                        $('.row_images[data-page_id="'+pageNo+'"]').hide();

                        $("#"+formId+" input[name=cover_data]").val('');
                        $('.reorder-gallery[data-page_id="'+curPageNo+'"]').html('');
					}
				}
			},
			'Showing Form data error.'
		);
	}


	$(document).ready(function() {
		initComponents();
		actionProcess();

		load_form_data(curPageNo);

		$('.btn_submit').click(function () {
            var pageNo = $(this).data('page_id');

            process_ajax(
                '',
                '<?=site_url('admin/design/update_header_data')?>',
                $('#frm_set_header_' + pageNo).serializeArray(),
                function (resp) {
                    show_toastr("success", "Successfully set", "Setting header");
                },
                'Submitting Form data error.'
            )
        });
	})
</script>