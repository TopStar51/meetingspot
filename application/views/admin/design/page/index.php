<h1 class="page-title">List of Pages</h1>
<div class="table-toolbar">
    <div class="row">
        <div class="col-md-6">
            <div class="btn-group">
                <a id="btn_add_page" class="btn sbold green" data-toggle="modal">
                    <i class="fa fa-plus"></i> Add Page
                </a>
            </div>
        </div>
    </div>
</div>
<table class="table table-striped table-hover order-column my-table" id="table-pages">
    <thead>
    <tr style="background: #eee; padding:">
        <th width="10%"> # </th>
        <th width="20%"> ID </th>
        <th width="15%"> Menu Name </th>
        <th width="25%"> Url </th>
        <th width="15%"> Show </th>
        <th width="15%"> Actions </th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<div class="modal fade" id="modal_page_data" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Page Data</h4>
            </div>
            <div class="modal-body">
                <form id="frm_add_page_data" class="form-horizontal">
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                    <input type="text" hidden name="id">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Menu Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="menu_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Show Menu</label>
                            <div class="col-md-6">
                                <input type="checkbox" checked class="make-switch" data-size="small" name="is_visible" id="is_visible">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Url</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="url">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a type="button" class="btn dark btn-outline" data-dismiss="modal">Close</a>
                <a type="button" class="btn green" id="btn_submit_page_data">Save changes</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>