<script>

    var handleValidation = function() {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var frm_add_page_data = $('#frm_add_page_data');
        var error1 = $('.alert-danger', frm_add_page_data);

        frm_add_page_data.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
            },
            rules: {
                name: {
                    required: true
                },
                menu_name: {
                    required: true
                },
                url: {
                    required: true
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                error1.show();
                App.scrollTo(error1, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                error1.hide();
            }
        });
    };

    var page_table;
    var pageTableInit = function () {
        return {
            init: function () {
                page_table = new Datatable();

                page_table.init({
                    src: $("#table-pages"),
                    onSuccess: function (grid, response) {
                        // grid:        grid object
                        // response:    json object of server side ajax response
                        // execute some code after table records loaded
                    },
                    onError: function (grid) {
                        // execute some code on network or other general error
                    },
                    onDataLoad: function (grid) {
                        // execute some code on ajax data load
                        $(".btn_edit_page").click(function(){
                            var id = $(this).data('id');
                            var name = $(this).data('name');
                            var menuName = $(this).data('menu_name');
                            var url = $(this).data('url');
                            var isVisible = $(this).data('is_visible');
                            
                            $('#frm_add_page_data input[name=id]').val(id);
                            $('#frm_add_page_data input[name=name]').val(name);
                            $('#frm_add_page_data input[name=menu_name]').val(menuName);
                            $('#frm_add_page_data input[name=url]').val(url);
                            $("#frm_add_page_data input[name=is_visible]").bootstrapSwitch('state', (isVisible == '1') ? true : false);

                            $('#modal_page_data').modal('show');
                        })
                    },
                    loadingMessage: 'Loading...',
                    dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                        // So when dropdowns used the scrollable div should be removed.
                        //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-page-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

                        // save datatable state(pagination, sort, etc) in cookie.
                        "bStateSave": true,

                        // save custom filters to the state
                        "fnStateSaveParams": function (oSettings, sValue) {
                            $("#table-pages tr.filter .form-control").each(function () {
                                sValue[$(this).attr('name')] = $(this).val();
                            });

                            return sValue;
                        },

                        // read the custom filters from saved state and populate the filter inputs
                        "fnStateLoadParams": function (oSettings, oData) {
                            //Load custom filters
                            $("#table-pages tr.filter .form-control").each(function () {
                                var element = $(this);
                                if (oData[element.attr('name')]) {
                                    element.val(oData[element.attr('name')]);
                                }
                            });

                            return true;
                        },

                        "lengthMenu": [
                            [10, 20, 50, 100, -1],
                            [10, 20, 50, 100, "All"] // change per page values here
                        ],
                        "pageLength": 10, // default record count per page
                        "ajax": {
                            "url": "<?=site_url('admin/design/get_table_page_data')?>", // ajax source
                        },
                        "ordering": false,
                        // 'serverSide': true,
                        "order": [
                            [1, "asc"]
                        ]// set first column as a default sort by asc
                    }
                });

                $('.table-toolbar .form-filter').change(function() {
                    page_table.setAjaxParam(this.name, this.value);
                    page_table.getDataTable().ajax.reload();
                });
            }
        };
    }();

    $(document).ready(function() {
        pageTableInit.init();

        handleValidation();

        $('#btn_add_page').click(function() {
            $('#frm_add_page_data input[name=id]').val('');
            $('#frm_add_page_data input[name=name]').val('');
            $('#frm_add_page_data input[name=menu_name]').val('');
            $('#frm_add_page_data input[name=url]').val('<?=site_url()?>');
            $("#frm_add_page_data input[name=is_visible]").bootstrapSwitch('state', true);

            $('#modal_page_data').modal('show');
        });

        $("#btn_submit_page_data").click(function(){
            if (!$("#frm_add_page_data").valid()) return;

            process_ajax(
                'btn_submit_page_data',
                '<?=site_url("admin/design/save_page");?>',
                $('#frm_add_page_data').serializeArray(),
                function(resp) {
                    $('#modal_page_data').modal('hide');
                    page_table.submitFilter();
                },
                'Internal Server Error'
            );
        })
    });

    function delete_page(pageID) {
        swal({
            type: 'warning',
            title: 'Are you sure?',
            text: 'You won\'t be able to revert this!',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it.'
        }, function (value) {
            if (value) {
                $.post({
                    url: '<?=site_url('admin/design/delete_page/')?>' +pageID,
                    // data: { id: pageID },
                    dataType: 'json',
                    success: function (resp) {
                        if (resp.status) {
                            page_table.getDataTable().ajax.reload();
                        }
                    }
                });
            }
        });
    }
</script>