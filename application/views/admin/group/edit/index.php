<style>
    .divider {
        border: solid 1px #aaa;
    }
</style>
<h1 class="page-title"> Groups/<?=isset($group)? 'Edit' : 'New'?> group</h1>

<form class="form form-group" action="<?=site_url('admin/group/save_group')?>" method="post">
    <div class="alert alert-danger" <?=!isset($error) ? 'hidden' : ''?>>
        <button class="close" data-close="alert"></button>
        <span> <?=isset($error) ? $error: ''?> </span>
    </div>
    <div class="form-body row">
        <?php if (isset($group) && isset($group['id'])) { ?>
            <input hidden name="id" value="<?=$group['id']?>">
        <?php } ?>
        <div class="form-group col-md-6">
            <label>Customer</label>
            <select name="user_id" class="form-control select2" required id="user_id">
                <option value=""></option>
                <?php foreach($customers as $customer) { ?>
                    <option value="<?=$customer['id']?>"><?=$customer['first_name']?>&nbsp;<?=$customer['last_name']?></option>
                <?php } ?>
            </select>
        </div>
        <?php
        $fields = array(
            array(
                'Group Name',
                'group_name',
                'text'
            ),
            array(
                'Address',
                'address',
                'text'
            ),
            array(
                'Contact Number',
                'phone',
                'number'
            ),
            array(
                'Website',
                'website',
                'url'
            ),
            array(
                'Facebook',
                'facebook',
                'url'
            )
        );
        foreach ($fields as $field) {
        ?>
            <div class="form-group col-md-6">
                <label><?=$field[0]?></label>
                <input name="<?=$field[1]?>"
                   <?php if (isset($group)) {
                       echo 'value="'.$group[$field[1]].'"';
                   }?>
                   type="<?=$field[2]?>" class="form-control" placeholder="" <?php if($field[1]=='group_name') echo 'required'; ?>>
            </div>
        <?php
        }
        ?>
        <div class="form-group col-md-6">
            <label>Nature of group</label>                              
            <div class="m-radio-list margin-top-10">
                <div class="col-md-3">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="nature_of_group" value="civic" checked>
                        Civic
                        <span></span>
                    </label>
                </div>
                <div class="col-md-3">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="nature_of_group" value="religious" <?=isset($location)&&$location['soft_drink']?'checked':''?>>
                        Religious
                        <span></span>
                    </label>
                </div>
                <div class="col-md-3">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="nature_of_group" value="sports" <?=isset($location)&&$location['alcoholic_drink']?'checked':''?>>
                        Sports
                        <span></span>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group col-md-6">
            <label>Minimum age of members</label>
            <input name="min_age" type="number" class="form-control" value="<?=isset($group) ? $group['min_age'] : ''?>" required>
        </div>
    </div>

    <div class="form-body">
        <button class="btn green" style="padding: 5px 60px; float: right;">Save</button>
    </div>
</form>