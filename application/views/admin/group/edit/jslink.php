<script type="text/javascript">
    $(document).ready(function() {
        $('#user_id').select2({
            placeholder: 'Select a customer'
        });
        <?php if(isset($group)) { ?>
        $('#user_id').val('<?=$group['user_id']?>').change();
        <?php } ?>
    })
</script>