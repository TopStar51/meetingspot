<style>
    .my-seperator {
        border: dashed 1px gray;  margin: 10px 0px;
    }
</style>
<h1 class="page-title">Locations/<?=isset($location)?'Edit' : 'New'?> Location</h1>
<div id="form_wizard_1">
    <form action="#" id="submit_form" method="POST">
            <div class="form-body">
                <div class="alert alert-danger display-none">
                    <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                <div class="alert alert-success display-none">
                    <button class="close" data-dismiss="alert"></button> Your form validation is successful!
                </div>
                <div class="m-form__heading">
                    <h3 class="m-form__heading-title">
                        Basic Info
                    </h3><br>
                </div>
                <div class="form-body row"><!-- Basic Info -->
                        <?php if (isset($location) && isset($location['id'])) { ?>
                            <input hidden name="id" value="<?=$location['id']?>">
                        <?php } ?>
                        <?php
                        $fields = array(
                            array(
                                'Location Name',
                                'name',
                                'text'
                            ),
                            array(
                                'Owner Name',
                                'owner_id',
                                'select',
                                'select2'
                            ),
                            array(
                                'Contact Phone',
                                'phone',
                                'number'
                            ),
                            array(
                                'Contact Email',
                                'email',
                                'email'
                            ),
                            array(
                                'Category',
                                'category_id',
                                'select',
                                'select2'
                            ),
                            array(
                                'Website',
                                'website_url',
                                'url'
                            ),
                            array(
                                'Menu',
                                'menu_url',
                                'url'
                            ),
                            array(
                                'Address',
                                'address',
                                'text'
                            ),
                            array(
                                'City',
                                'city',
                                'text',
                                'hide'
                            ),
                            array(
                                'State',
                                'state',
                                'text',
                                'hide'
                            ),
                            array(
                                'Zip',
                                'zip',
                                'text',
                                'hide'
                            ),
                            array(
                                'Latitude',
                                'latitude',
                                'number',
                                'hide'
                            ),
                            array(
                                'Longitude',
                                'longitude',
                                'number',
                                'hide'
                            )
                        );
                        foreach ($fields as $field) { ?>
                            <div class="form-group <?php if(false && ($field[1]=='owner_id' || $field[1]=='phone')) echo 'col-md-3'; else echo 'col-md-6'; ?> <?php if (count($field) > 3 && $field[3] == 'hide') echo $field[3]; ?>">
                                <label><?=$field[0]?></label>
                                <?php if ($field[2] == 'select') { ?>
                                    <select name="<?=$field[1]?>" class="<?=$field[3]?>" <?= isset($location) ? '' : 'required'?>></select>
                                <?php } else { ?>
                                <input name="<?=$field[1]?>"
                                    <?php if (isset($location)) {
                                        echo 'value="'.$location[$field[1]].'"';
                                    }?>
                                       type="<?=$field[2]?>" class="form-control" placeholder="" <?php if($field[1] == 'website_url' || $field[1] == 'menu_url') echo ''; else echo 'required'?> id="<?=$field[1]?>">
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" class="form-control" placeholder="location description" style="min-height: 150px;"><?php if (isset($location)) echo $location['description'];?></textarea>
                            </div>
                        </div>
                </div>
                <div style="border: dashed 1px gray; margin: 10px 0px;"></div>
                <div class="m-form__section">
                    <div class="m-form__heading">
                        <h3 class="m-form__heading-title">
                            Beverage Services
                        </h3>
                    </div>
                    <br>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6 m-form__group-sub">                                
                            <div class="m-checkbox-list">
                                <label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox" name="water" value="1" <?=isset($location)&&$location['water']?'checked':''?>>
                                    Water Included
                                    <span></span>
                                </label>&nbsp;&nbsp;
                                <label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox" name="soft_drink" value="1" <?=isset($location)&&$location['soft_drink']?'checked':''?>>
                                    Soft Drink
                                    <span></span>
                                </label>&nbsp;&nbsp;
                                <label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox" name="alcoholic_drink" value="1" <?=isset($location)&&$location['alcoholic_drink']?'checked':''?>>
                                    Alcoholic Drink
                                    <span></span>
                                </label>&nbsp;&nbsp;
                            </div>
                        </div>
                        <div class="col-lg-6 m-form__group-sub">
                            <label class="col-md-5 control-label">
                                Mandatory Minimum Order Per Person:
                            </label>
                            <div class="col-md-3">
                                <input type="number" name="mandatory_order"
                                   <?php if (isset($location)) echo 'value="' . $location['mandatory_order'] . '"' ?> class="form-control m-input" min="0">
                            </div>
                        </div>
                    </div>
                <div class="my-seperator"></div>
                <div class="m-form__heading margin-top-30">
                    <h4 class="m-form__heading-title">
                        Working Hours
                    </h4>
                </div>
                <div class="form-body row">
                    <div class="col-md-12">
                        <div class="my-div3 row">
                            <div class="col-md-offset-2 col-md-3">Open Hour</div>
                            <div class="col-md-3">Close Hour</div>
                        </div>
                        <?php
                        $index = 0;
                        foreach (array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday') as $day) {
                            $index += 1;
                            if ($index == 7) $index = 0;
                        ?>
                            <div class="my-div3 row">
                                <label class="col-md-2" style="text-align: right;"><?=$day?></label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input name="working_hour[<?=$index?>][open_hour]"
                                                <?php
                                                if (isset($working_hours) && array_key_exists($index, $working_hours))
                                                    echo 'value="'.$working_hours[$index]['open_hour'].'"';
                                                else
                                                    echo 'value="'.$example_hours[$index]['open'].'"';
                                                ?>
                                                type="text" class="form-control timepicker timepicker-no-seconds">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-clock-o"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input name="working_hour[<?=$index?>][close_hour]"
                                                <?php
                                                if (isset($working_hours) && array_key_exists($index, $working_hours)) echo 'value="'.$working_hours[$index]['close_hour'].'"';
                                                else echo 'value="'.$example_hours[$index]['close']; ?>
                                                type="text" class="form-control timepicker timepicker-no-seconds">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-clock-o"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-4 my-switch1">
                                    <input name="working_hour[<?=$index?>][status]" type="checkbox"
                                            <?php if (isset($working_hours) && array_key_exists($index, $working_hours) && $working_hours[$index]['status'] == '1') echo 'checked'; ?>
                                            class="make-switch" data-on-text="&nbsp;Open&nbsp;" data-off-text="&nbsp;Closed&nbsp;" data-on-color="success" data-off-color="danger">
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="my-seperator"></div>
                <div class="m-form__heading margin-top-30">
                    <h4 class="m-form__heading-title">
                        Media Files
                    </h4>
                </div>
                <div class="row" style="margin-right: 0px; margin-left: 0px;">
                    <div class="my-div1 col-md-3 col-lg-3">
                        <label>Thumbnail Image</label>
                        <div action="<?=site_url()?>" class="dropzone dropzone-file-area" id="thumb-dropzone">
                            <h3 class="sbold">Drop files here or click to upload</h3>
                        </div>
                        <?php if (isset($location)) { ?>
                        <div style="padding: 15px;">
                            <img src="<?=base_url('upload/location/'.$location['thumb_image'])?>" style="width: 100%; height: 20vh;" class="location-thumb-image">
                        </div>
                        <?php } ?>
                    </div>
                    <div class="my-div1 col-md-6 col-lg-6">
                        <label>Gallery</label>
                        <div action="<?=site_url()?>" class="dropzone dropzone-file-area" id="gallery-dropzone">
                            <h3 class="sbold">Drop files here or click to upload</h3>
                        </div>
                        <div class="row" style="margin: 10px 0px;">
                            <?php if (isset($location)) {
                                $galleries = json_decode($location['gallery']);
                                if (is_array($galleries)) {
                                    foreach ($galleries as $image) { ?>
                                        <div class="col-md-6 col-lg-4" style="padding: 5px;">
                                            <img src="<?=base_url('upload/location/'.$image) ?>" class="img-gallery" data-name="<?=$image?>" style="height:20vh">
                                            <button class="button-remove-gallery btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                        </div>
                                    <?php }
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="my-div1 col-md-3 col-lg-3">
                        <label>Video</label>
                        <div action="<?=site_url()?>" class="dropzone dropzone-file-area" id="video-dropzone" style="width: 100%;">
                            <h3 class="sbold">Drop files here or click to upload</h3>
                        </div>
                        <?php if (isset($location)) { ?>
                        <div style="padding: 10px;">
                            <video width="100%" controls><source src="<?=base_url('upload/location/'.$location['video'])?>"></video>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="m-form__heading margin-top-20">
                    <h4 class="m-form__heading-title">
                        Status
                    </h4>
                </div>
                <div class="form-body row">
                    <div class="form-group col-md-6">
                        <select name="status" class="selectpicker">
                            <option value="0" <?php if (isset($location) && $location['status'] == INACTIVE) echo 'selected';?>>Pending</option>
                            <option value="1" <?php if (isset($location) && $location['status'] == ACTIVE) echo 'selected';?>>Approved</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="col-12" style="text-align: right;">
                    <div>
                        <!-- <button class="btn green btn-icon"> Submit -->
                        <a href="javascript:submit_location()" class="btn green"> Submit
                            <i class="fa fa-check"></i>
                        </a>
                        <!-- </button> -->
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
