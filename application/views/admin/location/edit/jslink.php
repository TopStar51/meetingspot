<script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_MAP_API_KEY ?>&libraries=places"></script>
<script>
    var dropzone_gallery;
    var dropzone_video;
    var dropzone_thumb;

    var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        postal_code: 'short_name'
    };

    function initialize() {

        var options = {
            componentRestrictions: {country: "us"}
        };

        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }
            for (var component in componentForm) {
                document.getElementById(component).value = '';
            }
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {

                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        });     
        
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    
    jQuery(document).ready(function() {
        inputImage();
        //make select2 ajax (owner and category)
        $("select[name='owner_id']").select2({
            width: "off",
            ajax: {
                url: "<?=site_url('admin/owner/ajax_owner_list')?>",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page//?
                    };
                },
                processResults: function(data, page) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: function(owner) {
                return owner.owner;
            },
            templateSelection: function (owner) {
                return owner.owner;
            }
        });
        $('select[name="category_id"]').select2({
            width: "off",
            ajax: {
                url: "<?=site_url('admin/category/ajax_category_list')?>",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page//?
                    };
                },
                processResults: function(data, page) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: function(category) {
                return category.name;
            },
            templateSelection: function (category) {
                return category.name;
            }
        });
        //set owner_id, category_id to select
        <?php if (isset($location)) { ?>
        //$('select[name="owner_id"]')({name: '<?//=$location['owner']?>//', id: <?//=$location['owner_id']?>//});
        $('select[name="owner_id"]').closest('div').find('.select2-selection__rendered').html('<?=$location['owner']?>');
        $('select[name="category_id"]').closest('div').find('.select2-selection__rendered').html('<?=$location['category']?>');
        <?php } ?>

        $('input[name="video"]').change(function() {
            $('video').attr('src', this.value);
        });

        if (jQuery().timepicker) {
            $('.timepicker-no-seconds').timepicker({
                autoclose: true,
                minuteStep: 5,
                defaultTime: false
            });
        }

        Dropzone.options.thumbDropzone = {
            dictDefaultMessage: "",
            acceptedFiles: "image/*",
            maxFiles: 1,
            init: function() {
                dropzone_thumb = this;
                this.on("addedfile", function(file) {
                    // Create the remove button
                    var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn red btn-sm btn-block'>Remove</a>");

                    // Capture the Dropzone instance as closure.
                    var _this = this;

                    // Listen to the click event
                    removeButton.addEventListener("click", function(e) {
                        // Make sure the button click doesn't submit the form:
                        e.preventDefault();
                        e.stopPropagation();

                        // Remove the file preview.
                        _this.removeFile(file);
                        // If you want to the delete the file on the server as well,
                        // you can do the AJAX request here.
                    });

                    // Add the button to the file preview element.
                    file.previewElement.appendChild(removeButton);
                });
            }
        };

        Dropzone.options.galleryDropzone = {
            dictDefaultMessage: "",
            acceptedFiles: "image/*",
            maxFiles: 3,
            init: function() {
                dropzone_gallery = this;
                this.on("addedfile", function(file) {
                    // Create the remove button
                    var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn red btn-sm btn-block'>Remove</a>");

                    // Capture the Dropzone instance as closure.
                    var _this = this;

                    // Listen to the click event
                    removeButton.addEventListener("click", function(e) {
                        // Make sure the button click doesn't submit the form:
                        e.preventDefault();
                        e.stopPropagation();

                        // Remove the file preview.
                        _this.removeFile(file);
                        // If you want to the delete the file on the server as well,
                        // you can do the AJAX request here.
                    });

                    // Add the button to the file preview element.
                    file.previewElement.appendChild(removeButton);
                });
            }
        };

        Dropzone.options.videoDropzone = {
            dictDefaultMessage: "",
            acceptedFiles: "video/*",
            init: function() {
                dropzone_video = this;
                this.on("addedfile", function(file) {
                    // Create the remove button
                    var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn red btn-sm btn-block'>Remove</a>");

                    // Capture the Dropzone instance as closure.
                    var _this = this;

                    // Listen to the click event
                    removeButton.addEventListener("click", function(e) {
                        // Make sure the button click doesn't submit the form:
                        e.preventDefault();
                        e.stopPropagation();

                        // Remove the file preview.
                        _this.removeFile(file);
                        // If you want to the delete the file on the server as well,
                        // you can do the AJAX request here.
                    });

                    // Add the button to the file preview element.
                    file.previewElement.appendChild(removeButton);
                });
            }
        };

        $('.button-remove-gallery').click(function() {
            $(this).closest('div').remove();
        });

        // $('#submit_form').submit(function(e) {
        //     e.preventDefault();
        //     submit_location();
        // });
    });

    function submit_location() {
        if (!$('#submit_form').valid()) {
            return;
        }
        var formData = new FormData();
        var form = $('#submit_form')[0];
        var fileIndex = 0;
        for (var i =0; i < form.length; i +=1) {
           if (form[i].type == 'file') {
               //thumb & video file uploading
               //thumbnail, video, gallery
               // if (form[i].name == 'gallery') {
               //     formData.append('gallery[]', form[i].files[fileIndex]);
               // } else {
               if (form[i].files.length > 0)
                   formData.append(form[i].name, form[i].files[fileIndex]);
               // }
               fileIndex += 1;
           } else if (form[i].type == 'checkbox') {
                if (form[i].checked) {
                    formData.append(form[i].name, '1');
                } else {
                    formData.append(form[i].name, '0');
                }
           } else {
               formData.append(form[i].name, form[i].value);
           }
        }

        if (dropzone_thumb.files.length > 0) {
            formData.append('thumb', dropzone_thumb.files[0]);
        } else {
            //if there's not thumbnail set, notice user to select one
            if ($('.location-thumb-image').length == 0) {
                swal('', 'Please select a thumbnail image.', 'warning');
                return;
            }
        }
        
        var gallery_count = 0;
        //send gallery dropzone files to server
        for (var i =0; i < dropzone_gallery.files.length; i += 1) {
            formData.append('gallery[]', dropzone_gallery.files[i]);
            gallery_count += 1;
        }
        $('img.img-gallery').each(function() {
            formData.append('gallery[]', $(this).data('name'));
            gallery_count += 1;
        });
        if (gallery_count < 2) {
            swal('', 'Please select at least 2 images for gallery.', 'warning');
            return;
        }

        if (dropzone_video.files.length > 0) {
            formData.append('video', dropzone_video.files[0]);
        } else {
            //if there's not video set, notice user to select one
            if ($('video').length == 0) {
                swal('', 'Please select a video.', 'warning');
                return;
            }
        }

        process_form_data('', '<?=site_url('admin/location/save_location')?>', formData,
            function() {
                //success
                window.location.href = '<?=site_url('admin/location')?>';
            },
            function () {
                swal('', 'error occurred', 'error');
                // console.log('error occurred');
            }
        );
    }
</script>
