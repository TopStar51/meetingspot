<h1>Locations</h1>
<div class="table-toolbar">
    <div class="row">
        <div class="col-md-6">
            <div class="btn-group">
                <a href="<?=site_url('admin/location/add')?>" class="btn sbold green">
                    <i class="fa fa-plus"></i> Add Location
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="btn-group pull-right">
                <input name="s_search" class="form-control form-filter" placeholder="Search">
            </div>
        </div>
    </div>
</div>
<table class="table table-striped table-hover order-column my-table" id="table-locations">
    <thead>
        <tr style="background: #eee; padding:">
            <th> Thumbnail </th>
            <th> Name </th>
            <th> Owner </th>
            <th> Type </th>
            <th> Email </th> 
            <th> Phone </th>
            <th> City </th>
            <th> State </th>
            <th> Date Registered </th>
            <th> Status </th>
            <th> Actions </th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
