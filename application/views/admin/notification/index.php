<h1>Notifications</h1>
<div style="margin-top: 20px;"></div>
<table class="table table-striped table-hover order-column my-table" id="table-notifications">
    <thead>
        <tr style="background: #eee; padding:">
            <th> # </th>
            <th> Content </th>
            <th> Date Sent </th>
            <th> Show </th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
