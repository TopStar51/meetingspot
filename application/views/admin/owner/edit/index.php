<style>
    .divider {
        border: solid 1px #aaa;
    }
</style>
<h1 class="page-title"> Owners/<?=isset($owner)? 'Edit' : 'New'?> Owner</h1>

<form class="form form-owner" action="<?=site_url('admin/owner/save_owner')?>" method="post">
    <div class="alert alert-danger" <?=!isset($error) ? 'hidden' : ''?>>
        <button class="close" data-close="alert"></button>
        <span> <?=isset($error) ? $error: ''?> </span>
    </div>
    <h4>Person Info</h4>
    <div class="divider"></div>
    <div class="form-body row">
        <?php if (isset($owner) && isset($owner['id'])) { ?>
            <input hidden name="id" value="<?=$owner['id']?>">
        <?php } ?>
        <?php
        $fields = array(
            array(
                'First Name',
                'first_name',
                'text'
            ),
            array(
                'Last Name',
                'last_name',
                'text'
            ),
            array(
                'Email',
                'email',
                'email'
            ),
            array(
                'Phone',
                'phone',
                'number'
            ),
            array(
                'Password',
                'password',
                'password'
            ),
            array(
                'Confirm Password',
                'confirm_password',
                'password'
            )
        );
        foreach ($fields as $field) {
        ?>
            <div class="form-group col-md-6">
                <label><?=$field[0]?></label>
                <input name="<?=$field[1]?>"
                   <?php if (isset($owner)) {
                       if ($field[1] == 'confirm_password') {
                           $field[1] = 'password';
                       }
                       echo 'value="'.$owner[$field[1]].'"';
                   }?>
                   type="<?=$field[2]?>" class="form-control" placeholder="" required>
            </div>
        <?php
        }
        ?>
    </div>
    <h4>Address</h4>
    <div class="divider"></div>
    <div class="form-body row">
        <?php
        $fields = array(
            array(
                'Address',
                'address',
                'text'
            ),
            array(
                'City',
                'city',
                'text'
            ),
            array(
                'State',
                'state',
                'text'
            ),
            array(
                'Zip',
                'zip',
                'text'
            )
        );
        foreach ($fields as $field) {
            ?>
            <div class="form-group col-md-6">
                <label><?=$field[0]?></label>
                <input name="<?=$field[1]?>"
                   <?php if (isset($owner)) echo 'value="'.$owner[$field[1]].'"';?>
                   type="<?=$field[2]?>" class="form-control" placeholder="" required>
            </div>
            <?php
        }
        ?>
    </div>
    <h4>Status</h4>
    <div class="divider"></div>
    <div class="form-body row">
        <div class="form-group col-md-6">
            <select name="status" class="selectpicker">
                <option value="0" <?php if (isset($owner) && $owner['status'] == INACTIVE) echo 'selected';?>>Pending</option>
                <option value="1" <?php if (isset($owner) && $owner['status'] == ACTIVE) echo 'selected';?>>Approved</option>
            </select>
        </div>
    </div>
    <div class="form-body">
        <button class="btn green" style="padding: 5px 60px; float: right;">Save</button>
    </div>
</form>