<h1>Owners</h1>
<div class="table-toolbar">
    <div class="row">
        <div class="col-md-6">
            <div class="btn-group">
                <a href="<?=site_url('admin/owner/add')?>" class="btn sbold green">
                    <i class="fa fa-plus"></i> Add Owner
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="btn-group pull-right">
                <div class="col-md-6 pull-right">
                    <label>Status:</label>
                    <select name="status" class="form-filter selectpicker col-12">
                        <option value="">All</option>
                        <option>Pending</option>
                        <option>Approved</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <input name="s_search" class="form-control form-filter" placeholder="Search">
                </div>
            </div>
        </div>
    </div>
</div>
<table class="table table-striped table-hover order-column my-table" id="table-owners">
    <thead>
        <tr style="background: #eee; padding:">
            <th> First Name </th>
            <th> Last Name </th>
            <th> Email </th>
            <th> Phone </th>
            <th> Date Registered </th>
            <th> Status </th>
            <th> Actions </th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
