<script>
    var owner_table;
    var OwnerTableInit = function () {
        return {
            init: function () {
                owner_table = new Datatable();

                owner_table.init({
                    src: $("#table-owners"),
                    onSuccess: function (grid, response) {
                        // grid:        grid object
                        // response:    json object of server side ajax response
                        // execute some code after table records loaded
                    },
                    onError: function (grid) {
                        // execute some code on network or other general error
                    },
                    onDataLoad: function (grid) {
                        // execute some code on ajax data load
                    },
                    loadingMessage: 'Loading...',
                    dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                        // So when dropdowns used the scrollable div should be removed.
                        //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

                        // save datatable state(pagination, sort, etc) in cookie.
                        "bStateSave": true,

                        // save custom filters to the state
                        "fnStateSaveParams": function (oSettings, sValue) {
                            $("#table-owners tr.filter .form-control").each(function () {
                                sValue[$(this).attr('name')] = $(this).val();
                            });

                            return sValue;
                        },

                        // read the custom filters from saved state and populate the filter inputs
                        "fnStateLoadParams": function (oSettings, oData) {
                            //Load custom filters
                            $("#table-owners tr.filter .form-control").each(function () {
                                var element = $(this);
                                if (oData[element.attr('name')]) {
                                    element.val(oData[element.attr('name')]);
                                }
                            });

                            return true;
                        },

                        "lengthMenu": [
                            [10, 20, 50, 100, 150, -1],
                            [10, 20, 50, 100, 150, "All"] // change per page values here
                        ],
                        "pageLength": 10, // default record count per page
                        "ajax": {
                            "url": "<?=site_url('admin/owner/get_table_data')?>", // ajax source
                        },
                        "ordering": false,
                        // 'serverSide': true,
                        "order": [
                            [1, "asc"]
                        ]// set first column as a default sort by asc
                    }
                });

                $('.table-toolbar .form-filter').change(function() {
                    owner_table.setAjaxParam(this.name, this.value);
                    owner_table.getDataTable().ajax.reload();
                });
            }
        };
    }();

    $(document).ready(function() {
        OwnerTableInit.init();
    });

    function delete_owner(ownerID) {
        swal({
            type: 'warning',
            title: 'Are you sure?',
            text: 'You won\'t be able to revert this!',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it.'
        }, function (value) {
            if (value) {
                $.post({
                    url: '<?=site_url('admin/owner/delete_owner')?>/' +ownerID,
                    // data: { id: ownerID },
                    dataType: 'json',
                    success: function (resp) {
                        if (resp.status) {
                            owner_table.getDataTable().ajax.reload();
                        }
                    }
                });
            }
        });
    }
</script>