<script>

    var process_ajax = function (blockDiv, url, data, successCallback, errorTitle) {
        if (!blockDiv && blockDiv != '') $('#' + blockDiv).prop('disabled', true);
        $(document.body).css({'cursor' : 'wait'});
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: data,
            success: function (resp) {
                $(document.body).css({'cursor' : 'default'});
                if (!blockDiv && blockDiv != '') $('#' + blockDiv).prop('disabled', false);
                if (resp.state == 'success') {
                    successCallback(resp);
                } else {
                    show_toastr("error", resp.message, errorTitle);
                }
            },
            error: function () {
                $(document.body).css({'cursor' : 'default'});
            }
        });
    };

    //Functions End

    var show_toastr = function($type, $msg, $title) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "2000",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toastr[$type]($msg, $title);
    };

    function validate_email(sEmail) {
        var filter = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    }

    function inputImage() {
        $('.img-view').click(function() {
            $(this).closest('div').find('.img-input').click();
            // $('#img-input').click();
        });

        $('.img-input').change(function () {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(input).closest('div').find('.img-view').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        });
    }

    var process_form_data = function (blockDiv, url, data, successCallback, errorTitle) {
        if (!blockDiv && blockDiv != '') $('#' + blockDiv).prop('disabled', true);
        $(document.body).css({'cursor' : 'wait'});

        var request = new XMLHttpRequest();
        request.open("POST", url);

        request.onload = function() {
            var resp;
            resp = JSON.parse(request.responseText);

            $(document.body).css({'cursor' : 'default'});
            if (!blockDiv && blockDiv != '') $('#' + blockDiv).prop('disabled', false);

            if (resp.state == 'success') {
                successCallback(resp);
            } else {
                show_toastr("error", resp.msg, errorTitle);
            }
            return;
        };
        request.send(data);
    };
</script>