<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Meetingspot</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #5 for statistics, charts, recent events and reports" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN LAYOUT FIRST STYLES -->
    <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
    <!-- END LAYOUT FIRST STYLES -->
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url('assets/admin')?>/custom/custom.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/admin'); ?>/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin'); ?>/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin'); ?>/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin'); ?>/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?php echo base_url('assets/admin'); ?>/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin'); ?>/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin'); ?>/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin'); ?>/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin'); ?>/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin'); ?>/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin'); ?>/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?php echo base_url('assets/admin'); ?>/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo base_url('assets/admin'); ?>/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="<?php echo base_url('assets/admin'); ?>/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/admin'); ?>/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?php echo base_url('assets/admin'); ?>/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />

    <!-- custom styles -->
    <link rel="shortcut icon" type="image/png" href="<?= base_url('assets') ?>/admin/custom/images/favicon.png"/>
    <?php

    foreach ($this->custom_css_list as $css) {
        gf_html_load_css($css);
    }
    ?>

    <script type="text/javascript">
        var BASE_URL = "<?php echo base_url('admin') ?>";
    </script>

    <!-- END THEME LAYOUT STYLES -->
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN CONTAINER -->
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?=site_url('admin/dashboard');?>" style="width: 50%">
                        <img src="<?=base_url('assets/admin')?>/custom/images/logo.png" alt="logo" class="logo-default" style="margin-top: 10px;"/>
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                        <span></span>
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    <span></span>
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->

                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="padding-right: 10px;" title="Notfications">
                                <i class="icon-bell"></i>
                                <?php if ($pendingCount > 0) { ?>
                                <span class="badge badge-default"> <?=$pendingCount?> </span>
                                <?php } ?>
                            </a>
                            <?php if ($pendingCount > 0) { ?>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>
                                        <span class="bold"><?=$pendingCount?> Pending</span> Notifications
                                    </h3>
                                    <a href="<?=site_url('admin/notification')?>">View all</a>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                    <?php foreach ($pendingFives as $pending) { ?>
                                        <li>
                                            <a href="<?=site_url('admin/'.notice_menu($pending))?>">
                                                <span class="time"></span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-success">
                                                    <?php if ($pending['method'] == 'ADD') { ?>
                                                        <i class="fa fa-plus"></i>
                                                    <?php } else if ($pending['method'] == 'UPDATE') { ?>
                                                    <?php } else if ($pending['method'] == 'DELETE') { ?>
                                                        <i class="fa fa-trash"></i>
                                                    <?php } ?>
                                                    </span><?= notice_text($pending) ?></span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    </ul>
                                </li>
                            </ul>
                            <?php } ?>
                        </li>
                        <li class="dropdown dropdown-extended dropdown-tasks">
                            <a href="<?=site_url('home')?>" class="dropdown-toggle" target="_blank" data-hover="dropdown" data-close-others="true" aria-expanded="false" title="Home page" style="padding-right: 10px;">
                                <i class="icon-home"></i>
                            </a>
                        </li>
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <span class="username username-hide-on-mobile">Hello, <strong>Admin</strong></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="<?=site_url('admin/profile')?>">
                                       <i class="icon-user"></i> My Profile </a>
                                </li>
                                <li>
                                    <a href="<?=site_url('admin/login/logout')?>">
                                        <i class="icon-logout"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                        <li class="sidebar-toggler-wrapper hide">
                            <div class="sidebar-toggler">
                                <span></span>
                            </div>
                        </li>
                        <!-- END SIDEBAR TOGGLER BUTTON -->
                        <?php foreach ($menus as $menu) { 
                            $has_submenu = array_key_exists('submenu', $menu);?>

                        <li class="nav-item <?=$this->path[1] == $menu['path'] ? 'active open' : ''?>">
                            <a href="<?=$has_submenu ? 'javascript:;' : site_url('admin/'.$menu['path']);?>" class="nav-link nav-toggle">
                                <i class="<?=$menu['icon']?>"></i>
                                <span class="title"><?=$menu['title']?></span>
                                <?php if($this->path[1] == $menu['path']) echo '<span class="selected"></span>'; ?>
                                <?php if($has_submenu) echo '<span class="arrow"></span>'; ?>
                            </a>
                            <?php if($has_submenu) { ?>
                                <ul class="sub-menu">
                                    <?php foreach ($menu['submenu'] as $submenu) { 
                                        $has_submenu2 = array_key_exists('submenu', $submenu); ?>
                                        <li class="nav-item  <?php if($submenu['path'] == $this->path[2]) echo 'active open'?>">
                                            <a href="<?=$has_submenu2 ? 'javascript:;' : site_url('/admin/'.$menu['path'].'/'.$submenu['path']);?>" class="nav-link nav-toggle">
                                                <span class="title"><?=$submenu['text']?></span>
                                                <?php if($submenu['path'] == $this->path[2]) echo '<span class="selected"></span>'; ?>
                                                <?php if($has_submenu2) echo '<span class="arrow"></span>';?>
                                            </a>
                                            <?php if($has_submenu2) { ?>
                                                <ul class="sub-menu">
                                                    <?php foreach($submenu['submenu'] as $submenu2) { ?>
                                                    <li class="nav-item  <?php if($submenu2['path'] == $this->path[3]) echo 'active open'?>">
                                                        <a href="<?=site_url('/admin/'.$menu['path'].'/'.$submenu['path'].'/'.$submenu2['path'])?>" class="nav-link ">
                                                            <span class="title"><?=$submenu2['text']?></span>
                                                            <?php if($submenu2['path'] == $this->path[3]) echo '<span class="selected"></span>'; ?>
                                                        </a> 
                                                    </li>    
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                        <?php } ?>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <?php
                    foreach ($this->view_list as $view) {
                        $view_page = $view[0];
                        $view_param = $view[1];
                        $this->load->view($view_page, $view_param);
                    }
                    ?>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->

        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy; Meetingspot
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
    </div>
    <!-- END CONTAINER -->
    <!-- <div class="blockui-container" style="position: relative; bottom: 200px"></div> -->
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/moment.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/fullcalendar/fullcalendar.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?php echo base_url('assets/admin'); ?>/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="<?php echo base_url('assets/admin'); ?>/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/admin/global/plugins/bootstrap-sweetalert/sweetalert.min.js')?>" type="text/javascript"></script>

    <!-- END THEME LAYOUT SCRIPTS -->
    <?php

    foreach ($this->custom_js_list as $js) {
        gf_html_load_js($js);
    }

    $this->load->view('admin/partial/global-js');
    $this->load->view('admin/'.$jslink);

    ?>
</body>

</html>