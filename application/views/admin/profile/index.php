<h1 class="page-title"> Edit Profile</h1>
<br><br>
<div style="display: flex;">
    <div class="col-md-8 form" style="margin: 10px auto;">
        <?php if (isset($msg)) { ?>
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <span> <?=$msg?> </span>
            </div>
        <?php } ?>
        <div class="alert alert-danger" <?=!isset($error) ? 'hidden' : ''?>>
            <button class="close" data-close="alert"></button>
            <span> <?=isset($error) ? $error: ''?> </span>
        </div>
        <div class="tabbable-custom nav-justified">
            <ul class="nav nav-tabs nav-justified">
                <li <?php if (!$show_passwords) echo 'class="active"';?>>
                    <a href="#tab_1_1_1" data-toggle="tab" aria-expanded="true"> Personal Info </a>
                </li>
                <li <?php if ($show_passwords) echo 'class="active"';?>>
                    <a href="#tab_1_1_2" data-toggle="tab" aria-expanded="false"> Change Password </a>
                </li>
            </ul>
            <div class="tab-content">
                <br>
                <div class="tab-pane <?php if (!$show_passwords) echo 'active';?>" id="tab_1_1_1">
                <form id="form-info" action="<?=site_url('admin/profile')?>" method="post">
                <?php
                    $fields = array(
                        array(
                            'FIrst Name',
                            'first_name',
                            'text'
                        ),
                        array(
                            'Last Name',
                            'last_name',
                            'text'
                        ),
                        array(
                            'User Name',
                            'username',
                            'text'
                        ),
                        array(
                            'Email',
                            'email',
                            'email'
                        ),
                        array(
                            'Phone',
                            'phone',
                            'text'
                        )
                    );
                    foreach ($fields as $field) {
                        ?>
                        <div class="form-group row">
                            <label class="col-md-6" style="margin: 7px 0px;"><?=$field[0]?></label>
                            <div class="col-md-6">
                                <input name="<?=$field[1]?>"
                                    <?php if ($field[2] != 'password') echo ' required';
                                    if ($field[2] !='password' || $show_passwords) echo  ' value="'.$admin[$field[1]].'"';//means you have input
                                    ?>
                                type="<?=$field[2]?>" class="form-control" placeholder="">
                            </div>
                        </div>
                        <?php
                    }
                ?>
                    <div class="col-12" style="margin-top: 60px;">
                        <button class="btn green" style="padding: 5px 60px;">Update</button>
                    </div>
                </form>
                </div>
                <br>
                <div class="tab-pane <?php if ($show_passwords) echo 'active';?>" id="tab_1_1_2">
                <form class="form-password" action="<?=site_url('admin/profile')?>" method="post">
                <?php
                    $fields = array(
                        array(
                            'Current Password',
                            'cur_password',
                            'password'
                        ),
                        array(
                            'New Password',
                            'password',
                            'password'
                        ),
                        array(
                            'Confirm Password',
                            'confirm_password',
                            'password'
                        )
                    );
                    foreach ($fields as $field) {
                        ?>
                        <div class="form-group row">
                            <label class="col-md-6" style="margin: 7px 0px;"><?=$field[0]?></label>
                            <div class="col-md-6">
                                <input name="<?=$field[1]?>" required
                                    <?php
                                    if ($field[2] !='password' || $show_passwords) echo  ' value="'.$admin[$field[1]].'"';//means you have input
                                    ?>
                                type="<?=$field[2]?>" class="form-control" placeholder="">
                            </div>
                        </div>
                        <?php
                    }
                ?>
                    <div class="col-12" style="margin-top: 60px;">
                        <button class="btn green" style="padding: 5px 60px;">Update</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <div class="form-body">
            
        </div>
    </form>
</div>