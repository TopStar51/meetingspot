<script>
    $(document).ready(function() {
        $('.form-password').submit(function(e) {
            if ($('input[name="password"]').val() != $('input[name="confirm_password"]').val()) {
                $('.alert-danger span').html('Passwords doesn\'t match.');
                $('.alert-danger').show();

                e.preventDefault();
            }
        });
    });
</script>