<h1 class="page-title">Reservations/Details</h1>
<div class="row">
    <div class="col-md-4">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    Reservation
                </div>
            </div>
            <div class="portlet-body">
                <div class="my-div1">
                    <label>Reservation ID</label>
                    <span>#<?=$reservation['res_number']?></span>
                </div>
                <div class="my-div1">
                    <label>Room Name</label>
                    <span><?=$reservation['name']?></span>
                </div>
                <div class="my-div1">
                    <label>Guest Number</label>
                    <span><?=$reservation['guest_number']?> person(s)</span>
                </div>
                <div class="my-div1">
                    <label>Reservation Date</label>
                    <span><?=$reservation['res_date']?></span>
                </div>
                <div class="my-div1">
                    <label>Start Time</label>
                    <span><?=$reservation['start_time']?></span>
                </div>
                <div class="my-div1">
                    <label>End Time</label>
                    <span><?=$reservation['end_time']?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    Customer
                </div>
            </div>
            <div class="portlet-body">
                <div class="my-div1">
                    <label>Name</label>
                    <span><?=$customer['first_name'].' '.$customer['last_name']?></span>
                </div>
                <div class="my-div1">
                    <label>Email</label>
                    <span><?=$customer['email']?></span>
                </div>
                <div class="my-div1">
                    <label>Telephone</label>
                    <span><?=$customer['phone']?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <div class="my-div1">
                    <label>Date Added</label>
                    <span><?=$reservation['created_at']?></span>
                </div>
                <div class="my-div1">
                    <label>Date Modified</label>
                    <span><?=$reservation['updated_at']?></span>
                </div>
                <div class="my-div1">
                    <label>Customer Modified</label>
                    <span><?=$reservation['updated_at']?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    Location Details
                </div>
            </div>
            <div class="portlet-body row">
                <div class="my-div2 col-md-4">
                    <label>Name</label>
                    <span><?=$location['name']?></span>
                </div>
                <div class="my-div2 col-md-4">
                    <label>Owner</label>
                    <span><?=$location['owner']?></span>
                </div>
                <div class="my-div2 col-md-4">
                    <label>Category</label>
                    <span><?=$location['category']?></span>
                </div>
                <div class="my-div2 col-md-12">
                    <label>Address</label>
                    <span><?=$location['address']?></span>
                </div>
            </div>
        </div>
    </div>
</div>