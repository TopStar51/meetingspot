<h1 class="page-title">Reservations</h1>
<div class="table-toolbar">
    <div class="row">
        <div class="btn-group pull-right">
            <input name="s_search" class="form-control form-filter" placeholder="Search">
        </div>
    </div>
</div>
<table class="table table-striped table-hover order-column my-table" id="table-reservations">
    <thead>
    <tr style="background: #eee; padding:">
        <th> ID </th>
        <th> Customer </th>
        <th> Location </th>
        <th> Room </th>
        <th> Guests </th>
        <th> Reservation Date </th>
        <th> Start Time </th>
        <th> End Time </th>
        <th> Status </th>
        <th> Actions </th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
