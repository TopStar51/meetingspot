<h1 class="page-title">Reviews</h1>
<div>
    <div class="col-md-offset-2 col-md-8" style="margin-top: 20px;">
        <div class="my-div4 row">
            <label class="col-md-6">Location</label>
            <span class="col-md-6"><?=$review['location']?></span>
        </div>
        <div class="my-div4 row">
            <label class="col-md-6">Reservation ID</label>
            <span class="col-md-6"><?=$review['res_number']?></span>
        </div>
        <div class="my-div4 row">
            <label class="col-md-6">Author</label>
            <span class="col-md-6"><?=$review['author']?></span>
        </div>
        <div class="my-div4 row">
            <label class="col-md-6">Rating</label>
            <div class="col-md-6">
                <div class="star-rating" data-rating="<?=$review['rating']?>"></div>
            </div>
        </div>
        <div class="my-div4 row">
            <label class="col-md-6">Comment</label>
            <textarea class="col-md-6" style="height: 100px; padding: 5px 10px;"><?=$review['comment']?></textarea>
        </div>
        <div class="my-div4 row">
            <label class="col-md-6">Review Status</label>
            <select class="selectpicker">
                <option <?=$review['status']==INACTIVE ? 'selected' : ''?>>Pending</option>
                <option <?=$review['status']==ACTIVE ? 'selected' : ''?>>Approved</option>
            </select>
        </div>
    </div>
</div>