<script>
    function starRating(ratingElem) {
        ratingElem.each(function() {
            var dataRating = $(this).attr('data-rating');

            // Rating Stars Output
            for (var i = 0; i < 5; i += 1) {
                html = '<span class="';
                if (dataRating - i >= 0.75) {
                    html += 'star';
                } else if (dataRating - i >= 0.25) {
                    html += 'star half';
                } else
                    html += 'star empty';
                html += '"></span>';
                $(this).append(html);
            }
        });
    }

    $(document).ready(function() {
        starRating($('.star-rating'));
    });
</script>