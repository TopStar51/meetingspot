<h1 class="page-title">Reviews</h1>
<div class="table-toolbar">
    <div class="row">
        <div class="btn-group pull-right">
            <input name="s_search" class="form-control form-filter" placeholder="Search">
        </div>
    </div>
</div>
<table class="table table-striped table-hover order-column my-table" id="table-reviews">
    <thead>
    <tr style="background: #eee; padding:">
        <th> Review ID </th>
        <th> Author </th>
        <th> Location </th>
        <th> Rating </th>
        <th> Status </th>
        <th> Actions </th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
