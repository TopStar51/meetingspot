<script type="text/javascript">

    var dzThumbnail = null;
    var dzGallery = null;

	var BootstrapSwitch = function () {

        //== Private functions
        var demos = function () {
            // minimum setup
            $('[data-switch=true]').bootstrapSwitch();
        }

        return {
            // public functions
            init: function () {
                demos();
            }
        };
    }();

    /*var DropzoneDemo = function () {
        //== Private functions
        var demos = function () {
            
            // file type validation
            Dropzone.options.thumbDropzone = {
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 1,
                maxFilesize: 10, // MB
                addRemoveLinks: true,
                dictRemoveFile: "Remove File",
                acceptedFiles: "image/*",
                init: function() {
                    dzThumbnail = this;
                }
            };

            Dropzone.options.galleryDropzone = {
                acceptedFiles: 'image/*',
                paramName: "file", 
                maxFilesize: 10, // MB
                // autoProcessQueue: false,
                addRemoveLinks: true,
                dictRemoveFile: "Remove File",
                init: function() {
                    dzGallery = this;
                }
            };
        }

        return {
            // public functions
            init: function () {
                demos();
            }
        };
    }();*/


    jQuery(document).ready(function () {
        $("select[name='owner_id']").select2({
            width: "off",
            ajax: {
                url: "<?=site_url('admin/owner/ajax_owner_list')?>",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page//?
                    };
                },
                processResults: function(data, page) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: function(owner) {
                return owner.last_name;
            },
            templateSelection: function (owner) {
                return owner.last_name;
            }
        });
        
        BootstrapSwitch.init();
        //inputImage();
        $('#form-room').submit(function(e) {
            e.preventDefault();

            var formData = new FormData();
            var form = this;
            for (var i = 0; i < form.length; i += 1) {
                if (form[i].type == 'checkbox') {
                    formData.append(form[i].name, form[i].checked);
                } else {
                    formData.append(form[i].name, form[i].value);
                }
            }

/*            if (dzThumbnail && dzThumbnail.files.length > 0) {
                formData.append('thumb_image', dzThumbnail.files[0]);
            }
            if (dzGallery && dzGallery.files.length > 0) {
                for (var i = 0; i < dzGallery.files.length; i += 1)
                    formData.append('gallery[]', dzGallery.files[i]);
            }
*/
            process_form_data(
                $(this),
                '<?=site_url('admin/room/ajax_save')?>',
                formData,
                function(resp) {
                    // alert('success');
                    window.location.href = '<?=site_url('admin/room')?>';
                },
                function (err) {
                    alert('error occurred');
                }
            );
        });

        //set owner_id, category_id to select
        <?php if (isset($location)) { ?>
        //$('select[name="owner_id"]')({name: '<?//=$location['owner']?>//', id: <?//=$location['owner_id']?>//});
        $('select[name="owner_id"]').closest('div').find('.select2-selection__rendered').html('<?=$location['owner']?>');
        <?php } ?>

        $('.selectpicker').selectpicker();
    });

    //DropzoneDemo.init();
</script>