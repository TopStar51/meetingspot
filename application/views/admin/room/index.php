<h1>Rooms</h1>
<div class="table-toolbar">
    <div class="row">
        <div class="col-md-6">
            <div class="btn-group">
                <a href="<?=site_url('admin/room/create_room')?>" class="btn sbold green hide">
                    <i class="fa fa-plus"></i> Add Room
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="btn-group pull-right">
                <input name="s_search" class="form-control form-filter" placeholder="Search">
            </div>
        </div>
    </div>
</div>
<table class="table table-striped table-hover order-column my-table" id="table-rooms">
    <thead>
        <tr style="background: #eee;">
            <th width="20%"> Name </th>
            <th> Capacity </th>
            <th> Location </th>
            <th> Date Registered </th>
            <th width="10%"> Actions </th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
