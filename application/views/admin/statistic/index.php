<h1 class="page-title">Statistics</h1>
<div class="row">
    <div class="form-group">
        <div class="col-md-3">
            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm" data-date-viewmode="years" data-date-minviewmode="months">
                <input type="text" class="form-control" readonly value="<?=date("Y-m")?>" id="datepicker">
                <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <h4>Customers</h4>
        <div id="chart_customer" class="chart" style="height: 500px;"></div>
    </div>
    <div class="col-md-6">
        <h4>Locations</h4>
        <div id="chart_location" class="chart" style="height: 500px;"></div>
    </div>
    <div class="col-md-6">
        <h4>Reservations</h4>
        <div id="chart_reservation" class="chart" style="height: 500px;"></div>
    </div>
</div>