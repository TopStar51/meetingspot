<script>
    var date_start, date_end;
    $(document).ready(function () {

        $('#datepicker').change(function () {
            reload_charts();
        });
        reload_charts();
    });

    var initChart = function(chartid, valuelist) {
        AmCharts.makeChart(chartid, {
            "type": "serial",
            "theme": "light",

            "fontFamily": 'Open Sans',
            "color":    '#888888',

            "dataProvider": valuelist,
            "balloon": {
                "cornerRadius": 6
            },
            "graphs": [{
                "bullet": "square",
                "bulletBorderAlpha": 1,
                "bulletBorderThickness": 1,
                "fillAlphas": 0.3,
                "fillColorsField": "lineColor",
                "legendValueText": "[[value]]",
                "lineColorField": "lineColor",
                "title": "Customers",
                "valueField": "count"
            }],
            "chartScrollbar": {},
            "chartCursor": {
                "categoryBalloonDateFormat": "YYYY MMM DD",
                "cursorAlpha": 0,
                "zoomable": false
            },
            "dataDateFormat": "YYYY-MM-DD",
            "categoryField": "date",
            "categoryAxis": {
                "dateFormats": [{
                    "period": "DD",
                    "format": "DD"
                }, {
                    "period": "WW",
                    "format": "MMM DD"
                }, {
                    "period": "MM",
                    "format": "MMM"
                }, {
                    "period": "YYYY",
                    "format": "YYYY"
                }],
                "parseDates": true,
                "autoGridCount": false,
                "axisColor": "#555555",
                "gridAlpha": 0,
                "gridCount": 50
            }
        });
    };

    function reload_charts() {
        // return;
        $.post({
            url: "<?=site_url('admin/statistic/get_statistic')?>",
            data: {
                date: $('#datepicker').val()
            },
            dataType: 'json',
            success: function (resp) {
                if (resp.status) {
                    initChart('chart_customer', resp.customer);
                    initChart('chart_location', resp.location);
                    initChart('chart_reservation', resp.reservation);
                }
            }
        });
    }
</script>