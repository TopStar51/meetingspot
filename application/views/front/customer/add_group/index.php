<style type="text/css">
    .filter-tags {
        margin: 20px 10px;
    }
</style>
<div class="col-md-9">
    <!-- profile-edit-container--> 
    <div class="profile-edit-container">
        <div class="custom-form">
        <form id="form_add_group">
            <input hidden type="text" name="id" value="<?=isset($group_info)? $group_info['id']: '' ;?>" />
            <div class="pass-input-wrap fl-wrap">
                <label> Group Name <i class="fa fa-briefcase"></i></label>
                <input name="group_name" type="text" required value="<?=isset($group_info)? $group_info['group_name']:''?>"/>
            </div>
            <div class="pass-input-wrap fl-wrap">
                <label> Address <i class="fa fa-map-marker"></i></label>
                <input name="address" type="text" value="<?=isset($group_info)? $group_info['address']:''?>" />
            </div>
            <div class="pass-input-wrap fl-wrap">
                <label> Contact Number <i class="fa fa-phone"></i></label>
                <input name="phone" type="text" value="<?=isset($group_info)? $group_info['phone']:''?>"/>
            </div>
            <div class="pass-input-wrap fl-wrap row">
                <div class="col-md-6">
                    <label> Website <i class="fa fa-globe"></i></label>
                    <input name="website" type="text" value="<?=isset($group_info)? $group_info['website']:''?>"/>
                </div>
                <div class="col-md-6">
                    <label> Facebook <i class="fa fa-facebook"></i></label>
                    <input name="facebook" type="text" value="<?=isset($group_info)? $group_info['facebook']:''?>"/>
                </div>
            </div>
            <div class="pass-input-wrap">
                <label> Nature of Group </label>
                <div class="fl-wrap filter-tags row">
                    <div class="col-md-3">
                        <input id="civic" name="nature_of_group" type="radio" value="civic" checked>
                        <label for="civic">Civic</label>
                    </div>
                    <div class="col-md-3">
                        <input id="religious" name="nature_of_group" type="radio" value="religious" <?php if(isset($group_info) && $group_info['nature_of_group'] == 'religious') echo 'checked';?>>
                        <label for="religious">Religious</label>
                    </div>
                    <div class="col-md-3">
                        <input id="sports" name="nature_of_group" type="radio" value="sports" <?php if(isset($group_info) && $group_info['nature_of_group'] == 'sports') echo 'checked';?>>
                        <label for="sports">Sports</label>
                    </div>
                </div>
            </div>
            <div class="pass-input-wrap fl-wrap row">
                 <div class="col-md-6">
                    <label> Minimum age of members <i class="fa fa-user"></i></label>
                    <input name="min_age" type="text" required value="<?=isset($group_info)? $group_info['min_age']:''?>"/>
                </div>
            </div>
            <button class="btn  big-btn  color-bg flat-btn">Save <i class="fa fa-angle-right"></i></button>
        </form>
        </div>
    </div>
    <!-- profile-edit-container end-->                                        
</div>