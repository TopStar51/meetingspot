<script>
    $(document).ready(function() {
        $('#form_add_group').submit(function(e) {
            e.preventDefault();
            $.post({
                url: '<?=site_url('customer/add_group/ajax_save')?>',
                data: $(this).serializeArray(),
                dataType: 'json',
                success: function(resp) {
                    if (resp.status) {
                        //notice succss
                        window.location.href = '<?=site_url()?>' + resp.url;
                    }
                },
                error: function(err) {

                }
            })
        });
    });
</script>
