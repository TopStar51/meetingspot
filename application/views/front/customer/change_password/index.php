<div class="col-md-9">
    <!-- profile-edit-container--> 
    <div class="profile-edit-container">
        <div class="profile-edit-header fl-wrap" style="margin-top:30px">
            <h4>Change Password</h4>
        </div>
        <div class="custom-form no-icons">
        <form>
            <div class="pass-input-wrap fl-wrap">
                <label>Current Password</label>
                <input name="cur_password" type="password" class="pass-input" placeholder="" value="" required/>
                <span class="eye"><i class="fa fa-eye" aria-hidden="true"></i> </span>
            </div>
            <div class="pass-input-wrap fl-wrap">
                <label>New Password</label>
                <input name="new_password" type="password" class="pass-input" placeholder="" value="" required/>
                <span class="eye"><i class="fa fa-eye" aria-hidden="true"></i> </span>
            </div>
            <div class="pass-input-wrap fl-wrap">
                <label>Confirm New Password</label>
                <input name="confirm_password" type="password" class="pass-input" placeholder="" value="" required/>
                <span class="eye"><i class="fa fa-eye" aria-hidden="true"></i> </span>
            </div>
            <button class="btn  big-btn  color-bg flat-btn">Save Changes<i class="fa fa-angle-right"></i></button>
        </form>
        </div>
    </div>
    <!-- profile-edit-container end-->                                        
</div>