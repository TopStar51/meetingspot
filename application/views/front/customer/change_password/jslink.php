<script>
    $(document).ready(function() {
        $('.profile-edit-container form').submit(function(e) {
            e.preventDefault();
            
            if ($('input[name="new_password"]').val() != $('input[name="confirm_password"]').val()) {
                alert('New password doesn\'t match.');
                return;
            }

            $.post({
                url: "<?=site_url('customer/change_password/ajax_save')?>",
                data: $(this).serializeArray(),
                dataType: 'json',
                success: function(resp) {
                    if (resp.status) {
                        //notice success
                        swal('', 'Password changed successfully','success');
                    } else {
                        //error
                        swal('', resp.msg, 'error');
                    }
                }
            });
        });
    });
</script>