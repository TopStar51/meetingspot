<!-- wrapper -->	
<div id="wrapper">
    <!--content -->  
    <div class="content">
        <!--section --> 
        <section>
            <!-- container -->
            <div class="container">
                <!-- profile-edit-wrap -->
                <div class="profile-edit-wrap">
                    <div class="profile-edit-page-header">
                        <h2><?=$page_title?></h2>
                        <div class="breadcrumbs"><a href="#">Home</a><span><?=$page_title?></span></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="fixed-bar fl-wrap">
                                <div class="user-profile-menu-wrap fl-wrap">
                                    <!-- user-profile-menu-->
                                    <div class="user-profile-menu">
                                        <h3>Main</h3>
                                        <ul>													
                                            <li><a href="<?=site_url('customer/profile')?>" class="<?=$path[1] =='profile' ? 'user-profile-act' : ''?>"><i class="fa fa-user-o"></i> Edit profile</a></li>
                                            <li><a href="<?=site_url('customer/change_password')?>" class="<?=$path[1] =='change_password' ? 'user-profile-act' : ''?>"><i class="fa fa-unlock-alt"></i>Change Password</a></li>
                                            <!-- <li><a href="<?=site_url('customer/message')?>"><i class="fa fa-envelope-o"></i> Messages <span>3</span></a></li> -->
                                            <li><a href="<?=site_url('customer/mygroup')?>" class="<?=$path[1] == 'mygroup' ? 'user-profile-act' : ''?>"><i class="fa fa-users"></i> My Group </a></li>
                                            <li><a href="<?=site_url('customer/reservation')?>" class="<?=$path[1] =='reservation' ? 'user-profile-act' : ''?>"> <i class="fa fa-calendar-check-o"></i> Recent Reservations </a></li>
                                            <li><a href="<?=site_url('customer/review')?>" class="<?=$path[1] =='review' ? 'user-profile-act' : ''?>"><i class="fa fa-comments-o"></i> Recent Reviews </a></li>
                                            <li><a href="<?=site_url('customer/add_group')?>" class="<?=$path[1] == 'add_group' ? 'user-profile-act' : ''?>"><i class="fa fa-plus-square-o"></i> Add Group </a></li>
                                            <li><a href="<?=site_url('customer/notification')?>" class="<?=$path[1] == 'notification' ? 'user-profile-act' : ''?>"><i class="fa fa-bell"></i> Notifications<?php if ($pendingCount > 0) echo '<span>'.$pendingCount.'</span>'?></a></li>
                                        </ul>
                                    </div>
                                    <!-- user-profile-menu end-->                                        
                                    <a href="<?=site_url('customer/logout')?>" class="log-out-btn">Log Out</a>
                                </div>
                            </div>
                        </div>
                        <?php
                        foreach ($this->view_list as $view) {
                            $view_page = $view[0];
                            $view_param = $view[1];
                            $this->load->view($view_page, $view_param);
                        }
                        ?>
                    </div>
                </div>
                <!--profile-edit-wrap end -->
            </div>
            <!--container end -->
        </section>
        <!-- section end -->
        <div class="limit-box fl-wrap"></div>
        <!--section -->
        <section class="gradient-bg">
            <div class="cirle-bg">
                <div class="bg" data-bg="<?=base_url('assets/front/')?>images/bg/circle.png"></div>
            </div>
            <div class="container">
                <div class="join-wrap fl-wrap">
                    <div class="row">
                        <div class="col-md-8">
                            <h3>Do You Have Questions ?</h3>
                            <p>Lorem ipsum dolor sit amet, harum dolor nec in, usu molestiae at no.</p>
                        </div>
                        <div class="col-md-4"><a href="contacts.html" class="join-wrap-btn">Get In Touch <i class="fa fa-envelope-o"></i></a></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end -->
    </div>
</div>
<!-- wrapper end -->