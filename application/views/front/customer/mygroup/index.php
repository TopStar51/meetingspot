<style type="text/css">
    .group-info {
        padding-bottom : 20px;
        font-size: 13px;
    }

    .group-info i {
        color: #4DB7FE;
    }

    .row {
        margin-bottom: 15px;
    }
</style>
<div class="col-md-9">
    <div class="dashboard-list-box fl-wrap">
        <div class="dashboard-header fl-wrap">
            <h3>My Group</h3>
        </div>
        <!-- dashboard-list end-->    
        <div class="dashboard-list">
            <div class="dashboard-message">
                <div class="dashboard-listing-table-text">
                    <?php if(!empty($group_info)) { ?>
                        <h4><?=$group_info['group_name']; ?></h4>
                        <div class="row">
                        <?php if(!empty($group_info['address'])) { ?>
                            <div class="col-md-6">
                                <span class="group-info"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?=$group_info['address']?></span>
                            </div>
                        <?php } ?>
                        <?php if(!empty($group_info['phone'])) { ?>
                            <div class="col-md-6">
                            <span class="group-info"><i class="fa fa-phone"></i>&nbsp;&nbsp;<?=$group_info['phone']?></span></div>
                        <?php } ?>
                        </div>
                        <div class="row">
                        <?php if(!empty($group_info['website'])) { ?>
                            <div class="col-md-6">
                                <span class="group-info"><i class="fa fa-globe"></i><a href="<?=$group_info['website']?>" target="_blank">&nbsp;&nbsp;<?=$group_info['website']?></a></span>
                            </div>
                        <?php } ?>
                        <?php if(!empty($group_info['facebook'])) { ?>
                            <div class="col-md-6">
                                <span class="group-info"><i class="fa fa-facebook"></i><a href="<?=$group_info['facebook']?>" target="_blank">&nbsp;&nbsp;<?=$group_info['facebook']?></a></span>
                            </div>
                        <?php } ?>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <span class="group-info" style="font-weight: bold">Nature of group:</span> &nbsp;&nbsp;&nbsp;<?=$group_info['nature_of_group']?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <span class="group-info" style="font-weight: bold">Minimun age of members:</span> &nbsp;&nbsp;&nbsp; <?=$group_info['min_age']?>
                            </div>
                        </div>
                        <ul class="dashboard-listing-table-opt fl-wrap">
                            <li><a href="<?=site_url('customer/edit_group/index/').$group_info['id']?>">Edit <i class="fa fa-pencil-square-o"></i></a></li>
                            <li><a href="#" class="del-btn" data-id="<?=$group_info['id']?>">Delete <i class="fa fa-trash-o"></i></a></li>
                        </ul>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- dashboard-list end-->
    </div>
</div>