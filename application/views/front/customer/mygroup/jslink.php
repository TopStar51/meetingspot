<script>
    $(document).ready(function() {
        $('.del-btn').click(function(e){
            e.preventDefault();
            var groupId = $(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this information",
                icon: 'warning',
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
            }).then(function(result){
                if(result){
                    $.ajax({
                        url: "<?=site_url('customer/mygroup/del_group/')?>",
                        type: 'POST',
                        data: {group_id: groupId},
                        dataType: 'json',
                        success: function(resp) {
                            if(resp.state == 'success') {
                                window.location.reload();
                            }
                        }
                    });
                }
            });
        })
    });
</script>
