<div class="dashboard-list-box fl-wrap">
    <div class="dashboard-header fl-wrap">
        <h3>Notifications</h3>
    </div>
    <div class="box-widget opening-hours">
        <div class="box-widget-content">
            <ul>
            <?php foreach ($notifications as $notification) { ?>
                <li><span class="opening-hours-day"><a href="<?=$notification['url']?>"><?=$notification['content']?></a></span><span class="opening-hours-time"><?=$notification['created_at']?></span></li>
            <?php } ?>
            </ul>
        </div>
    </div>
</div>
<!-- pagination-->
<?=$links?>
