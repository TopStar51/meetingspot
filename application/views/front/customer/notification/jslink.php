<script>
function set_notification_page(cur_page) {
    $.post({
        url: '<?=site_url('customer/notification/ajax_pagination')?>',
        data: {
            cur_page: cur_page
        },
        dataType: 'json',
        success: function(resp) {
            if (resp.status) {
                $('#div-notifications').html(resp.htmlData);
            }
        }
    });
}

$(document).ready(function() {
    set_notification_page(0);
});
</script>