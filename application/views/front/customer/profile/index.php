<div class="col-md-9">
    <!-- profile-edit-container--> 
    <div class="profile-edit-container">
        <div class="profile-edit-header fl-wrap">
            <h4>My Account</h4>
        </div>
        <div class="custom-form">
        <form id="form-profile">
            <label>First Name <i class="fa fa-user-o"></i></label>
            <input name="first_name" type="text" value="<?=$customer['first_name']?>" required/>
            <label>Last Name <i class="fa fa-user-o"></i></label>
            <input name="last_name" type="text" value="<?=$customer['last_name']?>" required/>
            <label>Email Address<i class="fa fa-envelope-o"></i>  </label>
            <input name="email" type="email" value="<?=$customer['email']?>" required/>
            <label>Phone<i class="fa fa-phone"></i>  </label>
            <input name="phone" type="text" value="<?=$customer['phone']?>"/>
            <label> Address1 <i class="fa fa-map-marker"></i>  </label>
            <input name="address" type="text" value="<?=$customer['address']?>" required/>
            <label> Address2 <i class="fa fa-map-marker"></i>  </label>
            <input name="address2" type="text" value="<?=$customer['address2']?>" required/>
            <label> City <i class="fa fa-map-marker"></i>  </label>
            <input name="city" type="text" value="<?=$customer['city']?>" required/>
            <label> State <i class="fa fa-map-marker"></i>  </label>
            <input name="state" type="text" value="<?=$customer['state']?>" required/>
            <label> Zip code <i class="fa fa-map-marker"></i>  </label>
            <input name="zip" type="text" value="<?=$customer['zip']?>" required/>
            <button class="btn  big-btn  color-bg flat-btn">Save Changes<i class="fa fa-angle-right"></i></button>
        </form>
        </div>
    </div>
    <!-- profile-edit-container end-->   
</div>
<!-- <div class="col-md-2">
    <div class="edit-profile-photo fl-wrap">
        <img src="<?=base_url('assets/front/')?>images/avatar/avatar-bg.png" class="respimg" alt="">
        <div class="change-photo-btn">
            <div class="photoUpload">
                <span><i class="fa fa-upload"></i> Upload Photo</span>
                <input type="file" class="upload">
            </div>
        </div>
    </div>
</div> -->