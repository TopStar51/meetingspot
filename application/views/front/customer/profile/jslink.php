<script>
    $(document).ready(function() {
        $('#form-profile').submit(function(e) {
            e.preventDefault();
            $.post({
                url: '<?=site_url('customer/profile/ajax_save')?>',
                data: $(this).serializeArray(),
                dataType: 'json',
                success: function(resp) {
                    if (resp.status) {
                        //notice succss
                        window.location.reload();
                    }
                },
                error: function(err) {

                }
            })
        });
    });
</script>
