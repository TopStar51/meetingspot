<div class="dashboard-list-box fl-wrap">
    <div class="dashboard-header fl-wrap">
        <h3>Reservations</h3>
    </div>
<?php 
$status_arr = array('Pending', 'Confirmed', 'Cancel');
$status_color = array('crimson', 'green', 'yellow');
foreach ($reservations as $reservation) { ?>
    <!-- dashboard-list end-->    
    <div class="dashboard-list">
        <div class="dashboard-message">
            <span class="new-dashboard-item" style="background: <?=$status_color[$reservation['status']]?>; <?php if($reservation['status'] == 2) echo 'color: black;'?>"><?=$status_arr[$reservation['status']]?></span>
            <!-- <div class="dashboard-message-avatar">
                <img src="<?=base_url('assets/front/')?>images/avatar/avatar-bg.png" alt="">
            </div> -->
            <div class="dashboard-message-text">
                <div class="booking-details fl-wrap">
                    <span class="reviews-comments-item-date"><i class="fa fa-calendar"></i><?=date_format(new DateTime($reservation['created_at']), 'Y-m-i')?></span>
                </div>
                <!-- <div class="booking-details fl-wrap">
                    <span class="booking-title">Reservation Date</span> :
                    <span class="booking-text"><?=date_format(new DateTime($reservation['created_at']), 'Y-m-i')?></span>
                </div> -->
                <div class="booking-details fl-wrap">
                    <span class="booking-title">Reservation Number</span> : 
                    <span class="booking-text"><?=$reservation['res_number']?></span>
                </div>
                <div class="booking-details fl-wrap">
                    <span class="booking-title">Location</span> :
                    <span class="booking-text"><a href="<?=site_url('location_single/index/'.$reservation['location_id'])?>" target="_blank"><?=$reservation['location']?></a></span>
                </div>
                <div class="booking-details fl-wrap">
                    <span class="booking-title">Room </span> : 
                    <span class="booking-text"><?=$reservation['room']?></span>
                </div>
                <?php if ($reservation['group']) { ?>
                <div class="booking-details fl-wrap">
                    <span class="booking-title">Group </span> : 
                    <span class="booking-text"><?=$reservation['group']?></span>
                </div>
                <?php } ?>
                <div class="booking-details fl-wrap">
                    <span class="booking-title">Guest Number </span> : 
                    <span class="booking-text"><?=$reservation['guest_number']?></span>
                </div>
                <div class="booking-details fl-wrap">                          
                    <span class="booking-title">Date</span> : 
                    <span class="booking-text"><?=$reservation['res_date']?></a></span>
                </div>
                <div class="booking-details fl-wrap">                          
                    <span class="booking-title">Start Time</span> : 
                    <span class="booking-text"><?=$reservation['start_time']?></a></span>
                </div>
                <div class="booking-details fl-wrap">
                    <span class="booking-title">End Time</span> : 
                    <span class="booking-text"><?=$reservation['end_time']?></a></span>
                </div>
            </div>
            <?php if ($reservation['status'] == 0) { ?>
            <div class="booking-details custom-form fl-wrap" style="text-align: right;">
                <a href="<?=site_url('customer/reservation/edit/'.$reservation['id'])?>" class="btn color-bg flat-btn" style="float: right; margin-top: 0px;">Edit Reservation</a>
            </div>
            <?php } ?>
        </div>
    </div>
    <!-- dashboard-list end-->
<?php } ?>
       
</div>
<!-- pagination-->
<?=$links?>