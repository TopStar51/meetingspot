<div class="col-md-9">
    <!-- profile-edit-container--> 
    <div class="profile-edit-container">
        <div class="profile-edit-header fl-wrap">
            <h4>Edit Reservation</h4>
        </div>
        <form id="form-reservation">
            <?php if (isset($reservation)) { ?>
            <input style="display: none;" name="id" value="<?=$reservation['id']?>">
            <input hidden name="room_id" value="<?=$reservation['room_id']?>">
            <?php } ?>
            <label style="float: left; margin-bottom: 10px; color: #666;">Room Name</label>
            <div class="listsearch-input-item" style="margin-bottom: 20px; width: 100%; padding: 0px;">
                <select name="room_id" data-placeholder="Select Room" class="chosen-select" disabled="">
                    <?php foreach ($rooms as $room) { ?>
                    <option value="<?=$room['id']?>" <?php if (isset($reservation) && $reservation['room_id'] == $room['id']) echo 'selected';?>><?=$room['name']?></option>
                    <?php } ?>
                </select>
            </div>
            <label style="float: left; margin-bottom: 10px; color: #666;">Group Name</label>
            <div class="listsearch-input-item" style="margin-bottom: 20px; width: 100%; padding: 0px;">
                <select name="group_id" data-placeholder="Select Group" class="chosen-select" >
                    <?php foreach ($groups as $group) { ?>
                    <option value="<?=$group['id']?>" <?php if (isset($reservation) && $reservation['group_id'] == $group['id']) echo 'selected';?>><?=$group['group_name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="custom-form">
                <label>Guest Number <i class="fa fa-user-o"></i></label>
                <input name="guest_number" type="text" placeholder="" value="<?=isset($reservation) ? $reservation['guest_number'] : ''?>" required/>
                <label>Reservation Date <i class="fa fa-calendar-check-o"></i></label>
                <input name="res_date" type="text" placeholder="" value="<?=isset($reservation) ? $reservation['res_date'] : ''?>" required/>
                <label>Start Time<i class="fa fa-clock-o"></i>  </label>
                <input name="start_time" type="text" placeholder="" value="<?=isset($reservation) ? $reservation['start_time'] : ''?>" required/>
                <label>End Time<i class="fa fa-clock-o"></i>  </label>
                <input name="end_time" type="text" placeholder="" value="<?=isset($reservation) ? $reservation['end_time'] : ''?>"/>
                <button class="btn  big-btn  color-bg flat-btn">Save Changes<i class="fa fa-angle-right"></i></button>
            </div>
        </form>
    </div>
    <!-- profile-edit-container end-->   
</div>