<script>
$(document).ready(function() {
    $('#form-reservation').submit(function(e) {
        e.preventDefault();

        $.post({
            url: "<?=site_url('customer/reservation/ajax_post')?>",
            dataType: 'json',
            data: $(this).serializeArray(),
            method: 'POST',
            success: function(response) {
                if (response.status) {
                    window.location.href = "<?=site_url('customer/reservation')?>";
                } else {
                    alert(response.msg);
                }
            }
        });
    });
});
</script>