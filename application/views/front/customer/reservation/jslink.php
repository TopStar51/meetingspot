<script>
function set_reservation_page(cur_page) {
    $.post({
        url: '<?=site_url('customer/reservation/ajax_pagination')?>',
        data: {
            cur_page: cur_page
        },
        dataType: 'json',
        success: function(resp) {
            if (resp.status) {
                $('#div-reservations').html(resp.htmlData);
            }
        }
    });
}

$(document).ready(function() {
    set_reservation_page(0);
});
</script>