<div class="dashboard-list-box fl-wrap">
    <div class="dashboard-header fl-wrap">
        <h3>Reviews</h3>
    </div>
    <div class="reviews-comments-wrap" style="width: 100%;">
        <!-- reviews-comments-item -->  
        <?php foreach ($reviews as $review) { ?>
            <div class="reviews-comments-item">
            <div class="reviews-comments-item-text">
                <h4 style="padding-bottom: 10px"><a href="<?=site_url('location_single/index/'.$review['id'])?>" class="reviews-comments-item-link"><?=$review['location']?></a></h4>
                <div class="listing-rating card-popup-rainingvis">
                <?php for ($i = 0; $i < $review['rating']; $i += 1) { ?>
                    <i class="fa fa-star"></i>
                <?php } ?>
                </div>
                <div class="clearfix"></div>
                <h5 style="text-align: left; padding-bottom: 20px">(Reservation Number : <?=$review['res_number']?>)</h5>
                <p>"<?=$review['comment']?>"</p>
                <span class="reviews-comments-item-date"><i class="fa fa-calendar-check-o"></i><?=date_format(new DateTime($review['created_at']), 'Y-m-d')?></span>
            </div>
        </div>
        <?php } ?>
        <!--reviews-comments-item end-->  
    </div>
</div>
<!-- pagination-->
<?=$links?>
