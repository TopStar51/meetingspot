<script>
function set_review_page(cur_page) {
    $.post({
        url: '<?=site_url('customer/review/ajax_pagination')?>',
        data: {
            cur_page: cur_page
        },
        dataType: 'json',
        success: function(resp) {
            if (resp.status) {
                $('#div-reviews').html(resp.htmlData);
            }
        }
    });
}

$(document).ready(function() {
    set_review_page(0);
});
</script>