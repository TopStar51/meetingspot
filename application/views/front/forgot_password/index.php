<style type="text/css">
	section > div.container {
		width: 25%;
		min-height: 25vh;
	}

	.custom-form input[type="text"], .custom-form input[type="password"] {
        margin-top: 20px;
		padding: 15px;
	}

	.custom-form label{
		font-size: 14px;
    	margin-bottom: 15px;
	}

    .container > p {
        color: green;
        font-size: 16px;
        background: lightblue;
        padding: 10px;
    }

    span.text-danger {
        color: red;
        float: left;
    }

</style>
<div id="wrapper">
    <!--content-->  
    <div class="content">
    	<section>
    		<div class="container">
                <?php if($this->session->flashdata('msg')) { ?>
                    <p><?=$this->session->flashdata('msg')?></p>
                <?php } ?>
    			<div class="custom-form" id="div_reset_form">
                    <?php echo form_open('forgot_password'); ?>
                        <input name="email" type="text" placeholder="Email Address" value="<?php echo set_value('email');?>">
                        <button type="submit"  class="log-submit-btn pull-right">Submit</button>
                        <a href="<?=site_url('signin')?>" class="log-submit-btn" style="background: #fb5050" id="btn_back">Back to Login</a>
                        <div class="clearfix"></div>
                    <?php echo form_close(); ?>
                </div>
    		</div>
    	</section>
    </div>
</div>