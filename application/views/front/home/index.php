<div id="wrapper">
    <!-- Content-->
    <div class="content">
        <!--section -->
        <?php if($header_data['show_cover'] == 'on') { ?>
        <section class="scroll-con-sec hero-section" data-scrollax-parent="true" id="sec1">
            <div class="bg"  data-bg="<?=base_url('upload/front/header/').$header_data['cover_data']?>" data-scrollax="properties: { translateY: '200px' }"></div>
            <div class="overlay"></div>
            <div class="hero-section-wrap fl-wrap">
                <div class="container">
                    <?php if($header_data['show_title'] == 'on') { ?>
                    <div class="intro-item fl-wrap">
                        <h2><?=$header_data['title']?></h2>
                        <h3>Find nearby locations or click view all button below for the reservation.</h3>
                    </div>
                    <?php } ?>
                    <div class="main-search-input-wrap">
                        <div class="main-search-input fl-wrap">
                            <form id="search_form" action="<?=site_url('location_search')?>" method="get">
                                <div class="main-search-input-item">
                                    <input type="text" placeholder="What are you looking for?" id="location_name" name="location_name"/>
                                </div>
                                <div class="main-search-input-item location" id="autocomplete-container">
                                    <input type="text" name="address" placeholder="Enter the city name or zip code" id="autocomplete-input" />
                                    <a href="#"><i class="fa fa-dot-circle-o"></i></a>
                                </div>
                                <div class="main-search-input-item">
                                    <select data-placeholder="All Categories" class="chosen-select" name="category_id">
                                        <option value="">All Categories</option>
                                        <?php 
                                        foreach($categories as $category) {
                                            echo '<option value="'.$category['id'].'">'.$category['name'].'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <input hidden type="text" id="locality" name="city" value="">
                                <input hidden type="text" id="administrative_area_level_1" name="state" value="">
                                <input hidden type="text" id="postal_code" name="zip" value="">
                                <button type="submit" class="main-search-button">Search</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bubble-bg"> </div>
            <div class="header-sec-link">
                <div class="container"><a href="#sec2" class="custom-scroll-link">Let's Start</a></div>
            </div>
        </section>
        <?php } ?>
        <!-- section end -->
        <!--section -->
        <section id="sec2">
            <div class="container">
                <div class="section-title">
                    <h2>Top Rated Locations</h2>
                    <span class="section-separator"></span>
                    <p>Explore some of the best locations from our partners and friends.</p>
                </div>
                <!-- portfolio start -->
                <div class="gallery-items fl-wrap mr-bot spad">
                    <?php
                    foreach($top_locations as $location) { ?>
                    <!-- gallery-item-->
                    <div class="gallery-item">
                        <div class="grid-item-holder">
                            <div class="listing-item-grid">
                                <img src="<?=base_url('upload/location/').$location['thumb_image'];?>" style="width: 100%; height: 28vh" alt="">
                                <!-- <div class="listing-counter"><span>21 </span> Locations</div> -->
                                <div class="listing-counter"><?=$location['category']?></div>
                                <div class="listing-item-cat">
                                    <h3><a href="<?=site_url('location_single/index/').$location['id']?>"><?=$location['name']?></a></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- gallery-item end-->
                    <?php } ?>
                </div>
                <!-- portfolio end -->
                <a href="<?=site_url('location_search')?>" class="btn  big-btn circle-btn dec-btn  color-bg flat-btn">View All<i class="fa fa-eye"></i></a>
            </div>
        </section>
        <!-- section end -->
        <!--section -->
        <?php if(FALSE) { ?>
        <section class="gray-section">
            <div class="container">
                <div class="section-title">
                    <h2>Popular Locations</h2>
                    <span class="section-separator"></span>
                </div>
            </div>
            <!-- carousel -->
            <div class="list-carousel fl-wrap card-listing ">
                <!--listing-carousel-->
                <div class="listing-carousel  fl-wrap ">
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <img src="<?=base_url('assets/front/images/all/2.jpg')?>" alt="">
                                    <div class="overlay"></div>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    <a class="listing-geodir-category" href="listing.html">Tea Room</a>
                                    <!-- <div class="listing-avatar"><a href="author-single.html"><img src="images/avatar/1.jpg" alt=""></a>
                                        <span class="avatar-tooltip">Added By  <strong>Lisa Smith</strong></span>
                                    </div> -->
                                    <h3><a href="listing-single.html">Event in City Mol</a></h3>
                                    <p>Sed interdum metus at nisi tempor laoreet.  </p>
                                    <div class="geodir-category-options fl-wrap">
                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5">
                                            <span>(7 reviews)</span>
                                        </div>
                                        <div class="geodir-category-location"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> 27th Brooklyn New York, NY 10065</a></div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <img src="<?=base_url('assets/front/images/all/2.jpg')?>" alt="">
                                    <div class="overlay"></div>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    <a class="listing-geodir-category" href="listing.html">Cafe</a>
                                    <!-- <div class="listing-avatar"><a href="author-single.html"><img src="images/avatar/1.jpg" alt=""></a>
                                        <span class="avatar-tooltip">Added By  <strong>Mark Rose</strong></span>
                                    </div> -->
                                    <h3><a href="listing-single.html">Cafe "Lollipop"</a></h3>
                                    <p>Morbi suscipit erat in diam bibendum rutrum in nisl.</p>
                                    <div class="geodir-category-options fl-wrap">
                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="4">
                                            <span>(17 reviews)</span>
                                        </div>
                                        <div class="geodir-category-location"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> 27th Brooklyn New York, NY 10065</a></div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <img src="<?=base_url('assets/front/images/all/2.jpg')?>" alt="">
                                    <div class="overlay"></div>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    <a class="listing-geodir-category" href="listing.html">Restaurant </a>
                                    <!-- <div class="listing-avatar"><a href="author-single.html"><img src="images/avatar/1.jpg" alt=""></a>
                                        <span class="avatar-tooltip">Added By  <strong>Nasty Wood</strong></span>
                                    </div> -->
                                    <h3><a href="listing-single.html">Gym In Brooklyn</a></h3>
                                    <p>Morbiaccumsan ipsum velit tincidunt . </p>
                                    <div class="geodir-category-options fl-wrap">
                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="3">
                                            <span>(16 reviews)</span>
                                        </div>
                                        <div class="geodir-category-location"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> 27th Brooklyn New York, NY 10065</a></div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <img src="<?=base_url('assets/front/images/all/2.jpg')?>" alt="">
                                    <div class="overlay"></div>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    <a class="listing-geodir-category" href="listing.html">Cafe</a>
                                    <!-- <div class="listing-avatar"><a href="author-single.html"><img src="images/avatar/1.jpg" alt=""></a>
                                        <span class="avatar-tooltip">Added By  <strong>Nasty Wood</strong></span>
                                    </div> -->
                                    <h3><a href="listing-single.html">Shop in Boutique Zone</a></h3>
                                    <p>Morbiaccumsan ipsum velit tincidunt . </p>
                                    <div class="geodir-category-options fl-wrap">
                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="4">
                                            <span>(6 reviews)</span>
                                        </div>
                                        <div class="geodir-category-location"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> 27th Brooklyn New York, NY 10065</a></div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <img src="<?=base_url('assets/front/images/all/2.jpg')?>" alt="">
                                    <div class="overlay"></div>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    <a class="listing-geodir-category" href="listing.html">Restaurant</a>
                                    <!-- <div class="listing-avatar"><a href="author-single.html"><img src="<?=base_url('assets/front/images/avatar/avatar-bg.png')?>" alt=""></a>
                                        <span class="avatar-tooltip">Added By  <strong>Kliff Antony</strong></span>
                                    </div> -->
                                    <h3><a href="listing-single.html">Best deal For the Cars</a></h3>
                                    <p>Lorem ipsum gravida nibh vel velit.</p>
                                    <div class="geodir-category-options fl-wrap">
                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5">
                                            <span>(11 reviews)</span>
                                        </div>
                                        <div class="geodir-category-location"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> 27th Brooklyn New York, NY 10065</a></div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <img src="<?=base_url('assets/front/images/all/2.jpg')?>" alt="">
                                    <div class="overlay"></div>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    <a class="listing-geodir-category" href="listing.html">Cafe</a>
                                    <!-- <div class="listing-avatar"><a href="author-single.html"><img src="images/avatar/1.jpg" alt=""></a>
                                        <span class="avatar-tooltip">Added By  <strong>Adam Koncy</strong></span>
                                    </div> -->
                                    <h3><a href="listing-single.html">Luxury Restourant</a></h3>
                                    <p>Sed non neque elit. Sed ut imperdie.</p>
                                    <div class="geodir-category-options fl-wrap">
                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5">
                                            <span>(7 reviews)</span>
                                        </div>
                                        <div class="geodir-category-location"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> 27th Brooklyn New York, NY 10065</a></div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->
                </div>
                <!--listing-carousel end-->
                <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
            </div>
            <!--  carousel end-->
        </section>
        <!-- section end -->
        <?php } ?>
        <!--section -->
        <section>
            <div class="container">
                <div class="section-title">
                    <h2>How it works</h2>
                    <span class="section-separator"></span>
                </div>
                <!--process-wrap  -->
                <div class="process-wrap fl-wrap">
                    <ul>
                        <li>
                            <div class="process-item">
                                <span class="process-count">01 . </span>
                                <div class="time-line-icon"><i class="fa fa-map-o"></i></div>
                                <h4> Find Interesting Place</h4>
                                <p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus, finibus tellus ut, convallis eros sollicitudin turpis.</p>
                            </div>
                            <span class="pr-dec"></span>
                        </li>
                        <li>
                            <div class="process-item">
                                <span class="process-count">02 .</span>
                                <div class="time-line-icon"><i class="fa fa-envelope-open-o"></i></div>
                                <h4> Contact a Few Owners</h4>
                                <p>Faucibus ante, in porttitor tellus blandit et. Phasellus tincidunt metus lectus sollicitudin feugiat pharetra consectetur.</p>
                            </div>
                            <span class="pr-dec"></span>
                        </li>
                        <li>
                            <div class="process-item">
                                <span class="process-count">03 .</span>
                                <div class="time-line-icon"><i class="fa fa-hand-peace-o"></i></div>
                                <h4> Create a Location</h4>
                                <p>Maecenas pulvinar, risus in facilisis dignissim, quam nisi hendrerit nulla, id vestibulum metus nullam viverra porta.</p>
                            </div>
                        </li>
                    </ul>
                    <div class="process-end"><i class="fa fa-check"></i></div>
                </div>
                <!--process-wrap   end-->
            </div>
        </section>
        <!-- section end -->
        <!--section -->
        <!-- <section class="color-bg">
            <div class="shapes-bg-big"></div>
            <div class="container">
                <div class=" single-facts fl-wrap">
                    <div class="inline-facts-wrap">
                        <div class="inline-facts">
                            <div class="milestone-counter">
                                <div class="stats animaper">
                                    <div class="num" data-content="0" data-num="254">154</div>
                                </div>
                            </div>
                            <h6>New Visiters Every Week</h6>
                        </div>
                    </div>
                    <div class="inline-facts-wrap">
                        <div class="inline-facts">
                            <div class="milestone-counter">
                                <div class="stats animaper">
                                    <div class="num" data-content="0" data-num="12168">12168</div>
                                </div>
                            </div>
                            <h6>Happy customers every year</h6>
                        </div>
                    </div>
                    <div class="inline-facts-wrap">
                        <div class="inline-facts">
                            <div class="milestone-counter">
                                <div class="stats animaper">
                                    <div class="num" data-content="0" data-num="172">172</div>
                                </div>
                            </div>
                            <h6>Won Awards</h6>
                        </div>
                    </div>
                    <div class="inline-facts-wrap">
                        <div class="inline-facts">
                            <div class="milestone-counter">
                                <div class="stats animaper">
                                    <div class="num" data-content="0" data-num="732">732</div>
                                </div>
                            </div>
                            <h6>New Listing Every Week</h6>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- section end -->
        <!--section -->
        <section>
            <div class="container">
                <div class="section-title">
                    <h2>Recent Reviews</h2>
                    <!-- <div class="section-subtitle">Clients Reviews</div> -->
                    <span class="section-separator"></span>
                </div>
            </div>
            <!-- testimonials-carousel -->
            <div class="carousel fl-wrap">
                <!--testimonials-carousel-->
                <div class="testimonials-carousel single-carousel fl-wrap">
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi arch itecto beatae vitae dicta sunt explicabo. </p>
                        </div>
                        <div class="testimonilas-avatar-item"><!-- 
                            <div class="testimonilas-avatar"><img src="<?=base_url('assets/front')?>/images/avatar/avatar-bg.png" alt=""></div> -->
                            <h4>Lisa Noory</h4>
                            <!-- <span>Restaurant Owner</span> -->
                        </div>
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="4"> </div>
                            <p>Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                        </div>
                        <div class="testimonilas-avatar-item">
                            <!-- <div class="testimonilas-avatar"><img src="<?=base_url('assets/front')?>/images/avatar/avatar-bg.png" alt=""></div> -->
                            <h4>Antony Moore</h4>
                            <!-- <span>Restaurant Owner</span> -->
                        </div>
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                            <p>Feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te odio dignissim qui blandit praesent.</p>
                        </div>
                        <div class="testimonilas-avatar-item">
                            <!-- <div class="testimonilas-avatar"><img src="<?=base_url('assets/front')?>/images/avatar/avatar-bg.png" alt=""></div> -->
                            <h4>Austin Harisson</h4>
                            <!-- <span>Restaurant Owner</span> -->
                        </div>
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="4"> </div>
                            <p>Qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram seacula quarta decima et quinta decima.</p>
                        </div>
                        <div class="testimonilas-avatar-item">
                            <!-- <div class="testimonilas-avatar"><img src="<?=base_url('assets/front')?>/images/avatar/avatar-bg.png" alt=""></div> -->
                            <h4>Garry Colonsi</h4>
                            <!-- <span>Restaurant Owner</span> -->
                        </div>
                    </div>
                    <!--slick-slide-item end-->
                </div>
                <!--testimonials-carousel end-->
                <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
            </div>
            <!-- carousel end-->
        </section>
        <!-- section end -->
        <!--section -->
        <section class="gradient-bg">
            <div class="cirle-bg">
                <div class="bg" data-bg="<?=base_url('assets/front')?>/images/bg/circle.png"></div>
            </div>
            <div class="container">
                <div class="join-wrap fl-wrap">
                    <div class="row">
                        <div class="col-md-8">
                            <h3>Do You Have Questions ?</h3>
                            <p>Lorem ipsum dolor sit amet, harum dolor nec in, usu molestiae at no.</p>
                        </div>
                        <div class="col-md-4"><a href="<?=site_url('contacts')?>" class="join-wrap-btn">Get In Touch <i class="fa fa-envelope-o"></i></a></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end -->
    </div>
    <!-- Content end -->
</div>