<script type="text/javascript">
	document.getElementById('locality').value = '';
	document.getElementById('administrative_area_level_1').value = '';
	document.getElementById('postal_code').value = '';
	
	var componentForm = {
	    locality: 'long_name',
	    administrative_area_level_1: 'short_name',
	    postal_code: 'short_name'
	};

    function initialize() {

        var options = {
          	types: ['(regions)'],
          	componentRestrictions: {country: "us"}
        };

        var input = document.getElementById('autocomplete-input');
        google.maps.event.addDomListener(input, 'keydown', function(event) { 
		    if (event.keyCode === 13) { 
		        event.preventDefault(); 
		    }
	  	}); 
        var autocomplete = new google.maps.places.Autocomplete(input, options);
	    autocomplete.addListener('place_changed', function() {
	        var place = autocomplete.getPlace();
	        if (!place.geometry) {
	            window.alert("No details available for input: '" + place.name + "'");
	            return;
	        }
	        for (var component in componentForm) {
	            document.getElementById(component).value = '';
	        }
	        for (var i = 0; i < place.address_components.length; i++) {
	            var addressType = place.address_components[i].types[0];
	            if (componentForm[addressType]) {

	                var val = place.address_components[i][componentForm[addressType]];
	                document.getElementById(addressType).value = val;
	            }
	        }
	    });		
	    
    }
    google.maps.event.addDomListener(window, 'load', initialize);

</script>