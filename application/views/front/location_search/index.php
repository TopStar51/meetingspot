<div id="wrapper">
    <div class="content">
        <!-- Map -->
        <div class="map-container column-map right-pos-map">
            <div id="map-main"></div>
            <ul class="mapnavigation">
                <li><a href="#" class="prevmap-nav">Prev</a></li>
                <li><a href="#" class="nextmap-nav">Next</a></li>
            </ul>
    		<div class="scrollContorl mapnavbtn" title="Enable Scrolling"><span><i class="fa fa-lock"></i></span></div>   						
        </div>
        <!-- Map end -->          
        <!--col-list-wrap -->   
        <div class="col-list-wrap left-list">
            <div class="listsearch-options fl-wrap" id="lisfw" >
                <div class="container">
                    <div class="listsearch-header fl-wrap">
                        <h3>Searched Locations: </h3>
                    </div>
                    <!-- listsearch-input-wrap  -->  
                    <!-- <div class="listsearch-input-wrap fl-wrap">
                        <div class="listsearch-input-item">
                            <i class="mbri-key single-i"></i>
                            <input type="text" placeholder="Keywords?" value=""/>
                        </div>
                        <div class="listsearch-input-item">
                            <select data-placeholder="Location" class="chosen-select" >
                                <option>All Locations</option>
                                <option>Bronx</option>
                                <option>Brooklyn</option>
                                <option>Manhattan</option>
                                <option>Queens</option>
                                <option>Staten Island</option>
                            </select>
                        </div>
                        <div class="listsearch-input-item">
                            <select data-placeholder="All Categories" class="chosen-select" >
                                <option>All Categories</option>
                                <option>Cafe</option>
                                <option>Tea Room</option>
                                <option>Restaurants</option>
                            </select>
                        </div>
                        <div class="listsearch-input-text" id="autocomplete-container">
                            <label><i class="mbri-map-pin"></i> Enter Addres </label>
                            <input type="text" placeholder="Destination , Area , Street" id="autocomplete-input" class="qodef-archive-places-search" value=""/>
                            <a  href="#"  class="loc-act qodef-archive-current-location"><i class="fa fa-dot-circle-o"></i></a>
                        </div>
                        
                        <div class="hidden-listing-filter fl-wrap">
                            <div class="distance-input fl-wrap">
                                <div class="distance-title"> Radius around selected destination <span></span> km</div>
                                <div class="distance-radius-wrap fl-wrap">
                                    <input class="distance-radius rangeslider--horizontal" type="range" min="1" max="100" step="1" value="1" data-title="Radius around selected destination">
                                </div>
                            </div>
                            <div class=" fl-wrap filter-tags">
                                <h4>Filter by Tags</h4>
                                <input id="check-aa" type="checkbox" name="check">
                                <label for="check-aa">Elevator in building</label>
                                <input id="check-b" type="checkbox" name="check">
                                <label for="check-b">Friendly workspace</label>
                                <input id="check-c" type="checkbox" name="check">
                                <label for="check-c">Instant Book</label>
                                <input id="check-d" type="checkbox" name="check">
                                <label for="check-d">Wireless Internet</label>
                            </div>
                        </div>
                        
                        <button class="button fs-map-btn">Update</button>
                        <div class="more-filter-option">More Filters <span></span></div>
                    </div> -->
                    <!-- listsearch-input-wrap end -->
                </div>
            </div>
            <!-- list-main-wrap-->
            <div class="list-main-wrap fl-wrap card-listing">
                <!-- <a class="custom-scroll-link back-to-filters btf-l" href="#lisfw"><i class="fa fa-angle-double-up"></i><span>Back to Filters</span></a> --> 
                <div class="container">
                    <?php 
                    if(!empty($locations)) {
                        foreach ($locations as $key => $location) {
                    ?> 
                    <!-- listing-item -->
                    <div class="listing-item list-layout">
                        <article class="geodir-category-listing fl-wrap">
                            <div class="geodir-category-img">
                                <img src="<?=base_url('upload/location/').$location['thumb_image'] ?>" alt="" style="width: 435px; height: 295px">
                                <div class="overlay"></div>
                            </div>
                            <div class="geodir-category-content fl-wrap">
                                <a class="listing-geodir-category" href="#"><?=$location['category']?></a>
                                <!-- <div class="listing-avatar"><a href="author-single.html"><img src="<?=base_url('assets/front/')?>images/avatar/avatar-bg.png" alt=""></a>
                                    <span class="avatar-tooltip">Added By  <strong>Lisa Smith</strong></span>
                                </div> -->
                                <h3><a href="<?=site_url('location_single/index/').$location["id"]?>"><?=$location['name'];?></a></h3>
                                <p><?=$location['description']?></p>
                                <div class="geodir-category-options fl-wrap">
                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="<?=$location['review_avg']?>">
                                        <?php if(empty($location['review_count'])) { ?>
                                            <span>(No reviews)</span>
                                        <?php } else { ?>
                                            <span>(<?=$location['review_count']?> reviews)</span>
                                        <?php } ?>
                                    </div>
                                    <div class="geodir-category-location"><a  href="#0" class="map-item"><i class="fa fa-map-marker" aria-hidden="true"></i> <?=$location['address']?></a></div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <!-- listing-item end-->   
                    <?php } } else { ?>
                        <span> No Results Found. </span>
                    <?php } ?>                        
                </div>
                <!-- <a class="load-more-button" href="#">Load more <i class="fa fa-circle-o-notch"></i> </a>  --> 
            </div>
            <!-- list-main-wrap end-->
        </div>
        <!--col-list-wrap -->  
        <div class="limit-box fl-wrap"></div>
        <!--section -->  
        <section class="gradient-bg">
            <div class="cirle-bg">
                <div class="bg" data-bg="<?=base_url('assets/front/')?>images/bg/circle.png"></div>
            </div>
            <div class="container">
                <div class="join-wrap fl-wrap">
                    <div class="row">
                        <div class="col-md-8">
                            <h3>Join our online community</h3>
                            <p>Grow your marketing and be happy with your online business</p>
                        </div>
                        <div class="col-md-4"><a href="#" class="join-wrap-btn modal-open">Sign Up <i class="fa fa-sign-in"></i></a></div>
                    </div>
                </div>
            </div>
        </section>
        <!--section end -->  
    </div>
</div>