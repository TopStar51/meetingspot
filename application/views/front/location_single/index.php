<style>
    span.count{
        display: block;
        margin: 12px 0;
        font-size: 30px;
        font-weight: 800;
    }

    .filter-tags label, .custom-form .filter-tags label {
        color: black;
    }

    a {
        color: #888DA0;
    }

    .listing-features li {
        color: black;
    }

    .accordion a.toggle{
        color: black;
        font-size: 14px;
        font-weight: bold;
    }

    .opening-hours ul li span.opening-hours-day {
        color: black;
    }

    form.book-reservation .chosen-select {
        background: #f9f9f9;
        color: #666;
    }

    .scroll-nav-wrapper .scroll-nav li a {
        color: black;
        font-size: 14px;
    }

    form.book-reservation .chosen-select {
        color: black;
        font-weight: bold;
    }
    #group_res, #person_res {
        z-index: 8;
    }
</style>
<!-- wrapper -->
<div id="wrapper">
    <!--  content--> 
    <div class="content">
        <!--  carousel--> 
        <div class="list-single-carousel-wrap fl-wrap" id="Gallery">
            <div class="fw-carousel fl-wrap full-height lightgallery">
                <?php
                $gallery = json_decode($location_info['gallery']);
                foreach ($gallery as $image) { ?>
                    <!-- slick-slide-item -->
                    <div class="slick-slide-item">
                        <div class="box-item">
                            <img src="<?=base_url('upload/location/').$image?>"   alt="">
                            <a href="<?=base_url('upload/location/').$image?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                        </div>
                    </div>
                    <!-- slick-slide-item end -->
                <?php 
                }
                ?>
            </div>
            <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
            <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
        </div>
        <!--  carousel  end--> 
        <div class="scroll-nav-wrapper fl-wrap">
            <div class="container">
                <nav class="scroll-nav scroll-init">
                    <ul>
                        <li><a class="act-scrlink" href="#Gallery">Gallery</a></li>
                        <li><a href="#detail">Details</a></li>
                        <li><a href="#video">Video </a></li>
                        <li><a href="#room">Rooms</a></li>
                        <li><a href="#service">Services</a></li>
                        <li><a href="#review">Reviews</a></li>
                        <li><a href="#reservation">Reservation</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <!--  section   --> 
        <section class="gray-section no-top-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <!-- list-single-main-wrapper -->
                        <div class="list-single-main-wrapper fl-wrap" id="detail">
                            <div class="breadcrumbs gradient-bg  fl-wrap"><a href="#">Home</a><a href="#">Locations</a><span>Location Single</span></div>
                            <!-- list-single-header -->
                            <div class="list-single-header list-single-header-inside fl-wrap">
                                <div class="container">
                                    <div class="list-single-header-item">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="list-single-header-item-opt fl-wrap">
                                                    <div class="list-single-header-cat fl-wrap">
                                                        <a href="javascript:;"><?=$location_info['category']?></a>
                                                    </div>
                                                </div>
                                                <h2><?=$location_info['name']?> <span> - Owned By </span><a href="javascript:;"><?=$location_info['owner']?></a> </h2>
                                                <span class="section-separator"></span>
                                                <div class="listing-rating card-popup-rainingvis" data-starrating2="<?=$review_avg?>">
                                                    <span>(<?=$review_count?> reviews)</span>
                                                </div>
                                                <!-- <div class="list-post-counter single-list-post-counter"><span>4</span><i class="fa fa-heart"></i></div> -->
                                            </div>
                                            <div class="col-md-3">
                                                <div class="fl-wrap list-single-header-column">
                                                    <!-- <span class="viewed-counter"><i class="fa fa-eye"></i> Viewed -  156 </span> -->
                                                    <!-- <a class="custom-scroll-link" href="#service"><i class="fa fa-hand-o-right"></i>Add Review </a> -->
                                                    <!-- <div class="share-holder hid-share">
                                                        <div class="showshare"><span>Share </span><i class="fa fa-share"></i></div>
                                                        <div class="share-container  isShare"></div>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- list-single-header end -->
                            <!-- <div class="list-single-facts fl-wrap gradient-bg">
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <i class="fa fa-home"></i>
                                        <span class="room count"><?=$room_count?></span>
                                        <h6>Rooms</h6>
                                    </div>
                                </div>
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <i class="fa fa-male"></i>
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="5">0</div>
                                            </div>
                                        </div>
                                        <h6>Capacity</h6>
                                    </div>
                                </div>
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <i class="fa fa-cutlery"></i>
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="5">0</div>
                                            </div>
                                        </div>
                                        <h6>Happy meetings every week</h6>
                                    </div>
                                </div>                        
                            </div> -->
                            <div class="list-single-main-item fl-wrap">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>About</h3>
                                </div>
                                <p><?=$location_info['description']?>.</p>
                                <!-- <a href="#reservation" class="btn transparent-btn float-btn">Book Now <i class="fa fa-angle-right"></i></a> -->
                            </div>
                            <!-- list-single-main-item-->   
                            <div class="list-single-main-item fl-wrap" id="video">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Promo Video</h3>
                                </div>
                                <div class="iframe-holder fl-wrap">
                                    <div class="resp-video">
                                        <!-- <iframe src="https://player.vimeo.com/video/161566855" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
                                        <video width="745" height="450" controls>
                                            <source src="<?=base_url('upload/location/').$location_info['video']?>">
                                        </video>
                                    </div>
                                </div>
                            </div>
                            <!-- list-single-main-item end -->   
                            <div class="list-single-main-item-title fl-wrap row" id="room">
                                <h3>Rooms</h3>
                            </div>                         
                            <div class="accordion">
                                <?php 
                                $wall_types = array('Cement', 'Drywall', 'Wood', 'Divider');
                                $table_formats = array('Circle', 'Rectangular');
                                $table_types = array('Dining', 'Conference');
                                $food_services = array('At the ordering counter', 'Buffet');
                                if(!empty($rooms)) {
                                foreach($rooms as $key=>$room) { ?>
                                    <a class="toggle <?php if($key==0) echo 'act-accordion'?>" href="#"><?=$room['name']?><i class="fa fa-angle-down"></i></a>
                     
                                    <div class="accordion-inner <?php if($key==0) echo 'visible'?>">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <!-- <a class="btn transparent-btn" href="#reservation" style="float: right; padding: 10px 15px"><i class="fa fa-hand-o-right"></i> Book Now </a> -->
                                            <h3>Physical Aspects</h3>
                                        </div>
                                        <div class="listing-features fl-wrap filter-tags">
                                            <ul>
                                                <li>
                                                    <input id="has_window" type="checkbox" disabled <?php if($room['has_window']) echo "checked";?>>
                                                    <label for="has_window">Window</label></li>
                                                <li>
                                                    <input id="seclude_door" type="checkbox" disabled <?php if($room['has_seclude_door']) echo "checked";?>>
                                                    <label for="seclude_door">Door</label></li>
                                                <li>
                                                    <input id="lighting_option" type="checkbox" disabled <?php if($room['has_lighting_option']) echo "checked";?>>
                                                    <label for="lighting_option">Lighting Option</label></li>
                                                <li>
                                                    <input id="cloth_on_table" type="checkbox" disabled <?php if($room['cloth_on_table']) echo "checked";?>>
                                                    <label for="cloth_on_table">Cloth on tables</label></li>
                                            </ul>
                                        </div>
                                        <div class="listing-features fl-wrap filter-tags">
                                            <ul>
                                                <li><span> Capacity :</span> <a href="#"><?=$room['capacity']?></a></li>
                                                <li><span> Wall Type :</span> <a href="#"><?=$wall_types[$room['wall_type']]?></a></li>

                                                <li><span> Table Format :</span> <a href="#"><?=$table_formats[$room['table_format']]?></a></li>
                                                
                                                <li><span> Table Count :</span> <a href="#"><?=$room['table_count']?></a></li>
                                                <li><span> Table Length(m) :</span> <a href="#"><?=$room['table_length']?></a></li>
                                                <li><span> Table Width(m) :</span> <a href="#"><?=$room['table_width']?></a></li>
                                                <li><span> Table Type :</span> <a href="#"><?=$table_types[$room['table_type']]?></a></li>
                                                <li><span> Chairs Per Table:</span> <a href="#"><?=$room['chairs_per_table']?></a></li>
                                            </ul>
                                        </div>
                                        <span class="fw-separator"></span>
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Electronics</h3>
                                        </div>
                                        <div class="listing-features fl-wrap filter-tags">
                                            <ul>
                                                <li>
                                                    <input id="projection" type="checkbox" disabled <?php if($room['projection_provided']) echo "checked";?>>
                                                    <label for="projection">Projection</label></li>
                                                <li>
                                                    <input id="wifi" type="checkbox" disabled <?php if($room['wifi_access']) echo "checked";?>>
                                                    <label for="wifi">Free Wi Fi</label></li>
                                                <li>
                                                    <input id="podium" type="checkbox" disabled <?php if($room['podium_provided']) echo "checked";?>>
                                                    <label for="podium">Podium</label></li>
                                                <li>
                                                    <input id="speaker" type="checkbox" disabled <?php if($room['has_speaker']) echo "checked";?>>
                                                    <label for="speaker">Speaker</label></li>
                                                <li>
                                                    <input id="microphone" type="checkbox" disabled <?php if($room['has_microphone']) echo "checked";?>>
                                                    <label for="microphone">Microphone</label></li>
                                            </ul>
                                        </div>
                                        <div class="listing-features fl-wrap filter-tags">
                                            <ul>
                                                <li><span> Upload Speed(Mbps) :</span> <a href="#"><?=$room['upload_speed']?></a></li>
                                                <li><span> Download Speed(Mbps) :</span> <a href="#"><?=$room['download_speed']?></a></li>
                                                <li><span> Electric Outlets :</span> <a href="#"><?=$room['electric_outlet_count']?></a></li>
                                                <li><span> Outlet Extensions :</span> <a href="#"><?=$room['outlet_extensions']?></a></li>
                                                <li><span> Food Served :</span> <a href="#"><?=$food_services[$room['food_served']]?></a></li>
                                            </ul>
                                        </div>
                                        <span class="fw-separator"></span>
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Reserved Time</h3>
                                        </div>
                                        <div class="list-single-tags tags-stylwrap">
                                            <?php
                                                $res_flag = false; 
                                                foreach($reservations as $reservation) { 
                                                    if($reservation['room_id'] == $room['id']) {
                                                        $res_flag = true;
                                                        echo '<a href="javascript:;" style="background:green; color: white">'.date('M d , g:i A', strtotime($reservation['start_time'])).' - '.date('g:i A', strtotime($reservation['end_time'])).'</a>';
                                                    }
                                                }
                                                if(!$res_flag)
                                                    echo '<a href="javascript:;" style="background:#ef4646; color: white">No Available Reservation</a>';
                                            ?>                            
                                        </div>
                                    </div>
                                <?php }
                                } ?>
                                <!-- <a class="toggle act-accordion" href="#"> Details option   <i class="fa fa-angle-down"></i></a>
                                <div class="accordion-inner visible">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                </div> -->
                            </div>
                            <!-- list-single-main-item --> 
                            <div class="list-single-main-item fl-wrap" id="service">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Beverage Service</h3>
                                    <div class="fl-wrap filter-tags row">
                                        <div class="col-md-3">
                                            <input id="water_included" type="checkbox" disabled <?php if($location_info['water']) echo "checked";?>>
                                            <label for="water_included">Water</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input id="soft_drink" type="checkbox" disabled <?php if($location_info['soft_drink']) echo "checked";?>>
                                            <label for="soft_drink">Soft Drink</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input id="alcohol_drink" type="checkbox" disabled <?php if($location_info['alcoholic_drink']) echo "checked";?>>
                                            <label for="alcohol_drink">Alcoholic Drink</label>
                                        </div>
                                    </div>
                                </div>

                                <?php if(!empty($location_info['mandatory_order'])) { ?>
                                <span class="fw-separator"></span>                        
                                <div class="list-single-main-item-title fl-wrap">
                                    <h4 style="text-align: left; font-weight: bold">Mandatory minimum order per person :</h4>
                                </div>
                                <div class="list-single-tags tags-stylwrap">
                                    <a href="javascript:;" style="color: black">$<?=$location_info['mandatory_order'];?></a>      
                                </div>
                                <?php } ?> 
                            </div>
                            <?php if(!empty($reviews)) { ?> 
                            <div class="list-single-main-item fl-wrap" id="review">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3> Recent Reviews </h3>
                                </div>
                                <div class="reviews-comments-wrap">
                                    <?php foreach($reviews as $review) { ?>
                                    <!-- reviews-comments-item -->  
                                    <div class="reviews-comments-item" style="padding-left: 20px">
                                        <!-- <div class="review-comments-avatar">
                                            <img src="<?=base_url('assets/front/')?>images/avatar/avatar-bg.png" alt=""> 
                                        </div> -->
                                        <div class="reviews-comments-item-text">
                                            <h4><a href="#"><?=$review['customer']?></a></h4>
                                            <div class="listing-rating card-popup-rainingvis" data-starrating2="<?=$review['rating']?>"> </div>
                                            <div class="clearfix"></div>
                                            <p>"<?=$review['comment']?> "</p>
                                            <span class="reviews-comments-item-date"><i class="fa fa-calendar-check-o"></i><?=date('d M Y', strtotime($review['created_at']))?></span>
                                        </div>
                                    </div>
                                    <!--reviews-comments-item end-->
                                    <?php } ?>                                                                  
                                </div>
                            </div>
                            <!-- list-single-main-item end -->   
                            <?php } ?>
                            <!-- list-single-main-item -->
                            <?php if(1==0) { ?>   
                            <div class="list-single-main-item fl-wrap" id="service">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Add Reviews  & Rate item</h3>
                                </div>
                                <!-- Add Review Box -->
                                <div id="add-review" class="add-review-box">
                                    <div class="leave-rating-wrap">
                                        <span class="leave-rating-title">Your rating  for this location : </span>
                                        <div class="leave-rating">
                                            <input type="radio" name="rating" id="rating-1" value="1"/>
                                            <label for="rating-1" class="fa fa-star-o"></label>
                                            <input type="radio" name="rating" id="rating-2" value="2"/>
                                            <label for="rating-2" class="fa fa-star-o"></label>
                                            <input type="radio" name="rating" id="rating-3" value="3"/>
                                            <label for="rating-3" class="fa fa-star-o"></label>
                                            <input type="radio" name="rating" id="rating-4" value="4"/>
                                            <label for="rating-4" class="fa fa-star-o"></label>
                                            <input type="radio" name="rating" id="rating-5" value="5"/>
                                            <label for="rating-5" class="fa fa-star-o"></label>
                                        </div>
                                    </div>
                                    <!-- Review Comment -->
                                    <form   class="add-comment custom-form">
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label><i class="fa fa-user-o"></i></label>
                                                    <input type="text" placeholder="Your Name *" value=""/>
                                                </div>
                                                <div class="col-md-6">
                                                    <label><i class="fa fa-envelope-o"></i>  </label>
                                                    <input type="text" placeholder="Email Address*" value=""/>
                                                </div>
                                            </div>
                                            <textarea cols="40" rows="3" placeholder="Your Review:"></textarea>
                                        </fieldset>
                                        <button class="btn  big-btn  color-bg flat-btn">Submit Review <i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                    </form>
                                </div>
                                <!-- Add Review Box / End -->
                            </div>
                            <?php } ?>
                            <!-- list-single-main-item end -->   
                            <div class="list-single-main-item fl-wrap" id="reservation">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Book a Reservation: </h3>
                                </div>                                
                                <div class="box-widget-content">
                                    <?php if(!empty($type) && $type==CUSTOMER) { ?>  
                                    <form class="book-reservation custom-form">
                                        <fieldset>
                                            <input name="location_id" value="<?=$location_info['id']?>" hidden />
                                            <select name="room_id" id="room_id" class="chosen-select">
                                                <option value="" data-capacity="0">Select a room</option>
                                                <?php foreach($rooms as $room) { ?>
                                                <option value="<?=$room['id']?>" data-capacity="<?=$room['capacity']?>"><?=$room['name']?></option>
                                                <?php } ?>
                                            </select>
                                            <div class="fl-wrap filter-tags row" style="margin-bottom: 10px">
                                                <div class="col-md-4">
                                                    <label>Reservation with group?</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input id="group_res" type="radio" name="is_group" value="1" checked="">
                                                    <label for="group_res">Yes</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input id="person_res" type="radio" name="is_group" value="0">
                                                    <label for="person_res">No</label>
                                                </div>
                                            </div>
                                            <div class="fl-wrap" id="existent_group" style="<?php if(empty($groups)) echo 'display:none'?>">
                                                <select name="group_id" id="group_id" class="chosen-select">
                                                    <option value="">Select a group</option>
                                                    <?php foreach($groups as $group) { ?>
                                                    <option value="<?=$group['id']?>"><?=$group['group_name']?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="row" id="new_group" style="<?php if(!empty($groups)) echo 'display:none'?>">
                                                <div class="col-md-6">
                                                    <label><i class="fa fa-briefcase"></i>  </label>
                                                    <input type="text" name="group_name" id="group_name" placeholder="Group Name" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label><i class="fa fa-map-marker"></i>  </label>
                                                    <input type="text" name="address" id="address" placeholder="Address" />
                                                </div>
                                                <div class="col-md-6">      
                                                    <label><i class="fa fa-phone"></i>  </label>  
                                                    <input type="text" name="phone" id="phone" placeholder="Contact Number" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label><i class="fa fa-globe"></i>  </label>
                                                    <input type="text" name="website" id="website" placeholder="Website" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label><i class="fa fa-facebook"></i>  </label>
                                                    <input type="text" name="facebook" id="facebook" placeholder="Facebook" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label><i class="fa fa-user"></i>  </label>
                                                    <input type="text" name="min_age" id="min_age" placeholder="Minimum age of members" />
                                                </div>
                                                <div class="fl-wrap filter-tags" style="margin-bottom: 10px">
                                                    <div class="col-md-3">
                                                        <label>Nature of group: </label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input id="civic" type="radio" name="nature_of_group" value="civic" checked="">
                                                        <label for="civic">Civic</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input id="religious" type="radio" name="nature_of_group" value="religious">
                                                        <label for="religious">Religious</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input id="sports" type="radio" name="nature_of_group" value="sports">
                                                        <label for="sports">Sports</label>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="quantity fl-wrap">
                                                <span><i class="fa fa-user-plus"></i>Persons : </span>
                                                <div class="quantity-item">
                                                    <input type="button" value="-" class="minus">
                                                    <input type="text" name="guest_number"   title="Qty" class="qty" min="1" step="1" value="1" id="guest_number">
                                                    <input type="button" value="+" class="plus">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">                          
                                                    <label><i class="fa fa-calendar-check-o"></i>  </label>
                                                    <input type="text" name="res_date" id="res_date" placeholder="Date" class="datepicker"   data-large-mode="true" data-large-default="true" value=""/>
                                                </div>
                                                <div class="col-md-3"> 
                                                    <label><i class="fa fa-clock-o"></i>  </label>
                                                    <input type="text" name="start_time" placeholder="Time" id="start_time" class="timepicker" value="12:00 am"/>
                                                </div>
                                                <div class="col-md-3"> 
                                                    <label><i class="fa fa-clock-o"></i>  </label>
                                                    <input type="text" name="end_time" placeholder="Time" id="end_time" class="timepicker" value="12:00 am"/>
                                                </div>
                                            </div>
                                            <textarea name="note" cols="40" rows="3" placeholder="Additional Information:"></textarea>
                                        </fieldset>
                                        <button type="button" class="btn  big-btn  color-bg flat-btn" id="btn_res_submit">Submit<i class="fa fa-angle-right"></i></button>
                                    </form>
                                    <?php } else { ?>      
                                        <div style="display: flex;"><span>Already have an account? Please</span><a href="<?=site_url('signin')?>" style="color:blue; cursor: pointer;font-weight: bold" target="_blank">&nbsp;Sign in&nbsp;</a><span>to book a reservation.</span></div>
                                    <?php } ?> 
                                </div>                                
                            </div>                       
                        </div>
                    </div>
                    <!--box-widget-wrap -->
                    <div class="col-md-4">
                        <div class="box-widget-wrap">
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap">
                                <div class="box-widget-item-header">
                                    <h3>Working Hours : </h3>
                                </div>
                                <div class="box-widget opening-hours">
                                    <div class="box-widget-content">
                                        <!-- <span class="current-status"><i class="fa fa-clock-o"></i> Now Open</span> -->
                                        <ul>
                                            <?php
                                            foreach($working_hours as $working_hour) {
                                                echo '<li><span class="opening-hours-day">'.$weekdays[$working_hour['weekday']].'</span>';
                                                if($working_hour['status'] == OPEN) {
                                                    echo '<span class="opening-hours-time">'.date('g:i A',strtotime($working_hour['open_hour'])).' - '.date('g:i A',strtotime($working_hour['close_hour'])).'</span>';
                                                } else {
                                                    echo '<span class="opening-hours-time">Closed</span>';
                                                }
                                                echo '</li>';
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->  							
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap">
                                <div class="box-widget-item-header">
                                    <h3>Location / Contacts : </h3>
                                </div>
                                <div class="box-widget">
                                    <div class="map-container">
                                        <div id="singleMap" data-latitude="<?=$location_info['latitude']?>" data-longitude="<?=$location_info['longitude']?>" data-mapTitle="Our Location"></div>
                                    </div>
                                    <div class="box-widget-content">
                                        <div class="list-author-widget-contacts list-item-widget-contacts">
                                            <ul>
                                                <li><span><i class="fa fa-map-marker"></i> Address :</span> <a href="#"><?=substr($location_info['address'], 0,-5);?></a></li>
                                                <li><span><i class="fa fa-phone"></i> Phone :</span> <a href="#"><?=$location_info['phone']?></a></li>
                                                <li><span><i class="fa fa-envelope-o"></i> Mail :</span> <a href="#"><?=$location_info['email']?></a></li>
                                                <li><span><i class="fa fa-globe"></i> Website :</span> <a href="#"><?=$location_info['website_url']?></a></li>
                                                <li><span><i class="fa fa-th-list"></i> Menu :</span> <a href="<?=$location_info['menu_url']?>" target="_blank"><?=$location_info['menu_url']?></a></li>
                                            </ul>
                                        </div>
                                        <div class="list-widget-social">
                                            <ul>
                                                <li><a href="#" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->
                            <!--box-widget-item -->
                            
                            <!--box-widget-item end --> 
                            <!--box-widget-item -->
                            <!-- <div class="box-widget-item fl-wrap">
                                <div class="box-widget-item-header">
                                    <h3>Our Instagram : </h3>
                                </div>
                                <div class="box-widget">
                                    <div class="box-widget-content">
             							<div class='jr-insta-thumb' id="insta-content" data-instatoken="3075034521.5d9aa6a.284ff8339f694dbfac8f265bf3e93c8a"></div>
                                        <a class="widget-posts-link" href="#" target="_blank">Follow Us<span><i class="fa fa-angle-right"></i></span></a>
                                    </div>
                                </div>
                            </div> -->
                            <!--box-widget-item end -->										
                            <!--box-widget-item -->
                            <!-- <div class="box-widget-item fl-wrap">
                                <div class="box-widget-item-header">
                                    <h3>Hosted by : </h3>
                                </div>
                                <div class="box-widget list-author-widget">
                                    <div class="list-author-widget-header shapes-bg-small  color-bg fl-wrap">
                                        <span class="list-author-widget-link"><a href="author-single.html">Alisa Noory</a></span>
                                        <img src="<?=base_url('assets/front/')?>images/avatar/avatar-bg.png" alt=""> 
                                    </div>
                                    <div class="box-widget-content">
                                        <div class="list-author-widget-text">
                                            <div class="list-author-widget-contacts">
                                                <ul>
                                                    <li><span><i class="fa fa-phone"></i> Phone :</span> <a href="#">+7(123)987654</a></li>
                                                    <li><span><i class="fa fa-envelope-o"></i> Mail :</span> <a href="#">AlisaNoory@domain.com</a></li>
                                                    <li><span><i class="fa fa-globe"></i> Website :</span> <a href="#">themeforest.net</a></li>
                                                </ul>
                                            </div>
                                            <a href="author-single.html" class="btn transparent-btn">View Profile <i class="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!--box-widget-item end -->
                            <!--box-widget-item -->
                            <!-- <div class="box-widget-item fl-wrap">
                                <div class="box-widget-item-header">
                                    <h3>More from this owner : </h3>
                                </div>
                                <div class="box-widget widget-posts">
                                    <div class="box-widget-content">
                                        <ul>
                                            <li class="clearfix">
                                                <a href="#"  class="widget-posts-img"><img src="<?=base_url('assets/front/')?>images/all/1.jpg"  alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title="">Cafe "Lollipop"</a>
                                                    <span class="widget-posts-date"><i class="fa fa-calendar-check-o"></i> 21 Mar 2017 </span> 
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <a href="#"  class="widget-posts-img"><img src="<?=base_url('assets/front/')?>images/all/1.jpg"  alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title=""> Apartment in the Center</a>
                                                    <span class="widget-posts-date"><i class="fa fa-calendar-check-o"></i> 7 Mar 2017 </span> 
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <a href="#"  class="widget-posts-img"><img src="<?=base_url('assets/front/')?>images/all/1.jpg"  alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title="">Event in City Mol</a>
                                                    <span class="widget-posts-date"><i class="fa fa-calendar-check-o"></i> 7 Mar 2017 </span>
                                                </div>
                                            </li>
                                        </ul>
                                        <a class="widget-posts-link" href="#">See All Listing<span><i class="fa fa-angle-right"></i></span></a>   
                                    </div>
                                </div>
                            </div> -->
                            <!--box-widget-item end -->     
                        </div>
                    </div>
                    <!--box-widget-wrap end -->
                </div>
            </div>
        </section>
        <!--  section  end--> 
        <div class="limit-box fl-wrap"></div>
        <!--  section   --> 
        <section class="gradient-bg">
            <div class="cirle-bg">
                <div class="bg" data-bg="<?=base_url('assets/front/')?>images/bg/circle.png"></div>
            </div>
            <div class="container">
                <div class="join-wrap fl-wrap">
                    <div class="row">
                        <div class="col-md-8">
                            <h3>Join our online community</h3>
                            <p>Grow your marketing and be happy with your online business</p>
                        </div>
                        <div class="col-md-4"><a href="#" class="join-wrap-btn modal-open">Sign Up <i class="fa fa-sign-in"></i></a></div>
                    </div>
                </div>
            </div>
        </section>
        <!--  section  end--> 
    </div>
    <!--  content  end--> 
</div>
<!-- wrapper end -->