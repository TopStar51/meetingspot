<script type="text/javascript" src="<?=base_url('assets/front/')?>js/map_infobox.js"></script>
<script type="text/javascript" src="<?=base_url('assets/front/')?>js/markerclusterer.js"></script>  
<script type="text/javascript" src="<?=base_url('assets/front/')?>js/maps.js"></script>
<script>
$(document).ready(function(){
	var selected_room_capacity = 0;
	$("#room_id").change(function(){
		selected_room_capacity = $(this).find(':selected').data('capacity');
		//$("#guest_number").prop("max", selected_room_capacity);
	})

	$("input[name='is_group']").change(function(){
		if($("#group_res").prop('checked')) {
			<?php if(!empty($groups)) { ?>
			$("#existent_group").show();
			$("#new_group").hide();
			<?php } else { ?>
			$("#existent_group").hide();
			$("#new_group").show();	
			<?php } ?>
		} else {
			$("#existent_group").hide();
			$("#new_group").hide();

		}
	})

	$("#btn_res_submit").click(function(){
		if($("#room_id").val() == '' || $("#room_id").val() == undefined) {
			swal({
				title: "",
				text: "Please select a room",
				icon: "warning",
			});
			return;
		}

		if($("#group_id").css('display') != 'none' && $("#group_id").val() == '') {
			swal({
				title: "",
				text: "Please select your group",
				icon: "warning",
			});
			return;
		}

		if($("#new_group").css('display') != 'none' && $("#group_name").val() == '') {
			swal({
				title: "",
				text: "Please input group name",
				icon: "warning",
			});
			return;
		}

		if($("#new_group").css('display') != 'none' && $("#min_age").val() == '') {
			swal({
				title: "",
				text: "Please input minimum age of members",
				icon: "warning",
			});
			return;
		}

		if($("input[name=guest_number]").val() =='' || $("input[name=guest_number]").val() == 'undefined') {
			swal({
				title: "",
				text: "Please input guest number",
				icon: "warning",
			});
			return;
		}
		if(parseInt($("input[name=guest_number]").val()) > selected_room_capacity) {
			swal({
				title: "",
				text: "The number of guests should be smaller than capacity. This room's capacity : " + selected_room_capacity,
				icon: "warning",
			});
			return;
		}
		if(Date.parse($("#res_date").val()) < Date.parse(new Date())) {
			swal({
				title: "",
				text: "Please input valid date.",
				icon: "warning",
			});
			return;
		}

		if(changeTimeFormat($("#res_date").val(), $("#start_time").val()) >= changeTimeFormat($("#res_date").val(), $("#end_time").val())) {
			swal({
				title: "",
				text: "Please input valid start and end time.",
				icon: "warning",
			});
			return;
		}

		$.ajax({
			url: '<?=site_url('reservation/save_reservation')?>',
			method: 'POST',
			data: $(".book-reservation").serializeArray(),
			dataType: 'json',
			success: function(resp){
				if(resp.status == 'success') {
					swal({
						title: "",
						text: resp.msg,
						icon: "success",
					}).then((value)=>{
						window.location.href = '<?=site_url('customer/reservation')?>';
					});
					return;
				} else {
					swal({
						title: "",
						text: resp.msg,
						icon: "error",
					});
					return;
				}
			}
		})
	});
})

function changeTimeFormat(date, time) {
	var hours = Number(time.match(/^(\d+)/)[1]);
	var minutes = Number(time.match(/:(\d+)/)[1]);
	var AMPM = time.match(/\s(.*)$/)[1];
	if(AMPM == "pm" && hours<12) hours = hours+12;
	if(AMPM == "am" && hours==12) hours = hours-12;
	var sHours = hours.toString();
	var sMinutes = minutes.toString();
	if(hours<10) sHours = "0" + sHours;
	if(minutes<10) sMinutes = "0" + sMinutes;
	return Date.parse(date + " " + sHours + ":" + sMinutes);
}
</script>