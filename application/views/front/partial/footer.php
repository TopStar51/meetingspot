            <!--footer -->
            <footer class="main-footer dark-footer  ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="footer-widget fl-wrap">
                                <h3>About Us</h3>
                                <div class="footer-contacts-widget fl-wrap">
                                    <p><?=json_decode($footer_data['about_us_text'])->text;?> </p>
                                    <ul  class="footer-contacts fl-wrap">
                                        <li><span><i class="fa fa-envelope-o"></i> Email :</span><a href="#" target="_blank"><?=json_decode($footer_data['contact_info'])->email;?></a></li>
                                        <li> <span><i class="fa fa-map-marker"></i> Address :</span><a href="#" target="_blank"><?=json_decode($footer_data['contact_info'])->address;?></a></li>
                                        <li><span><i class="fa fa-phone"></i> Phone :</span><a href="#"><?=json_decode($footer_data['contact_info'])->phone;?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-3">
                            <div class="footer-widget fl-wrap">
                                <h3>Our Last News</h3>
                                <div class="widget-posts fl-wrap">
                                    <ul>
                                        <li class="clearfix">
                                            <a href="#"  class="widget-posts-img"><img src="<?=base_url('assets/front')?>/images/all/1.jpg" class="respimg" alt=""></a>
                                            <div class="widget-posts-descr">
                                                <a href="#" title="">Vivamus dapibus rutrum</a>
                                                <span class="widget-posts-date"> 21 Mar 09.05 </span>
                                            </div>
                                        </li>
                                        <li class="clearfix">
                                            <a href="#"  class="widget-posts-img"><img src="<?=base_url('assets/front')?>/images/all/1.jpg" class="respimg" alt=""></a>
                                            <div class="widget-posts-descr">
                                                <a href="#" title=""> In hac habitasse platea</a>
                                                <span class="widget-posts-date"> 7 Mar 18.21 </span>
                                            </div>
                                        </li>
                                        <li class="clearfix">
                                            <a href="#"  class="widget-posts-img"><img src="<?=base_url('assets/front')?>/images/all/1.jpg" class="respimg" alt=""></a>
                                            <div class="widget-posts-descr">
                                                <a href="#" title="">Tortor tempor in porta</a>
                                                <span class="widget-posts-date"> 7 Mar 16.42 </span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="col-md-4">
                            <div class="footer-widget fl-wrap">
                                <h3>Our  Twitter</h3>
                                <div id="footer-twiit"></div>
                            </div>
                        </div> -->
                        <div class="col-md-6">
                            <div class="footer-widget fl-wrap">
                                <h3>Subscribe</h3>
                                <div class="subscribe-widget fl-wrap">
                                    <p><?=json_decode($footer_data['subscribe_text'])->text;?></p>
                                    <div class="subcribe-form">
                                        <form id="subscribe">
                                            <input class="enteremail" name="email" id="subscribe-email" placeholder="Email" spellcheck="false" type="text">
                                            <button type="submit" id="subscribe-button" class="subscribe-button"><i class="fa fa-rss"></i> Subscribe</button>
                                            <label for="subscribe-email" class="subscribe-message"></label>
                                        </form>
                                    </div>
                                </div>
                                <div class="footer-widget fl-wrap">
                                    <div class="footer-menu fl-wrap">
                                        <ul>
                                            <?php foreach($menus as $menu) { ?>
                                            <li><a href="<?=$menu['url']?>"><?=$menu['menu_name']?> </a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sub-footer fl-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="about-widget">
                                    <img src="<?=base_url('assets/front/')?>images/logo.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="copyright"> &#169; <?=json_decode($footer_data['footer_text'])->text;?></div>
                            </div>
                            <div class="col-md-4">
                                <div class="footer-social">
                                    <ul>
                                        <li><a href="<?=json_decode($footer_data['social_info'])->facebook;?>" target="_blank" ><i class="fa fa-facebook-official"></i></a></li>
                                        <li><a href="<?=json_decode($footer_data['social_info'])->twitter;?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="<?=json_decode($footer_data['social_info'])->instagram;?>" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="<?=json_decode($footer_data['social_info'])->whatsapp;?>" target="_blank" ><i class="fa fa-whatsapp"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!--footer end  -->
            <a class="to-top"><i class="fa fa-angle-up"></i></a>
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="<?=base_url('assets/front/js/jquery.min.js')?>"></script>
        <script type="text/javascript" src="<?=base_url('assets/front/js/plugins.js')?>"></script>
        <script type="text/javascript" src="<?=base_url('assets/front/js/scripts.js')?>"></script>
        <script type="text/javascript" src="<?=base_url('assets/front/js/sweetalert.min.js')?>"></script>
        <script src="<?php echo base_url('assets/admin'); ?>/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <?php
            $this->load->view('front/partial/global-js');
            $this->load->view('front/'.$jslink);
        ?>
    </body>
</html>