<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Meetingspot</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="<?=base_url('assets/front/css/reset.css')?>">
        <link type="text/css" rel="stylesheet" href="<?=base_url('assets/front/css/plugins.css')?>">
        <link type="text/css" rel="stylesheet" href="<?=base_url('assets/front/css/style.css')?>">
        <link type="text/css" rel="stylesheet" href="<?=base_url('assets/front/css/color.css')?>">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" type="image/png" href="<?= base_url('assets') ?>/front/images/favicon.png"/>

        <script type="text/javascript">
            var BASE_URL = "<?php echo base_url('') ?>";
            var marker_url = BASE_URL + "/assets/front/images/marker.png";
        </script>
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main">
            <!-- header-->
            <header class="main-header dark-header fs-header sticky">
                <div class="header-inner">
                    <div class="logo-holder">
                        <a href="<?=site_url('home')?>"><img src="<?=base_url('assets/front/images/logo.png')?>" alt=""></a>
                    </div>
                    <!-- <div class="show-search-button"><i class="fa fa-search"></i> <span>Search</span></div> -->
                    <!-- <a href="<?=site_url('owner/login')?>" class="add-list" target="_blank">For location owners <span><i class="fa fa-plus"></i></span></a> -->
                    <?php
                    if(isset($type) && $type == CUSTOMER) { ?>
                    <div class="header-user-menu">
                        <div class="header-user-name">
                            <span><img src="<?=base_url('assets/front/')?>images/avatar/avatar-bg.png" alt=""></span>
                            Hello , <?=$user_name?>
                        </div>
                        <ul>
                            <li><a href="<?=site_url('customer/profile')?>"> Edit profile</a></li>
                            <li><a href="<?=site_url('customer/change_password')?>"> Change Password</a></li>
                            <li><a href="<?=site_url('customer/mygroup')?>"> My Group</a></li>
                            <li><a href="<?=site_url('customer/reservation')?>"> Reservations</a></li>
                            <li><a href="<?=site_url('customer/review')?>"> Reviews</a></li>
                            <li><a href="<?=site_url('customer/add_group')?>"> Add Group</a></li>
                            <li><a href="<?=site_url('customer/logout')?>">Log Out</a></li>
                        </ul>
                    </div>
                    <?php } ?>
                    <!-- nav-button-wrap-->
                    <div class="nav-button-wrap color-bg">
                        <div class="nav-button">
                            <span></span><span></span><span></span>
                        </div>
                    </div>
                    <!-- nav-button-wrap end-->
                    <!--  navigation -->
                    <div class="nav-holder main-menu">
                        <nav>
                            <ul>
                                <?php
                                foreach($menus as $menu) { 
                                    if(($menu['name'] == 'signin' || $menu['name'] == 'register') && $type == CUSTOMER)
                                        continue;
                                    else { ?>
                                        <li>
                                            <a href="<?=$menu['url']?>" class="<?= $path[0] == $menu['name']? 'act-link' : ''?>"><?=$menu['menu_name']?> </a>
                                        </li>
                                    <?php } 
                                } ?>
                            </ul>
                        </nav>
                    </div>
                    <!-- navigation  end -->
                </div>
            </header>
            <!--  header end -->