<style type="text/css">
    section > div.container {
        width: 30%;
    }
    
    .custom-form input[type="text"], .custom-form input[type="password"] {
        padding: 15px;
        margin-bottom: 5px;
        font-size: 14px;
    }

    .custom-form label{
        font-size: 14px;
        margin-bottom: 10px;
    }

    .log-submit-btn {
        width: 100%;
        font-size: 14px;
    }
    span.text-danger {
        color: red;
        float: left;
    }
    .row {
        margin-top: 20px;
    }
    .container > p {
        color: green;
        font-size: 16px;
        background: lightblue;
        padding: 10px;
    }

</style>
<div id="wrapper">
    <!--content-->  
    <div class="content">
        <section>
            <div class="container">
                <?php if($this->session->flashdata('msg')) { ?>
                    <p><?=$this->session->flashdata('msg')?></p>
                <?php } ?>
                <div class="custom-form">
                    <?php echo form_open('register'); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <input name="first_name" type="text" placeholder="First Name" value="<?php echo set_value('first_name');?>">
                                <?php echo form_error('first_name', '<span class="text-danger">', '</span>'); ?>
                            </div>
                            <div class="col-md-6">
                                <input name="last_name" type="text" placeholder="Last Name" value="<?php echo set_value('last_name');?>">
                                <?php echo form_error('last_name', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input name="username" type="text" placeholder="Username" value="<?php echo set_value('username');?>">
                                <?php echo form_error('username', '<span class="text-danger">', '</span>'); ?>
                            </div>
                            <div class="col-md-6">
                                <input name="phone" type="text" placeholder="Phone Number" value="<?php echo set_value('phone');?>">
                                <?php echo form_error('phone', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input name="email" type="text" placeholder="Email" value="<?php echo set_value('email');?>">
                                <?php echo form_error('email', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input name="address" type="text" placeholder="Address1" value="<?php echo set_value('address');?>">
                                <?php echo form_error('address', '<span class="text-danger">', '</span>'); ?>
                            </div>
                            <div class="col-md-6">
                                <input name="address2" type="text" placeholder="Address2" value="<?php echo set_value('address2');?>">
                                <?php echo form_error('address2', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <input name="city" type="text" placeholder="City" value="<?php echo set_value('city');?>">
                                <?php echo form_error('city', '<span class="text-danger">', '</span>'); ?>
                            </div>
                            <div class="col-md-4">
                                <input name="state" type="text" placeholder="State" value="<?php echo set_value('state');?>">
                                <?php echo form_error('state', '<span class="text-danger">', '</span>'); ?>
                            </div>
                            <div class="col-md-4">
                                <input name="zip" type="text" placeholder="Zip Code" value="<?php echo set_value('zip');?>">
                                <?php echo form_error('zip', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input name="password" type="password" placeholder="Password" value="<?php echo set_value('password');?>">
                                <?php echo form_error('password', '<span class="text-danger">', '</span>'); ?>
                            </div>
                            <div class="col-md-6">
                                <input name="password_confirm" type="password" placeholder="Confirm Password" value="<?php echo set_value('password_confirm');?>">
                                <?php echo form_error('password_confirm', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <!--col --> 
                            <div class="col-md-6">
                                <label class="radio inline"> 
                                <input type="radio" name="type" checked value="<?=CUSTOMER?>">
                                <span style="color:black">Normal User</span> 
                                </label>
                            </div>
                            <!--col end--> 
                            <!--col --> 
                            <div class="col-md-6">
                                <label class="radio inline"> 
                                <input type="radio" name="type" value="<?=OWNER?>">
                                <span  style="color:green">Location Owner</span> 
                                </label>
                            </div>
                            <!--col end--> 
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="log-submit-btn" id="btn_register"><strong>Register</strong></button>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </section>
    </div>
</div>