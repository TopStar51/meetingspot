<style type="text/css">
	section > div.container {
		width: 25%;
		min-height: 25vh;
	}
    
	.custom-form input[type="text"], .custom-form input[type="password"] {
		padding: 15px;
	}

	.custom-form label{
		font-size: 14px;
    	margin-bottom: 10px;
	}

    .container > p {
        color: green;
        font-size: 16px;
        background: lightblue;
        padding: 10px;
    }

    span.text-danger {
        color: red;
        float: left;
    }

    .col-md-12 {
        margin-top: 20px;
    }

</style>
<div id="wrapper">
    <!--content-->  
    <div class="content">
    	<section>
    		<div class="container">
                <?php if($this->session->flashdata('msg')) { ?>
                    <p><?=$this->session->flashdata('msg')?></p>
                <?php } ?>
    			<div class="custom-form" id="div_reset_form">
                    <?php echo form_open('reset_password/'); ?>
                    <div class="col-md-12">
                        <input name="id" type="text" value="<?=$user_id?>" hidden/>
                        <input name="password" type="password" placeholder="New Password" value="<?php echo set_value('password');?>">
                        <?php echo form_error('password', '<span class="text-danger">', '</span>'); ?>
                    </div>
                    <div class="col-md-12">
                        <input name="password_confirm" type="password" placeholder="Confirm Password" value="<?php echo set_value('password_confirm');?>">
                        <?php echo form_error('password_confirm', '<span class="text-danger">', '</span>'); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <button type="submit"  class="log-submit-btn pull-right">Submit</button>
                        <a href="<?=site_url('signin')?>" class="log-submit-btn" style="background: #fb5050" id="btn_back">Back to Login</a>
                    </div>
                    <?php echo form_close(); ?>
                </div>
    		</div>
    	</section>
    </div>
</div>