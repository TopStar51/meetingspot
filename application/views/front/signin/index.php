<style type="text/css">
	section > div.container {
		width: 25%;
	}
	.custom-form input[type="text"], .custom-form input[type="password"] {
		padding: 15px;
	}

	.custom-form label{
		font-size: 14px;
    	margin-bottom: 10px;
	}

	.log-submit-btn {
		width: 100%;
		font-size: 14px;
	}
</style>
<div id="wrapper">
    <!--content-->  
    <div class="content">
    	<section>
	    	<div class="container">
	    		<div class="custom-form" id="div_login_form">
                    <span class="server-err" style="color: red;"></span>
                    <form name="loginform" id="login_form">
                        <div class="input-group" style="margin-bottom: 10px">
                            <input name="username" type="text" value="" id="username" placeholder="Username or Email Address">
                            <span class="username-err" style="color: red; "></span>
                        </div>
                        <div class="input-group">
                            <input name="password" type="password" value="" id="password" placeholder="Password">
                            <span class="password-err" style="color: red; "></span>
                        </div>
                        <div class="filter-tags">
                            <input id="check-a" type="checkbox" name="check">
                            <label for="check-a">Remember me</label>
                        </div>
                        <button type="button"  class="log-submit-btn" id="btn_login"><span>Login</span></button>
                    </form>
                    <div class="lost_password">
                        <a href="<?=site_url('forgot_password')?>">Forgot Your Password?</a>
                    </div>
                </div>
	    	</div>
	    </section>
    </div>
</div>