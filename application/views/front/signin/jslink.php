<script type="text/javascript">
	$(document).ready(function(){
		$("#btn_login").click(function(){
			if($("#username").val()=='') {
				$("span.username-err").html('Please input the username or email address.');
				return;
			} else {
				$("span.username-err").html('');
			}

			if($("#password").val()=='') {
				$("span.password-err").html('Please input the password.');
				return;
			} else {
				$("span.password-err").html('');
			}

			$.ajax({
				url: "<?=site_url('signin/login')?>",
				method: "POST",
				data: $("#login_form").serializeArray(),
				dataType: "json",
				success: function(resp) {
					if(resp.state == 'success') {
						window.location.href = '<?=site_url()?>' + resp.url;
					} else {
						$("span.server-err").html(resp.msg);
						return;
					}
				}
			})
		});

		$('#login_form input').keypress(function(e) {
		    var key = e.which;
		    if (key == 13) // the enter key code
		    {
		      	$("#btn_login").trigger('click');
		    }
		});
	})
</script>