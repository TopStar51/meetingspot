<div class="m-subsubheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                My Locations
            </h3>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-6 col-xl-3">
        <div class="m-portlet m-portlet--skin-dark m-portlet--bordered-semi m--bg-primary">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Approved
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <h4><?=$location['approved']?></h4>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6 col-xl-3">
        <div class="m-portlet m-portlet--bordered-semi m--bg-secondary">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Pending
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <h4><?=$location['pending']?></h4>
            </div>
        </div>
    </div>
</div>
<div class="m-subsubheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                My Rooms
            </h3>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-6 col-xl-3">
        <div class="m-portlet m-portlet--skin-dark m-portlet--bordered-semi m--bg-info">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Total
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <h4><?=$room?></h4>
            </div>
        </div>
    </div>
</div>
<div class="m-subsubheader">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Reservations
            </h3>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-6 col-xl-3">
        <div class="m-portlet m-portlet--skin-dark m-portlet--bordered-semi m--bg-success">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Total
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <h4><?=$reservation['total']?></h4>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6 col-xl-3">
        <div class="m-portlet m-portlet--skin-dark m-portlet--bordered-semi m--bg-primary">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Confirmed
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <h4><?=$reservation['confirmed']?></h4>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6 col-xl-3">
        <div class="m-portlet m-portlet--bordered-semi m--bg-secondary">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Pending
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <h4><?=$reservation['pending']?></h4>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6 col-xl-3">
        <div class="m-portlet m-portlet--skin-dark m-portlet--bordered-semi m--bg-danger">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Cancelled
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <h4><?=$reservation['cancelled']?></h4>
            </div>
        </div>
    </div>
</div>
<div class="m-subsubheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Reviews
            </h3>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-6 col-xl-3">
        <div class="m-portlet m-portlet--skin-dark m-portlet--bordered-semi m--bg-warning">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Total
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <h4><?=$customer['total']?></h4>
            </div>
        </div>
    </div>
</div>