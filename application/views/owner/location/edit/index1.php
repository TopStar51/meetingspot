<div class="row">
    <div class="col-xl-12">
        <!--Begin::Main Portlet-->
        <div class="m-portlet">
            <!--begin: Form Wizard-->
            <div class="m-wizard m-wizard--5 m-wizard--success" id="m_wizard">
                <!--begin: Message container -->
                <div class="m-portlet__padding-x">
                    <!-- Here you can put a message or alert -->
                </div>
                <!--end: Message container -->
                <!--begin: Form Wizard Head -->
                <div class="m-wizard__head m-portlet__padding-x">
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1">
                            <!--begin: Form Wizard Nav -->
                            <div class="m-wizard__nav">
                                <div class="m-wizard__steps">
                                    <div class="m-wizard__step m-wizard__step--current"
                                         data-wizard-target="#m_wizard_form_step_1">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span class="m-wizard__step-label">
													Basic Info
											    </span>
                                                <span class="m-wizard__step-icon">
													<i class="la la-check"></i>
												</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_2">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span class="m-wizard__step-label">
                                                    Working Hours
                                                </span>
                                                <span class="m-wizard__step-icon">
                                                    <i class="la la-check"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_3">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span class="m-wizard__step-label">
													Media
												</span>
                                                <span class="m-wizard__step-icon">
													<i class="la la-check"></i>
												</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Nav -->
                        </div>
                    </div>
                </div>
                <!--end: Form Wizard Head -->
                <!--begin: Form Wizard Form-->
                <div class="m-wizard__form">
                    <!--
                    1) Use m-form--label-align-left class to alight the form input lables to the right
                    2) Use m-form--state class to highlight input control borders on form validation
                    -->
                    <form class="m-form m-form--label-align-left- m-form--state-" id="m_form">
                        <!--begin: Form Body -->
                        <div class="m-portlet__body">
                            <!--begin: Form Wizard Step 1-->
                            <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                                <div class="row">
                                    <div class="col-xl-10 offset-xl-1">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        * Name:
                                                    </label>
                                                    <input type="text" name="id" <?php if(isset($location)) echo 'value="'.$location['id'].'"'?> class="form-control m-input" hidden>
                                                    <input type="text" name="name" <?php if(isset($location)) echo 'value="'.$location['name'].'"'?>class="form-control m-input">
                                                </div>
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        * Category:
                                                    </label>
                                                    <select class="form-control m-input selectpicker" name="category_id">
                                                        <option value="">
                                                            Select
                                                        </option>
                                                        <?php
                                                        foreach ($categories as $category) {
                                                            echo '<option value="'.$category['id'].'"';
                                                            if (isset($location['category_id']) && $category['id'] == $location['category_id']) {
                                                                echo ' selected ';
                                                            }
                                                            echo '>'.$category['name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        * Phone:
                                                    </label>
                                                    <input type="text" name="phone" class="form-control m-input" <?php if (isset($location)) echo 'value="'.$location['phone'].'"';?>>
                                                </div>
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        * Address:
                                                    </label>
                                                    <input type="text" name="address" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['address'].'"'?> id="location_address">
                                                    <input type="text" name="city" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['city'].'"'?> id="locality" hidden>
                                                    <input type="text" name="state" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['state'].'"'?> id="administrative_area_level_1" hidden>
                                                    <input type="text" name="zip" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['zip'].'"'?> id="postal_code" hidden>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        * Website:
                                                    </label>
                                                    <input type="text" name="website_url" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['website_url'].'"'?>>
                                                </div>
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        Menu link:
                                                    </label>
                                                    <input type="text" name="menu_url" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['menu_url'].'"'?>>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        * Description:
                                                    </label>
                                                    <textarea class="form-control m-input" style="min-height: 150px;" name="description"> <?php if(isset($location)) echo $location['description'];?></textarea>
                                                </div>
                                            </div>
                                                <!-- <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        * City:
                                                    </label>
                                                    <input type="text" name="city" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['city'].'"'?>>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        * State:
                                                    </label>
                                                    <input type="text" name="state" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['state'].'"'?>>
                                                </div>
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        * Zip code:
                                                    </label>
                                                    <input type="text" name="zip" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['zip'].'"'?>>
                                                </div>
                                            </div> -->
                                            <!-- <div class="form-group m-form__group row">
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        * Latitude:
                                                    </label>
                                                    <input type="text" name="latitude" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['latitude'].'"'?>>
                                                </div>
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        * Longitude:
                                                    </label>
                                                    <input type="text" name="longitude" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['longitude'].'"'?>>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
                            <!--begin: Form Wizard Step 2-->
                            <div class="m-wizard__form-step" id="m_wizard_form_step_2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="m-form__section">
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-2 m-form__group-sub"></div>
                                                <div class="col-lg-3 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        Open Hour
                                                    </label>
                                                </div>
                                                <div class="col-lg-3 m-form__group-sub">
                                                    <label class="form-control-label">
                                                        Close Hour
                                                    </label>
                                                </div>
                                                <div class="col-lg-3 m-form__group-sub">
                                                </div>
                                            </div>
                                            <?php
                                            $index = 0;
                                            foreach (array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday') as $day) {
                                                $index += 1;
                                                if ($index == 7) $index = 0;
                                                ?>
                                                <div class="my-div3 row">
                                                    <label class="col-md-2" style="text-align: right;"><?=$day?></label>
                                                    <div class="col-md-3">
                                                        <div class="input-group timepicker">
                                                            <input name="working_hour[<?=$index?>][open_hour]"
                                                                <?php if (isset($working_hours) && array_key_exists($index, $working_hours)) echo 'value="'.$working_hours[$index]['open_hour'].'"'; ?>
                                                                   type="text" class="form-control timepicker-no-seconds">
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-clock-o"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="input-group timepicker">
                                                            <input name="working_hour[<?=$index?>][close_hour]"
                                                                <?php if (isset($working_hours) && array_key_exists($index, $working_hours)) echo 'value="'.$working_hours[$index]['close_hour'].'"'; ?>
                                                                   type="text" class="form-control timepicker-no-seconds">
                                                                <span class="input-group-btn">
                                                                    <button class="btn default" type="button">
                                                                        <i class="fa fa-clock-o"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-4 my-switch1">
                                                        <input name="working_hour[<?=$index?>][status]" type="checkbox" data-switch="true"
                                                            <?php if (isset($working_hours) && array_key_exists($index, $working_hours) && $working_hours[$index]['status'] == 'OPEN') echo 'checked'; ?>
                                                               class="make-switch" data-on-text="Open&nbsp;" data-off-text="Close&nbsp;" data-on-color="success" data-off-color="danger">
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 2-->
                            <!--begin: Form Wizard Step 3-->
                            <div class="m-wizard__form-step" id="m_wizard_form_step_3">
                                <div class="row">
                                    <div class="my-div1 col-md-4 col-lg-3">
                                    <div class="my-div1">
                                        <label>Thumbnail Image</label>
                                        <img class="img-view" <?php if (isset($location)) echo 'src="'.base_url($location['thumb_image']).'"';?>>
                                        <input class="img-input" type="file" accept="image/*" name="thumb_image">
                                    </div>
                                    <div class="my-div1">
                                        <label>Video</label>
                                        <div action="<?=site_url()?>" class="dropzone dropzone-file-area" id="video-dropzone" style="width: 100%;">
                                            <h3 class="sbold">Drop files here or click to upload</h3>
                                        </div>
                                        <label>Youtube</label>
                                        <input name="video" type="text" placeholder="https://wwww.youtube.com/" <?php if (isset($location)) echo 'value="'.$location['video'].'"';?> class="form-control">
                                        <div style="margin-top: 10px;">
                                            <video id="video-location" controls="show" style="width: 100%;" <?php if(isset($location)) echo 'src="'.$location['video'].'"';?>></video>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="my-div1 col-md-8 col-lg-9">
                                        <label>Gallery</label>
                                        <div action="<?=site_url()?>" class="dropzone dropzone-file-area" id="gallery-dropzone" style="width: 500px; margin-top: 50px; margin-bottom: 20px;">
                                            <h3 class="sbold">Drop files here or click to upload</h3>
                                        </div>
                                        <div>
                                            <?php if (isset($location)) {
                                                $galleries = json_decode($location['gallery']);
                                                if (is_array($galleries)) {
                                                    echo '<div class="row">';
                                                    foreach ($galleries as $image) { ?>
                                                        <div class="col-md-6 col-lg-4">
                                                            <img src="<?=base_url('upload/location/'.$image) ?>" class="img-gallery" data-name="<?=$image?>">
                                                            <button class="button-remove-gallery btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                                        </div>
                                                    <?php }
                                                    echo '</div>';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 3-->
                        </div>
                        <!--end: Form Body -->
                        <!--begin: Form Actions -->
                        <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-4 m--align-left">
                                        <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon"
                                           data-wizard-action="prev">
                                            <span>
                                                <i class="la la-arrow-left"></i>
                                                <span>
                                                    Back
                                                </span>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-lg-6 m--align-right">
                                        <a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon"
                                           data-wizard-action="submit">
                                            <span>
                                                <i class="la la-check"></i>
                                                <span>
                                                    Submit
                                                </span>
                                            </span>
                                        </a>
                                        <a href="#" class="btn btn-warning m-btn m-btn--custom m-btn--icon"
                                           data-wizard-action="next">
                                            <span>
                                                <span>
                                                    Save & Continue
                                                </span>
                                                <i class="la la-arrow-right"></i>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-lg-1"></div>
                                </div>
                            </div>
                        </div>
                        <!--end: Form Actions -->
                    </form>
                </div>
                <!--end: Form Wizard Form-->
            </div>
            <!--end: Form Wizard-->
        </div>
        <!--End::Main Portlet-->
    </div>
</div>
