<script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_MAP_API_KEY ?>&libraries=places"></script>
<script type="text/javascript">

    var dzThumbnail = null;
    var dzGallery = null;
    var dzVideo = null;

	var BootstrapSwitch = function () {

        //== Private functions
        var demos = function () {
            // minimum setup
            $('[data-switch=true]').bootstrapSwitch();
        }

        return {
            // public functions
            init: function () {
                demos();
            }
        };
    }();

    var DropzoneDemo = function () {
        //== Private functions
        var demos = function () {
            Dropzone.options.mDropzoneOne = {
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 1,
            maxFilesize: 5, // MB
            addRemoveLinks: true,
            accept: function(file, done) {
                if (file.name == "justinbieber.jpg") {
                    done("Naha, you don't.");
                } else { 
                    done(); 
                }
            },
            init: function() {
                alert('hey');
            }
        };
            // file type validation
            Dropzone.options.thumbImages = {
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 1,
                maxFilesize: 10, // MB
                addRemoveLinks: true,
                dictRemoveFile: "Remove File",
                acceptedFiles: "image/*",
                init: function() {
                    dzThumbnail = this;
                }
            };

            Dropzone.options.galleryDropzone = {
                acceptedFiles: 'image/*',
                paramName: "file", 
                maxFilesize: 10, // MB
                // autoProcessQueue: false,
                addRemoveLinks: true,
                dictRemoveFile: "Remove File",
                init: function() {
                    dzGallery = this;
                }
            };

            Dropzone.options.videoDropzone = {
                acceptedFiles: 'video/*',
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 1,
                maxFilesize: 100, // MB
                addRemoveLinks: true,
                dictRemoveFile: "Remove File",
                init: function() {
                    dzVideo = this;
                }
            };
        }

        return {
            // public functions
            init: function () {
                demos();
            }
        };
    }();

    var WizardDemo = function () {
        //== Base elements
        var formEl = $('#m_form');
        var validator;

        //== Private functions
        var initWizard = function () {
            // timepicker
            $('.timepicker').timepicker({
                minuteStep: 1,
                showSeconds: false,
                showMeridian: true,
                defaultTime: '12:00 AM'
            });
            $('.timepicker').timepicker({
                minuteStep: 1,
                showSeconds: false,
                showMeridian: true,
                defaultTime: '11:59 PM'
            });
        }

        var initValidation = function () {
            validator = formEl.validate({
                //== Validate only visible fields
                ignore: ":hidden",

                //== Validation rules
                rules: {
                    //=== Location Information(step 1)
                    //== Basic Info
                    name: {
                        required: true
                    },
                    category_id: {
                        required: true
                    },
                    phone: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    address: {
                        required: true
                    },
                    website_url: {
                        url: true
                    },
                    menu_url: {
                        url: true
                    }
                },

                //== Validation messages
                messages: {
                    'account_communication[]': {
                        required: 'You must select at least one communication option'
                    },
                    accept: {
                        required: "You must accept the Terms and Conditions agreement!"
                    }
                },

                //== Display error
                invalidHandler: function (event, validator) {
                    mApp.scrollTop();

                    swal({
                        "title": "",
                        "text": "There are some errors in your submission. Please correct them.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                },

                //== Submit valid form
                submitHandler: function (form) {

                }
            });
        }

        var initSubmit = function () {
            var btn = formEl.find('[type="submit"]');

            // btn.on('click', function (e) {
            btn.click(function(e) {
                e.preventDefault();

                if (validator.form()) {
                    var formData = new FormData();
                    var form = $('#m_form')[0];
                    var fileIndex = 0;
                    for (var i =0; i < form.length; i +=1) {
                        if (form[i].name == 'amenity[]') continue;
                        if (form[i].type == 'file') {
                            //thumb & video file uploading
                            //thumbnail, video, gallery
                            // if (form[i].name == 'gallery') {
                            //     formData.append('gallery[]', form[i].files[fileIndex]);
                            // } else {
                            if (form[i].files.length > 0)
                                formData.append(form[i].name, form[i].files[fileIndex]);
                            // }
                            fileIndex += 1;
                        } else if (form[i].type == 'checkbox') {
                            if (form[i].checked) {
                                formData.append(form[i].name, '1');
                            } else {
                                formData.append(form[i].name, '0');
                            }
                        } else {
                            formData.append(form[i].name, form[i].value);
                        }
                    }
                    if (dzThumbnail && dzThumbnail.files.length > 0) {
                        formData.append('thumb_image', dzThumbnail.files[0]);
                    }
                    <?php if (!isset($location)) { ?>
                    if (!dzThumbnail || dzThumbnail.files.length == 0) {
                        swal('','You must select a thumnail image.','warning');
                        return;
                    }
                    <?php } ?>

                    //send gallery dropzone files to server
                    var galleryCount = 0;
                    if (dzGallery) {
                        galleryCount += dzGallery.files.length;
                        for (var i =0; i < dzGallery.files.length; i += 1) {
                            formData.append('gallery[]', dzGallery.files[i]);
                        }
                    }

                    $('img.img-gallery').each(function() {
                        formData.append('gallery[]', $(this).data('name'));
                        galleryCount += 1;
                    });

                    if (galleryCount < 2) {
                        swal('','You must select at least 2 gallery images.','warning');
                        return;
                    }

                    if (dzVideo && dzVideo.files.length > 0) {
                        formData.append('video', dzVideo.files[0]);
                    }

                    <?php if (!isset($location)) { ?>
                        if (!dzVideo || dzVideo.files.length == 0) {
                            swal('','You must select a video for location.','warning');
                            return;
                        }
                    <?php } ?>

                    //== See: src\js\framework\base\app.js
                    mApp.progress(btn);
                    //mApp.block(formEl);

                    process_form_data('', '<?=site_url('owner/location/save_location')?>', formData,
                        function() {
                            mApp.unprogress(btn);
                            //mApp.unblock(formEl);

                            swal({
                                "title": "",
                                "text": "The location has been successfully submitted!",
                                "type": "success",
                                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                            }).then(function(result) {
                                window.location.href = '<?=site_url('/owner/location')?>';
                            });
                        },
                        function () {
                            alert('error occurred');
                            // console.log('error occurred');
                        }
                    );
                }
            });
        }

        return {
            // public functions
            init: function () {
                formEl = $('#m_form');

                initWizard();
                initValidation();
                initSubmit();
            }
        };
    }();

    var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        postal_code: 'short_name'
    };

    function initialize() {

        var options = {
            componentRestrictions: {country: "us"}
        };

        var input = document.getElementById('location_address');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }
            for (var component in componentForm) {
                document.getElementById(component).value = '';
            }
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {

                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        });     
        
    }
    google.maps.event.addDomListener(window, 'load', initialize);

    jQuery(document).ready(function () {
    	$('.timepicker').timepicker({
            defaultTime: '11:45 AM',
        });
        WizardDemo.init();
        BootstrapSwitch.init();
        //inputImage();

        $('.button-remove').click(function() {
            $(this).closest('div').remove();
        });

        <?php if (isset($alert_owner)) { ?>
            alert('You should register at least one location');
        <?php } ?>


    });

    DropzoneDemo.init();
</script>