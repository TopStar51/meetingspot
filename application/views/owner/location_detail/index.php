<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="m-portlet">
			<!--begin::Form-->
			<form class="m-form">
				<div class="m-portlet__body">
					<div class="m-form__section m-form__section--first">
						<div class="m-form__heading">
							<h3 class="m-form__heading-title">
								Basic Info
							</h3>
						</div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Name:
                                </label>
                                <input type="text" name="id" <?php if(isset($location)) echo 'value="'.$location['id'].'"'?> class="form-control m-input" hidden>
                                <input type="text" name="name" <?php if(isset($location)) echo 'value="'.$location['name'].'"'?>class="form-control m-input">
                            </div>
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Category:
                                </label>
                                <select class="form-control m-input selectpicker" name="category_id">
                                    <option value="">
                                        Select
                                    </option>
                                    <?php
                                    foreach ($categories as $category) {
                                        echo '<option value="'.$category['id'].'"';
                                        if (isset($location['category_id']) && $category['id'] == $location['category_id']) {
                                            echo ' selected ';
                                        }
                                        echo '>'.$category['name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Phone:
                                </label>
                                <input type="text" name="phone" class="form-control m-input" <?php if (isset($location)) echo 'value="'.$location['phone'].'"';?>>
                            </div>
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Address:
                                </label>
                                <input type="text" name="address" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['address'].'"'?> id="location_address">
                                <input type="text" name="city" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['city'].'"'?> id="locality" hidden>
                                <input type="text" name="state" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['state'].'"'?> id="administrative_area_level_1" hidden>
                                <input type="text" name="zip" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['zip'].'"'?> id="postal_code" hidden>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Website:
                                </label>
                                <input type="text" name="website_url" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['website_url'].'"'?>>
                            </div>
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    Menu link:
                                </label>
                                <input type="text" name="menu_url" class="form-control m-input" <?php if(isset($location)) echo 'value="'.$location['menu_url'].'"'?>>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Description:
                                </label>
                                <textarea class="form-control m-input" style="min-height: 150px;" name="description"> <?php if(isset($location)) echo $location['description'];?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="m-form__seperator m-form__seperator--dashed"></div>
                    <div class="m-form__section">
                    	<div class="m-form__heading">
							<h3 class="m-form__heading-title">
								Working Hours
							</h3>
						</div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-2 m-form__group-sub"></div>
                            <div class="col-lg-3 m-form__group-sub">
                                <label class="form-control-label">
                                    Open Hour
                                </label>
                            </div>
                            <div class="col-lg-3 m-form__group-sub">
                                <label class="form-control-label">
                                    Close Hour
                                </label>
                            </div>
                            <div class="col-lg-3 m-form__group-sub">
                            </div>
                        </div>
                        <?php
                        foreach ($example_hours as $hour) {
                            ?>
                            <div class="my-div3 row" style="margin-bottom: 10px">
                                <label class="col-md-2" style="text-align: right;"><?=$weekdays[$hour['day']]?></label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input name="working_hour[<?=$hour['day']?>][open_hour]"
                                            <?php if (isset($working_hours) && array_key_exists($hour['day'], $working_hours)) echo 'value="'.$working_hours[$hour['day']]['open_hour'].'"'; 
                                            else echo 'value="'.$example_hours[$hour['day']]['open'].'"'?> type="text" class="form-control timepicker">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-clock-o"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input name="working_hour[<?=$hour['day']?>][close_hour]"
                                            <?php if (isset($working_hours) && array_key_exists($hour['day'], $working_hours)) echo 'value="'.$working_hours[$hour['day']]['close_hour'].'"'; 
                                            else echo 'value="'.$example_hours[$hour['day']]['close'].'"'?> type="text" class="form-control timepicker">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-clock-o"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-4 my-switch1">
                                    <input name="working_hour[<?=$hour['day']?>][status]" type="checkbox" data-switch="true"
                                        <?php 
                                        if (isset($working_hours) && array_key_exists($hour['day'], $working_hours) && $working_hours[$hour['day']]['status'] == OPEN) echo 'checked'; 
                                        else echo 'checked';
                                            ?>
                                           class="make-switch" data-on-text="Open&nbsp;" data-off-text="Closed&nbsp;" data-on-color="success" data-off-color="danger">
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="m-form__seperator m-form__seperator--dashed"></div>
                    <div class="m-form__section">
                        <div class="m-form__heading">
                            <h3 class="m-form__heading-title">
                                Media Files
                            </h3>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-3 m-form__group-sub">
                                <label class="form-control-label">
                                    <i class="fa fa-image"></i> Thumbnail Image
                                </label>
                                <div class="m-dropzone dropzone m-dropzone--primary" action="inc/api/dropzone/upload.php" id="thumb-dropzone">
                                    <div class="m-dropzone__msg dz-message needsclick">
                                        <h3 class="m-dropzone__msg-title">
                                            Drop thumbnail here or click to upload.
                                        </h3>
                                        <span class="m-dropzone__msg-desc">
                                            Upload only 1 image
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    <i class="fa fa-file-image-o"></i> Gallery
                                </label>
                                <div class="m-dropzone dropzone m-dropzone--info" action="inc/api/dropzone/upload.php" id="gallery-dropzone">
                                    <div class="m-dropzone__msg dz-message needsclick">
                                        <h3 class="m-dropzone__msg-title">
                                            Drop images here or click to upload.
                                        </h3>
                                        <span class="m-dropzone__msg-desc">
                                            Upload up to 5 images
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 m-form__group-sub">
                                <label class="form-control-label">
                                    <i class="fa fa-video-camera"></i> Promo Video
                                </label>
                                <div class="m-dropzone dropzone m-dropzone--success" action="inc/api/dropzone/upload.php" id="video-dropzone">
                                    <div class="m-dropzone__msg dz-message needsclick">
                                        <h3 class="m-dropzone__msg-title">
                                            Drop video here or click to upload.
                                        </h3>
                                        <span class="m-dropzone__msg-desc">
                                            Upload only 1 video
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions">
						<button type="reset" class="btn btn-primary">
							Submit
						</button>
						<button type="reset" class="btn btn-secondary">
							Cancel
						</button>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->
	</div>
</div>