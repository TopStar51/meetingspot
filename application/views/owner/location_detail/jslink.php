<script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_MAP_API_KEY ?>&libraries=places"></script>
<script type="text/javascript">

    var dzThumbnail = null;
    var dzGallery = null;
    var dzVideo = null;

	var BootstrapSwitch = function () {

        //== Private functions
        var demos = function () {
            // minimum setup
            $('[data-switch=true]').bootstrapSwitch();
        }

        return {
            // public functions
            init: function () {
                demos();
            }
        };
    }();

    var DropzoneDemo = function () {
        //== Private functions
        var demos = function () {
            
            // file type validation
            Dropzone.options.thumbDropzone = {
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 1,
                maxFilesize: 10, // MB
                addRemoveLinks: true,
                dictRemoveFile: "Remove File",
                acceptedFiles: "image/*",
                init: function() {
                    dzThumbnail = this;
                }
            };

            Dropzone.options.galleryDropzone = {
                acceptedFiles: 'image/*',
                paramName: "file", 
                maxFilesize: 10, // MB
                // autoProcessQueue: false,
                addRemoveLinks: true,
                dictRemoveFile: "Remove File",
                init: function() {
                    dzGallery = this;
                }
            };

            Dropzone.options.videoDropzone = {
                acceptedFiles: 'video/*',
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 1,
                maxFilesize: 100, // MB
                addRemoveLinks: true,
                dictRemoveFile: "Remove File",
                init: function() {
                    dzVideo = this;
                }
            };
        }

        return {
            // public functions
            init: function () {
                demos();
            }
        };
    }();

    var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        postal_code: 'short_name'
    };

    function initialize() {

        var options = {
            componentRestrictions: {country: "us"}
        };

        var input = document.getElementById('location_address');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }
            for (var component in componentForm) {
                document.getElementById(component).value = '';
            }
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {

                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        });     
        
    }
    google.maps.event.addDomListener(window, 'load', initialize);

    jQuery(document).ready(function () {
    	$('.timepicker').timepicker({
            defaultTime: '11:45 AM',
        });
        BootstrapSwitch.init();
        DropzoneDemo.init();
        //inputImage();
    });
</script>