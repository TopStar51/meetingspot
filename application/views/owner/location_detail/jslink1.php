<script>
    var dropzone_gallery;
    var dropzone_video;
    //== Class definition
    var WizardDemo = function () {
        //== Base elements
        var wizardEl = $('#m_wizard');
        var formEl = $('#m_form');
        var validator;
        var wizard;

        //== Private functions
        var initWizard = function () {
            //== Initialize form wizard
            wizard = wizardEl.mWizard({
                startStep: 1
            });

            //== Validation before going to next page
            wizard.on('beforeNext', function (wizard) {
                if (validator.form() !== true) {
                    return false;  // don't go to the next step
                }
            })

            //== Change event
            wizard.on('change', function (wizard) {
                mApp.scrollTop();
            });

            // timepicker
            $('.timepicker').timepicker({
                minuteStep: 1,
                showSeconds: false,
                showMeridian: true,
                defaultTime: '12:00 AM'
            });
            $('.timepicker').timepicker({
                minuteStep: 1,
                showSeconds: false,
                showMeridian: true,
                defaultTime: '11:59 PM'
            });
        }

        var initValidation = function () {
            validator = formEl.validate({
                //== Validate only visible fields
                ignore: ":hidden",

                //== Validation rules
                rules: {
                    //=== Location Information(step 1)
                    //== Basic Info
                    name: {
                        required: true
                    },
                    category_id: {
                        required: true
                    },
                    phone: {
                        required: true
                    },
                    website_url: {
                        required: true,
                        url: true
                    },
                    address: {
                        required: true
                    },
                    //=== Location Information(step 2)
                    //== Detailed Info
                    description: {
                        required: true
                    },
                    // 'amenity[]': {
                    //     required: true
                    // },

                    //=== Location Information(step 3)
                    //== Media Information
                    billing_card_name: {
                        required: true
                    },
                    billing_card_number: {
                        required: true,
                        creditcard: true
                    },
                    billing_card_exp_month: {
                        required: true
                    },
                    billing_card_exp_year: {
                        required: true
                    },
                    billing_card_cvv: {
                        required: true,
                        minlength: 2,
                        maxlength: 3
                    },

                    //== Billing Address
                    billing_address_1: {
                        required: true
                    },
                    billing_address_2: {},
                    billing_city: {
                        required: true
                    },
                    billing_state: {
                        required: true
                    },
                    billing_zip: {
                        required: true,
                        number: true
                    },
                    billing_delivery: {
                        required: true
                    }
                },

                //== Validation messages
                messages: {
                    'account_communication[]': {
                        required: 'You must select at least one communication option'
                    },
                    accept: {
                        required: "You must accept the Terms and Conditions agreement!"
                    }
                },

                //== Display error
                invalidHandler: function (event, validator) {
                    mApp.scrollTop();

                    swal({
                        "title": "",
                        "text": "There are some errors in your submission. Please correct them.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                },

                //== Submit valid form
                submitHandler: function (form) {

                }
            });
        }

        var initSubmit = function () {
            var btn = formEl.find('[data-wizard-action="submit"]');

            btn.on('click', function (e) {
                e.preventDefault();

                if (validator.form()) {
                    //== See: src\js\framework\base\app.js
                    mApp.progress(btn);
                    //mApp.block(formEl);

                    var formData = new FormData();
                    var form = $('#m_form')[0];
                    var fileIndex = 0;
                    for (var i =0; i < form.length; i +=1) {
                        if (form[i].name == 'amenity[]') continue;
                        if (form[i].type == 'file') {
                            //thumb & video file uploading
                            //thumbnail, video, gallery
                            // if (form[i].name == 'gallery') {
                            //     formData.append('gallery[]', form[i].files[fileIndex]);
                            // } else {
                            if (form[i].files.length > 0)
                                formData.append(form[i].name, form[i].files[fileIndex]);
                            // }
                            fileIndex += 1;
                        } else if (form[i].type == 'checkbox') {
                            if (form[i].checked) {
                                formData.append(form[i].name, 'OPEN');
                            } else {
                                formData.append(form[i].name, 'CLOSED');
                            }
                        } else {
                            formData.append(form[i].name, form[i].value);
                        }
                    }

                    //send gallery dropzone files to server
                    for (var i =0; i < dropzone_gallery.files.length; i += 1) {
                        formData.append('gallery[]', dropzone_gallery.files[i]);
                    }

                    if (dropzone_video.files.length > 0) {
                        formData.append('video', dropzone_video.files[0]);
                    }

                    $('img.img-gallery').each(function() {
                        formData.append('gallery[]', $(this).data('name'));
                    });

                    process_form_data('', '<?=site_url('owner/location/save_location')?>', formData,
                        function() {
                            //success
                            window.location.href = '<?=site_url('owner/location')?>';
                        },
                        function () {
                            alert('error occurred');
                            // console.log('error occurred');
                        }
                    );

                    return;
                    var param = new Object();
                    var gallery = new Array();
                    for (var i = 0; i < dropzone_gallery.files.length; i += 1) {
                        gallery.push(dropzone_gallery.files[i]);
                    }
                    param['gallery'] = gallery;
                    //== See: http://malsup.com/jquery/form/#ajaxSubmit
                    if (dropzone_video.files.length > 0)
                        param['video'] = dropzone_video.files[0];
                    formEl.ajaxSubmit({
                        url: '<?=site_url('/owner/location/save_location')?>',
                        method: 'post',
                        data: param,
                        datatype: 'json',
                        success: function () {
                            mApp.unprogress(btn);
                            //mApp.unblock(formEl);

                            swal({
                                "title": "",
                                "text": "The location has been successfully submitted!",
                                "type": "success",
                                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                            }).then(function(result) {
                                window.location.href = '<?=site_url('/owner/location')?>';
                            });
                        }
                    });
                }
            });
        }

        return {
            // public functions
            init: function () {
                wizardEl = $('#m_wizard');
                formEl = $('#m_form');

                initWizard();
                initValidation();
                initSubmit();
            }
        };
    }();

    var BootstrapSwitch = function () {

        //== Private functions
        var demos = function () {
            // minimum setup
            $('[data-switch=true]').bootstrapSwitch();
        }

        return {
            // public functions
            init: function () {
                demos();
            }
        };
    }();

    var DropzoneDemo = function () {
        //== Private functions
        var demos = function () {
            acceptedFiles: 'image/*',
            // single file uload
            Dropzone.options.galleryDropzone = {
                acceptedFiles: 'image/*',
                paramName: "file", // The name that will be used to transfer the file
                // maxFiles: 5,
                maxFilesize: 5, // MB
                // autoProcessQueue: false,
                addRemoveLinks: true,
                init: function() {
                    dropzone_gallery = this;
                },
                accept: function (file, done) {
                    if (file.name == "justinbieber.jpg") {
                        done("Naha, you don't.");
                    } else {
                        done();
                    }
                }
            };

            // multiple file upload
            Dropzone.options.videoDropzone = {
                acceptedFiles: 'video/*',
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 10,
                maxFilesize: 10, // MB
                addRemoveLinks: true,
                init: function() {
                    dropzone_video = this;
                },
                accept: function (file, done) {
                    if (file.name == "justinbieber.jpg") {
                        done("Naha, you don't.");
                    } else {
                        done();
                    }
                }
            };

            // file type validation
            Dropzone.options.mDropzoneThree = {
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 10,
                maxFilesize: 10, // MB
                addRemoveLinks: true,
                acceptedFiles: "image/*,application/pdf,.psd",
                accept: function (file, done) {
                    if (file.name == "justinbieber.jpg") {
                        done("Naha, you don't.");
                    } else {
                        done();
                    }
                }
            };
        }

        return {
            // public functions
            init: function () {
                demos();
            }
        };
    }();
    DropzoneDemo.init();

    jQuery(document).ready(function () {
        WizardDemo.init();
        BootstrapSwitch.init();
        inputImage();
    });
</script>
