<script>
    //== Class Definition
    var SnippetLogin = function() {

        var login = $('#m_login');

        var showErrorMsg = function(form, type, msg) {
            var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>');

            form.find('.alert').remove();
            alert.prependTo(form);
            alert.animateClass('fadeIn animated');
            alert.find('span').html(msg);
        }

        //== Private Functions

        var displaySignUpForm = function() {
            login.removeClass('m-login--forget-password');
            login.removeClass('m-login--signin');

            login.addClass('m-login--signup');
            login.find('.m-login__signup').animateClass('flipInX animated');
        }

        var displaySignInForm = function() {
            login.removeClass('m-login--forget-password');
            login.removeClass('m-login--signup');

            login.addClass('m-login--signin');
            login.find('.m-login__signin').animateClass('flipInX animated');
        }

        var displayForgetPasswordForm = function() {
            login.removeClass('m-login--signin');
            login.removeClass('m-login--signup');

            login.addClass('m-login--forget-password');
            login.find('.m-login__forget-password').animateClass('flipInX animated');
        }

        var handleFormSwitch = function() {
            $('#m_login_forget_password').click(function(e) {
                e.preventDefault();
                displayForgetPasswordForm();
            });

            $('#m_login_forget_password_cancel').click(function(e) {
                e.preventDefault();
                displaySignInForm();
            });

            $('#m_login_signup').click(function(e) {
                e.preventDefault();
                displaySignUpForm();
            });

            $('#m_login_signup_cancel').click(function(e) {
                e.preventDefault();
                displaySignInForm();
            });
        }

        var handleSignInFormSubmit = function() {
            $('#form-login').submit(function(e) {
                e.preventDefault();
                var btn = $(this).find('button');
                var form = $(this);

                // form.validate({
                //     rules: {
                //         email: {
                //             required: true,
                //             // email: true
                //         },
                //         password: {
                //             required: true
                //         }
                //     }
                // });

                // if (!form.valid()) {
                //     return;
                // }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({
                    url: '<?=site_url('/owner/login/ajax_login')?>',
                    method: 'post',
                    dataType: 'json',
                    success: function(response, status, xhr, $form) {
                        // similate 2s delay

                        if (response.state == 'success') {
                            window.location.href = '<?=site_url()?>' + response.url;
                        } else {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                            showErrorMsg(form, 'danger', response.message);
                        }
                    }
                });
            });
        }

        var handleSignUpFormSubmit = function() {
            $('#form-signup').submit(function(e) {
                alert('hey');
                e.preventDefault();

                var form = $(this);
                var btn = $(this).find('button');

                // form.validate({
                //     rules: {
                //         first_name: {
                //             required: true
                //         },
                //         last_name: {
                //             required: true
                //         },
                //         email: {
                //             required: true,
                //             email: true
                //         },
                //         phone: {
                //             required: true,
                //         },
                //         password: {
                //             required: true
                //         },
                //         rpassword: {
                //             required: true
                //         },
                //         address: {
                //             required: true
                //         },
                //         city: {
                //             required: true
                //         },
                //         state: {
                //             required: true
                //         }
                //     }
                // });

                // if (!form.valid()) {
                //     return;
                // }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({
                    url: '<?=site_url('owner/login/sign_up')?>',
                    method: 'post',
                    dataType: 'json',
                    success: function(response, status, xhr, $form) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                            if (response.status) {
                                form.clearForm();
                                form.validate().resetForm();

                                // display signup form
                                displaySignInForm();
                                var signInForm = login.find('.m-login__signin form');
                                signInForm.clearForm();
                                signInForm.validate().resetForm();

                                showErrorMsg(signInForm, 'success', 'Thank you. To complete your registration please check your email.');
                            } else {
                                showErrorMsg(signInForm, 'danger', 'Thank you. To complete your registration please check your email.');
                            }
                        }, 2000);
                    }
                });
            });
        }

        var handleForgetPasswordFormSubmit = function() {
            $('#m_login_forget_password_submit').click(function(e) {
                e.preventDefault();

                var btn = $(this);
                var form = $(this).closest('form');

                form.validate({
                    rules: {
                        email: {
                            required: true,
                            email: true
                        }
                    }
                });

                if (!form.valid()) {
                    return;
                }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({
                    url: '',
                    success: function(response, status, xhr, $form) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false); // remove
                            form.clearForm(); // clear form
                            form.validate().resetForm(); // reset validation states

                            // display signup form
                            displaySignInForm();
                            var signInForm = login.find('.m-login__signin form');
                            signInForm.clearForm();
                            signInForm.validate().resetForm();

                            showErrorMsg(signInForm, 'success', 'Cool! Password recovery instruction has been sent to your email.');
                        }, 2000);
                    }
                });
            });
        }

        //== Public Functions
        return {
            // public functions
            init: function() {
                handleFormSwitch();
                handleSignInFormSubmit();
                handleSignUpFormSubmit();
                handleForgetPasswordFormSubmit();
            }
        };
    }();

    //== Class Initialization
    jQuery(document).ready(function() {
        SnippetLogin.init();
    });
</script>