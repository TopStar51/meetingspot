<script type="text/javascript">
    function inputImage() {
        $('.img-view').click(function() {
            $(this).closest('div').find('.img-input').click();
            // $('#img-input').click();
        });

        $('.img-input').change(function () {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(input).closest('div').find('.img-view').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        });
    }

    var process_form_data = function (blockDiv, url, data, successCallback, errorCallback) {
        if (!blockDiv && blockDiv != '') $('#' + blockDiv).prop('disabled', true);
        $(document.body).css({'cursor' : 'wait'});

        var request = new XMLHttpRequest();
        request.open("POST", url);

        request.onload = function() {
            var resp;
            resp = JSON.parse(request.responseText);

            $(document.body).css({'cursor' : 'default'});
            if (!blockDiv && blockDiv != '') $('#' + blockDiv).prop('disabled', false);

            if (resp.state == 'success') {
                successCallback(resp);
            } else {
                errorCallback(resp);
            }
            return;
        };
        request.send(data);
    };

</script>