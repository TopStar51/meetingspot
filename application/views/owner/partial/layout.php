<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<head>
<script>
var pendingCount = <?=$pendingCount?>;
</script>
    <meta charset="utf-8"/>
    <title>
        <?=$title?>
    </title>
    <meta name="description" content="Accordion examples">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="<?= base_url('assets/owner') ?>/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('assets/owner') ?>/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css"/>
    <!--end::Base Styles -->
    <link rel="shortcut icon" type="image/png" href="<?= base_url('assets') ?>/owner/custom/images/favicon.png"/>
    <?php

    foreach ($this->custom_css_list as $css) {
        gf_html_load_css($css);
    }
    ?>

    <script type="text/javascript">
        var BASE_URL = "<?php echo base_url('owner') ?>";
    </script>
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
    <header class="m-grid__item    m-header " data-minimize-offset="200" data-minimize-mobile-offset="200">
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">
                <!-- BEGIN: Brand -->
                <div class="m-stack__item m-brand  m-brand--skin-dark ">
                    <div class="m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-stack__item--middle m-brand__logo">
                            <a href="<?=site_url('/owner/dashboard')?>" class="m-brand__logo-wrapper">
                                <img alt=""
                                     src="<?= base_url('assets/owner') ?>/custom/images/logo.png"/>
                            </a>
                        </div>
                        <div class="m-stack__item m-stack__item--middle m-brand__tools">
                            <!-- BEGIN: Left Aside Minimize Toggle -->
                            <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block 
					 ">
                                <span></span>
                            </a>
                            <!-- END -->
                            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                            <a href="javascript:;" id="m_aside_left_offcanvas_toggle"
                               class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>
                            <!-- END -->
                            <!-- BEGIN: Responsive Header Menu Toggler -->
                            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;"
                               class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>
                            <!-- END -->
                            <!-- BEGIN: Topbar Toggler -->
                            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;"
                               class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                <i class="flaticon-more"></i>
                            </a>
                            <!-- BEGIN: Topbar Toggler -->
                        </div>
                    </div>
                </div>
                <!-- END: Brand -->
                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                    <!-- BEGIN: Horizontal Menu -->
                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark "
                            id="m_aside_header_menu_mobile_close_btn">
                        <i class="la la-close"></i>
                    </button>
                    <!-- END: Horizontal Menu -->                                <!-- BEGIN: Topbar -->
                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" data-dropdown-toggle="click" data-dropdown-persistent="true">
                                    <a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
                                        <?php if ($pendingCount) { ?>
                                        <span class="m-nav__link-badge m-badge m-badge--dot m-badge--danger"></span>
                                        <?php } ?>
                                        <span class="m-nav__link-icon">
                                            <i class="flaticon-music-2"></i>
                                        </span>
                                    </a>
                                    <?php if ($pendingCount > 0) { ?>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center" style="background: url(<?=base_url('')?>assets/owner/app/media/img/misc/notification_bg.jpg); background-size: cover;">
                                                <span class="m-dropdown__header-title">
                                                    <?=$pendingCount?> New
                                                </span>
                                                <span class="m-dropdown__header-subtitle">
                                                    User Notifications
                                                </span>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <div class="tab-content">
                                                        <div class="m-scrollable" data-scrollable="true" data-max-height="250" data-mobile-max-height="200">
                                                            <div class="m-list-timeline m-list-timeline--skin-light">
                                                                <div class="m-list-timeline__items">
                                                                <?php foreach ($pendingFive as $pending) { ?>
                                                                    <div class="m-list-timeline__item">
                                                                        <span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
                                                                        <a href="<?=site_url('owner/'.notice_menu($pending))?>" class="m-list-timeline__text">
                                                                            <?=notice_text($pending)?>
                                                                        </a>
                                                                        <span class="m-list-timeline__time">
                                                                        </span>
                                                                    </div>
                                                                <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </li>

                                <li class="m-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large" title="go to home page">
                                    <a href="<?=site_url('home')?>" class="m-nav__link m-dropdown__toggle" target="_blank">
                                        <span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
                                        <span class="m-nav__link-icon">
                                            <i class="flaticon-layers"></i>
                                        </span>
                                    </a>
                                </li>
                                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                                    data-dropdown-toggle="click">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
										<span class="m-topbar__userpic">
                                            Hello, <?=$user_name?>
										</span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav m-nav--skin-light">
                                                        <li class="m-nav__section m--hide">
																	<span class="m-nav__section-text">
																		Section
																	</span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="<?=site_url('owner/profile')?>" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                <span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			My Profile
																		</span>
																	</span>
																</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__separator m-nav__separator--fit"></li>
                                                        <li class="m-nav__item">
                                                            <a href="<?=site_url('signin')?>"
                                                               class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                                                Logout
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- END: Topbar -->
                </div>
            </div>
        </div>
    </header>
    <!-- END: Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
            <i class="la la-close"></i>
        </button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
            <!-- BEGIN: Aside Menu -->
            <div
                    id="m_ver_menu"
                    class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
                    data-menu-vertical="true"
                    data-menu-scrollable="false" data-menu-dropdown-timeout="500"
            >
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                    <?php
                    foreach ($menus as $menu) {
                        $has_submenu = array_key_exists('submenu', $menu);
                        ?>
                        <li class="m-menu__item 
                            <?php if($has_submenu) echo 'm-menu__item--submenu'; ?>
                            <?php
                            if ($menu['path'] == $path[1]) { 
                                echo ' m-menu__item--active';
                                if ($has_submenu) {
                                    echo ' m-menu__item--open m-menu__item--expanded';
                                }
                            }?>"
                        >
                            <a href="<?= site_url('/owner/' . $menu['path']) ?>" class="m-menu__link <?php if ($has_submenu) echo 'm-menu__toggle';?>">
                                <i class="m-menu__link-icon <?= $menu['icon'] ?>"></i>
                                <span class="m-menu__link-text">
                                    <?= $menu['text'] ?>
                                </span>
                                <?php if ($has_submenu) { ?>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                                <?php } ?>
                            </a>
                            <?php if ($has_submenu) { ?>
                            <div class="m-menu__submenu" style="">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                <?php foreach ($menu['submenu'] as $submenu) { ?>
                                    <li class="m-menu__item <?php if ($submenu['path'] == $this->path[2]) echo 'm-menu__item--active'; ?>" aria-haspopup="true">
                                        <a href="<?=site_url('/owner/'.$menu['path'].'/'.$submenu['path'])?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">
                                                <?=$submenu['text']?>
                                            </span>
                                        </a>
                                    </li>
                                <?php } ?>
                                </ul>
                            </div>
                            <?php } ?>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <!-- END: Aside Menu -->
        </div>
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title ">
                            <?=$title?>
                        </h3>
                    </div>
                </div>
            </div>
            <!-- END: Subheader -->
            <div class="m-content">
                <?php
                foreach ($this->view_list as $view) {
                    $view_page = $view[0];
                    $view_param = $view[1];
                    $this->load->view($view_page, $view_param);
                }
                ?>
            </div>
        </div>
    </div>
    <!-- end:: Body -->
</div>
<!-- end:: Page -->
<!-- begin::Scroll Top -->
<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500"
     data-scroll-speed="300">
    <i class="la la-arrow-up"></i>
</div>
<!-- end::Scroll Top -->
<!--begin::Base Scripts -->
<script src="<?= base_url('assets/owner') ?>/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="<?= base_url('assets/owner') ?>/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<!--end::Base Scripts -->
<?php
foreach ($this->custom_js_list as $js) {
    gf_html_load_js($js);
}
$this->load->view('owner/partial/global-js');
$this->load->view('owner/'.$jslink);
?>
</body>
<!-- end::Body -->
</html>
