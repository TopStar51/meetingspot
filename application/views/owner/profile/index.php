<div class="m-portlet">
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1" novalidate="novalidate" action="#" onsubmit="javascript:;">
        <div class="m-portlet__body">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">
                    <div class="m-form__section m-form__section--first">
                        <div class="m-form__heading">
                            <h3 class="m-form__heading-title">
                                Personal Info
                            </h3>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * First Name:
                                </label>
                                <input type="text" name="first_name" class="form-control m-input" value="<?=$owner['first_name']?>">
                            </div>
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Last Name:
                                </label>
                                <input type="text" name="last_name" class="form-control m-input" value="<?=$owner['last_name']?>">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Email:
                                </label>
                                <input type="text" name="email" class="form-control m-input" value="<?=$owner['email']?>">
                            </div>
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Phone:
                                </label>
                                <input type="text" name="phone" class="form-control m-input" value="<?=$owner['phone']?>">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Username:
                                </label>
                                <input type="text" name="username" class="form-control m-input" value="<?=$owner['username']?>">
                            </div>
                            <div class="col-lg-3 m-form__group-sub">
                                <label class="form-control-label">
                                    website:
                                </label>
                                <input type="text" name="website" class="form-control m-input" value="<?=$owner['website']?>">
                            </div>
                            <div class="col-lg-3 m-form__group-sub">
                                <label class="form-control-label">
                                    facebook:
                                </label>
                                <input type="text" name="facebook" class="form-control m-input" value="<?=$owner['facebook']?>">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Password:
                                </label>
                                <input type="password" name="password" class="form-control m-input">
                            </div>
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Confirm Password:
                                </label>
                                <input type="password" name="confirm_password" class="form-control m-input">
                            </div>
                        </div>
                    </div>
                    <div class="m-form__section">
                        <div class="m-form__heading">
                            <h3 class="m-form__heading-title">
                                Address
                            </h3>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Address:
                                </label>
                                <input type="text" name="address" class="form-control m-input" value="<?=$owner['address']?>">
                            </div>
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * City:
                                </label>
                                <input type="text" name="city" class="form-control m-input" value="<?=$owner['city']?>">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * State:
                                </label>
                                <input type="text" name="state" class="form-control m-input" value="<?=$owner['state']?>">
                            </div>
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="form-control-label">
                                    * Zip code:
                                </label>
                                <input type="text" name="zip" class="form-control m-input" value="<?=$owner['zip']?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions">
                <div class="row">
                    <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" class="btn btn-success">
                            Confirm
                        </button>
                        <!-- <button type="reset" class="btn btn-secondary">
                            Reset
                        </button> -->
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>