<script>
    var FormValidation = function () {

        // basic validation
        var handleValidation1 = function() {
            // for more info visit the official plugin documentation:
            // http://docs.jquery.com/Plugins/Validation
            $( "#m_form_1" ).validate({
                // define validation rules
                rules: {
                    first_name: {
                        minlength: 2,
                        required: true
                    },
                    last_name: {
                        minlength: 2,
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    username: {
                        minlength: 2,
                        required: true
                    },
                    phone: {
                        required: true
                    },
                    confirm_password: {
                        equalTo: '[name="password"]'
                    },
                    address: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    state: {
                        required: true
                    },
                    zip: {
                        required: true,
                        number: true
                    }
                },

                //display error alert on form submit
                invalidHandler: function(event, validator) {
                    var alert = $('#m_form_1_msg');
                    alert.removeClass('m--hide').show();
                    mApp.scrollTo(alert, -200);
                },

                submitHandler: function (form) {
                    // form[0].submit(); // submit the form
                    $(form).ajaxSubmit({
                        url: '<?=site_url('/owner/profile/ajax_save')?>',
                        method: 'post',
                        dataType: 'json',
                        success: function (resp) {
                            if (resp.status == 'success') {
                                swal({
                                    "title": "",
                                    "text": "Your profile has been successfully updated!",
                                    "type": "success",
                                    "confirmButtonClass": "btn btn-success m-btn m-btn--wide"
                                }).then((result) => {
                                    if(result.value) {
                                        window.location.reload();
                                    }
                                });
                                
                            } else {
                                swal({
                                    "title": "",
                                    "text": "Email Duplicated. Please correct your email.",
                                    "type": "error",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                            }
                        },
                        error: function (resp) {
                            if (resp.status == 'success') {
                                swal({
                                    "title": "",
                                    "text": "Your profile has been successfully submitted!",
                                    "type": "success",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                            } else {
                                swal({
                                    "title": "",
                                    "text": "Email Duplicated. Please correct your email.",
                                    "type": "error",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                            }
                        }
                    });
                }
            });
        }

        return {
            //main function to initiate the module
            init: function () {

                handleValidation1();

            }

        };

    }();

    jQuery(document).ready(function() {
        FormValidation.init();
    });

</script>