<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:36 AM
 */
?>
<script>
    //== Class definition
    var datatable;

    var DatatableRemoteAjaxDemo = function () {
        //== Private functions

        // basic demo
        var demo = function () {

            datatable = $('.m_datatable').mDatatable({
                // datasource definition
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            // sample GET method
                            method: 'GET',
                            url: '<?=site_url('/owner/reservation/ajax_table')?>',
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            },
                        },
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                },

                // layout definition
                layout: {
                    scroll: false,
                    footer: false
                },

                // column sorting
                sortable: true,

                pagination: true,

                toolbar: {
                    // toolbar items
                    items: {
                        // pagination
                        pagination: {
                            // page size select
                            pageSizeSelect: [10, 20, 30, 50, 100],
                        },
                    },
                },

                search: {
                    input: $('#generalSearch'),
                },

                // columns definition
                columns: [/*
                    {
                        field: 'pos',
                        title: '#',
                        sortable: false, // disable sort for this column
                        width: 40,
                        selector: false,
                        textAlign: 'center',
                    },*/ {
                        field: 'res_number',
                        title: 'ID',
                        width: 50
                    }, {
                        field: 'customer',
                        title: 'Customer',
                    }, {
                        field: 'room',
                        title: 'Room',
                    }, {
                        field: 'location',
                        title: 'Location',
                        width: 200
                    }, {
                        field: 'guest_number',
                        title: 'Guests Number',
                    }, {
                        field: 'res_date',
                        title: 'Reservation Date',
                    }, {
                        field: 'start_time',
                        title: 'Start Time',
                        type: 'time',
                        format: 'HH:MM:SS',
                    }, {
                        field: 'end_time',
                        title: 'End Time',
                        type: 'time',
                        format: 'HH:MM:SS',
                    }, {
                        field: 'status',
                        title: 'Status',
                        template: function(row) {
                            var status = {
                                0: {'title':'Pending', 'class': 'm-badge--brand'},
                                1: {'title':'Confirmed', 'class': 'm-badge--success'},
                                2: {'title':'Cancelled', 'class': 'm-badge--warning'},
                            };
                            return '<span class="m-badge '+ status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
                        },
                    }, {
                        field: 'Actions',
                        width: 100,
                        title: 'Actions',
                        sortable: false,
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var html = '';
                            if (row.status == '0') {
                                html +='<button type="button" onclick="on_approve(' + row.id + ')" class="btn btn-success btn-sm" style="margin: 5px;">Approve</button>';
                                html += '<button type="button" onclick="on_reject(' + row.id + ')" class="btn btn-danger btn-sm" style="margin: 5px;">Reject</button>';
                            }
                            html +=' <button type="button" onclick="on_detail(' + row.id + ')" class="btn btn-info btn-sm" style="margin: 5px;">Detail</button>';
                            return html;
                        },
                    }],
            });

            $('#m_form_status').on('change', function () {
                datatable.search($(this).val().toLowerCase(), 'Status');
            });

            $('#m_form_type').on('change', function () {
                datatable.search($(this).val().toLowerCase(), 'Type');
            });

            $('#m_form_status, #m_form_type').selectpicker();

        };

        return {
            // public functions
            init: function () {
                demo();
            },
        };
    }();

    function on_detail(id) {
        window.location.href = '<?=site_url('/owner/reservation/detail/')?>' + id;
    }

    function on_approve(id) {
        $.ajax({
            url: '<?=site_url('owner/reservation/ajax_approve')?>',
            method: 'post',
            datatype: 'json',
            data: {
                id: id
            },
            success: function (resp) {
                datatable.reload();
            }
        })
    }

    function on_reject(id) {
        $.post({
            url: '<?=site_url('owner/reservation/ajax_reject')?>',
            dataType: 'json',
            data: {
                id: id
            },
            success: function (response) {
                if (response.status) {
                    datatable.reload();
                }
            }
        });
    }

    jQuery(document).ready(function () {
        DatatableRemoteAjaxDemo.init();
        $('#select-status').change(function() {
            datatable.search(this.value, 'status');
        })
    });
</script>
