<style type="text/css">
    .form-group label {
        font-weight: bold;
    }
</style>
<div class="row">
    <div class="col-lg-4">
        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Reservation
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group m-form__group m-form__group--sm">
                    <label class="col-md-12 col-form-label">
                        Reservation ID
                    </label>
                    <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['res_number']?>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm">
                    <label class="col-md-12 col-form-label">
                        Guest Number
                    </label>
                    <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['guest_number']?>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm">
                    <label class="col-md-12 col-form-label">
                        Room Name
                    </label>
                    <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['room_name']?>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm">
                    <label class="col-md-12 col-form-label">
                        Reservation Date
                    </label>
                    <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=date_format(date_create($reservation_data['start_time']), 'Y-m-d')?>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm">
                    <label class="col-md-12 col-form-label">
                        Reservation Time
                    </label>
                    <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['start_time']?> - <?=$reservation_data['end_time']?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Portlet-->
    </div>
    <div class="col-lg-4">
        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Customer
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group m-form__group m-form__group--sm">
                    <label class="col-md-12 col-form-label">
                        Name
                    </label>
                    <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['customer']?>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm">
                    <label class="col-md-12 col-form-label">
                        Email
                    </label>
                    <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['email']?>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm">
                    <label class="col-md-12 col-form-label">
                        Telephone
                    </label>
                    <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['phone']?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Portlet-->
    </div>
    <div class="col-lg-4">
        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="form-group m-form__group m-form__group--sm">
                    <label class="col-md-12 col-form-label">
                        Date Added
                    </label>
                    <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['created_at']?>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm">
                    <label class="col-md-12 col-form-label">
                        Date Modified
                    </label>
                    <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['updated_at']?>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm">
                    <label class="col-md-12 col-form-label">
                        Customer Notified
                    </label>
                    <div class="col-md-12">
                        <span class="m-form__control-static">
                            #20011
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Portlet-->
    </div>
    <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--mobile m-portlet--bordered-semi">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Location Details
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="form-group m-form__group m-form__group--sm col-md-3">
                                <label class="col-md-12 col-form-label">
                                    Name
                                </label>
                                <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['location']?>
                        </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group m-form__group--sm col-md-3">
                                <label class="col-md-12 col-form-label">
                                    Owner
                                </label>
                                <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['owner']?>
                        </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group m-form__group--sm col-md-3">
                                <label class="col-md-12 col-form-label">
                                    Category
                                </label>
                                <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['category']?>
                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group m-form__group m-form__group--sm col-md-4">
                                <label class="col-md-12 col-form-label">
                                    Address
                                </label>
                                <div class="col-md-12">
                        <span class="m-form__control-static">
                            <?=$reservation_data['address']?>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Portlet-->
    </div>
</div>