<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:36 AM
 */
?>
<script>
    //== Class definition

    var DatatableRemoteAjaxDemo = function () {
        //== Private functions

        // basic demo
        var demo = function () {

            var datatable = $('.m_datatable').mDatatable({
                // datasource definition
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            // sample GET method
                            method: 'GET',
                            url: '<?=site_url('/owner/review/ajax_table')?>',
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            },
                        },
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                },

                // layout definition
                layout: {
                    scroll: false,
                    footer: false
                },

                // column sorting
                sortable: true,

                pagination: true,

                toolbar: {
                    // toolbar items
                    items: {
                        // pagination
                        pagination: {
                            // page size select
                            pageSizeSelect: [10, 20, 30, 50, 100],
                        },
                    },
                },

                search: {
                    input: $('#generalSearch'),
                },

                // columns definition
                columns: [
                    {
                        field: 'pos',
                        title: '#',
                        sortable: false, // disable sort for this column
                        width: 40,
                        selector: false,
                        textAlign: 'center',
                    }, {
                        field: 'res_number',
                        title: 'Reservation ID',
                    }, {
                        field: 'author',
                        title: 'Author',
                    }, {
                        field: 'location',
                        title: 'Location',
                    }, {
                        field: 'rating',
                        title: 'Rating',
                    }, {
                        field: 'Status',
                        title: 'Status',
                        // callback function support for column rendering
                        template: function (row) {
                            var status = {
                                '0': {'title': 'Pending', 'class': 'm-badge--brand'},
                                '1': {'title': 'Approved', 'class': ' m-badge--success'},
                            };
                            return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
                        },
                    }, {
                        field: 'Actions',
                        width: 160,
                        title: 'Actions',
                        sortable: false,
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                            return '\
                            <button type="button" onclick="on_detail(' + row.id + ')" class="btn btn-info">Detail</button>\
                            ';
                        },
                    }],
            });

            $('#m_form_status').on('change', function () {
                datatable.search($(this).val().toLowerCase(), 'Status');
            });

            $('#m_form_type').on('change', function () {
                datatable.search($(this).val().toLowerCase(), 'Type');
            });

            $('#m_form_status, #m_form_type').selectpicker();

        };

        return {
            // public functions
            init: function () {
                demo();
            },
        };
    }();

    function on_detail(id) {
        window.location.href = '<?=site_url('/owner/review/detail/')?>' + id;
    }

    jQuery(document).ready(function () {
        DatatableRemoteAjaxDemo.init();
    });
</script>
