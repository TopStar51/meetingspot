<div class="row">
    <div class="col-lg-8">
        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="form-group m-form__group m-form__group--sm row">
                    <label class="col-xl-4 col-lg-4 col-form-label">
                        Location
                    </label>
                    <div class="col-xl-8 col-lg-8">
                        <span class="m-form__control-static">
                            <?= $review_data['location'] ?>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm row">
                    <label class="col-xl-4 col-lg-4 col-form-label">
                        Reservation ID
                    </label>
                    <div class="col-xl-8 col-lg-8">
                        <span class="m-form__control-static">
                            <?= $review_data['res_number'] ?>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm row">
                    <label class="col-xl-4 col-lg-4 col-form-label">
                        Author
                    </label>
                    <div class="col-xl-8 col-lg-8">
                        <span class="m-form__control-static">
                            <?= $review_data['author'] ?>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm row">
                    <label class="col-xl-4 col-lg-4 col-form-label">
                        Rating
                    </label>
                    <div class="col-xl-8 col-lg-8">
                        <span class="m-form__control-static" style="color: #ffb822;">
                            <?php
                            for ($i = 0; $i < $review_data['rating']; $i ++)
                                echo '<i class="fa fa-star"></i> ';
                            for (; $i < 5; $i ++)
                                echo '<i class="fa fa-star-o"></i> ';
                            ?>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm row">
                    <label class="col-xl-4 col-lg-4 col-form-label">
                        Comment
                    </label>
                    <div class="col-xl-8 col-lg-8">
                        <textarea class="form-control m-input"
                                  name="description" rows="10"><?= $review_data['comment'] ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group m-form__group--sm row">
                    <label class="col-xl-4 col-lg-4 col-form-label">
                        Review status
                    </label>
                    <div class="col-xl-8 col-lg-8">
                        <span class="m-form__control-static">
                            <?php
                            $status = array(
                                'PENDING' => array('title' => 'Pending', 'class' => 'm-badge--brand'),
                                'APPROVED' => array('title' => 'Approved', 'class' => ' m-badge--success'),
                            );
                            ?>
                            <span class="m-badge <?= $status[$review_data['status']]['class'] ?> m-badge--wide"><?= $status[$review_data['status']]['title'] ?></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Portlet-->
    </div>
</div>