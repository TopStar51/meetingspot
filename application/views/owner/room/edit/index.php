<div class="row">
    <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="m-portlet">
            <!--begin::Form-->
            <form class="m-form" id="form-room">
                <div class="m-portlet__body">
                    <div class="m-form__section m-form__section--first">
                        <div class="m-form__heading">
                            <h3 class="m-form__heading-title">
                                Basic Info
                            </h3>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 m-form__group-sub">
                                <label class="form-control-label">
                                    * Room Name:
                                </label>
                                <input type="text"
                                       name="id" <?php if (isset($room)) echo 'value="' . $room['id'] . '"' ?>
                                       class="form-control m-input" hidden>
                                <input type="text" name="name"
                                       <?php if (isset($room)) echo 'value="' . $room['name'] . '"' ?>class="form-control m-input" required>
                            </div>
                            <div class="col-lg-4 m-form__group-sub">
                                <label class="form-control-label">
                                    * Capacity:
                                </label>
                                <input type="text" name="capacity"
                                       <?php if (isset($room)) echo 'value="' . $room['capacity'] . '"' ?>class="form-control m-input" required>
                            </div>
                            <div class="col-lg-4 m-form__group-sub">
                                <label class="form-control-label">
                                    * Location:
                                </label>
                                <select class="form-control m-input selectpicker" name="location_id" required>
                                    <option value="">
                                        Select your location
                                    </option>
                                    <?php
                                    foreach ($locations as $location) {
                                        echo '<option value="' . $location['id'] . '"';
                                        if (isset($room['location_id']) && $location['id'] == $room['location_id']) {
                                            echo ' selected ';
                                        }
                                        echo '>' . $location['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="m-form__heading">
                            <h3 class="m-form__heading-title">
                                Physical Aspects
                            </h3>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-8 m-form__group-sub">
                                <div class="m-checkbox-inline">
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--brand">
                                        <input type="checkbox" name="has_window" value="1" <?=isset($room)&&$room['has_window']?'checked':''?>>
                                        Windows
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--solid  m-checkbox--brand">
                                        <input type="checkbox" name="has_seclude_door" value="1" <?=isset($room)&&$room['has_seclude_door']?'checked':''?>>
                                        Secluding Door
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--solid  m-checkbox--brand">
                                        <input type="checkbox" name="has_lighting_option" value="1" <?=isset($room)&&$room['has_lighting_option']?'checked':''?>>
                                        Lighting Option
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--brand">
                                        <input type="checkbox" name="cloth_on_table" value="1" <?=isset($room)&&$room['cloth_on_table']?'checked':''?>>
                                        Cloth On Table
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-2 m-form__group-sub">
                                <label class="form-control-label">
                                    * Wall Type:
                                </label>
                                <select class="form-control m-input selectpicker" name="wall_type" required>
                                    <option value="">
                                        Select
                                    </option>
                                    <?php
                                    foreach ($wall_types as $wall_type) {
                                        echo '<option value="' . $wall_type['id'] . '"';
                                        if (isset($room['wall_type']) && $wall_type['id'] == $room['wall_type']) {
                                            echo ' selected ';
                                        }
                                        echo '>' . $wall_type['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-2 m-form__group-sub">
                                <label class="form-control-label">
                                    * Table Format:
                                </label>
                                <select class="form-control m-input selectpicker" name="table_format" required>
                                    <option value="">
                                        Select
                                    </option>
                                    <?php
                                    foreach ($table_formats as $table_format) {
                                        echo '<option value="' . $table_format['id'] . '"';
                                        if (isset($room['table_format']) && $table_format['id'] == $room['table_format']) {
                                            echo ' selected ';
                                        }
                                        echo '>' . $table_format['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-2 m-form__group-sub">
                                <label class="form-control-label">
                                    * Table Count:
                                </label>
                                <input type="number" name="table_count"
                                       <?php if (isset($room)) echo 'value="' . $room['table_count'] . '"' ?>class="form-control m-input" required>
                            </div>
                            <div class="col-lg-3 m-form__group-sub">
                                <label class="form-control-label">
                                    * Table Length(in):
                                </label>
                                <input type="number" name="table_length"
                                       <?php if (isset($room)) echo 'value="' . $room['table_length'] . '"' ?>class="form-control m-input" required>
                            </div>
                            <div class="col-lg-3 m-form__group-sub">
                                <label class="form-control-label">
                                    * Table Width(in):
                                </label>
                                <input type="number" name="table_width"
                                       <?php if (isset($room)) echo 'value="' . $room['table_width'] . '"' ?>class="form-control m-input" required>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-2 m-form__group-sub">
                                <label class="form-control-label">
                                    * Table Type:
                                </label>
                                <select class="form-control m-input selectpicker" name="table_type" required>
                                    <option value="">
                                        Select
                                    </option>
                                    <?php
                                    foreach ($table_types as $table_type) {
                                        echo '<option value="' . $table_type['id'] . '"';
                                        if (isset($room['table_type']) && $table_type['id'] == $room['table_type']) {
                                            echo ' selected ';
                                        }
                                        echo '>' . $table_type['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-2 m-form__group-sub">
                                <label class="form-control-label">
                                    * Chairs Per Table:
                                </label>
                                <input type="number" name="chairs_per_table"
                                       <?php if (isset($room)) echo 'value="' . $room['chairs_per_table'] . '"' ?>class="form-control m-input" required>
                            </div>
                        </div><br>
                        <div class="m-form__heading">
                            <h3 class="m-form__heading-title">
                                Electronics
                            </h3>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-12 m-form__group-sub">
                                <div class="m-checkbox-inline">                                    
                                    <label class="m-checkbox m-checkbox--solid  m-checkbox--brand">
                                        <input type="checkbox" name="projection_provided" value="1" <?=isset($room)&&$room['projection_provided']?'checked':''?>>
                                        Projection
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--solid  m-checkbox--brand">
                                        <input type="checkbox" name="wifi_access" value="1" <?=isset($room)&&$room['wifi_access']?'checked':''?>>
                                        Wifi Access
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--brand">
                                        <input type="checkbox" name="podium_provided" value="1" <?=isset($room)&&$room['podium_provided']?'checked':''?>>
                                        Podium
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--solid  m-checkbox--brand">
                                        <input type="checkbox" name="has_speaker" value="1" <?=isset($room)&&$room['has_speaker']?'checked':''?>>
                                        Speakers
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--solid  m-checkbox--brand">
                                        <input type="checkbox" name="has_microphone" value="1" <?=isset($room)&&$room['has_microphone']?'checked':''?>>
                                        Microphone
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-2 m-form__group-sub">
                                <label class="form-control-label">
                                    * Upload Speed(Mbps):
                                </label>
                                <input type="number" name="upload_speed" min="0"
                                       <?php if (isset($room)) echo 'value="' . $room['upload_speed'] . '"' ?>class="form-control m-input" required>
                            </div>
                            <div class="col-lg-2 m-form__group-sub">
                                <label class="form-control-label">
                                    * Download Speed(Mbps):
                                </label>
                                <input type="number" name="download_speed" min="0"
                                       <?php if (isset($room)) echo 'value="' . $room['download_speed'] . '"' ?>class="form-control m-input" required>
                            </div>
                            <div class="col-lg-2 m-form__group-sub">
                                <label class="form-control-label">
                                    * Electric Outlet Count :
                                </label>
                                <input type="number" name="electric_outlet_count" min="0"
                                       <?php if (isset($room)) echo 'value="' . $room['electric_outlet_count'] . '"' ?>class="form-control m-input" required>
                            </div>
                            <div class="col-lg-2 m-form__group-sub">
                                <label class="form-control-label">
                                    * Outlet Extensions:
                                </label>
                                <input type="number" name="outlet_extensions" min="0"
                                       <?php if (isset($room)) echo 'value="' . $room['outlet_extensions'] . '"' ?>class="form-control m-input" required>
                            </div>
                        </div>
                        <br>
                        <div class="m-form__heading">
                            <h3 class="m-form__heading-title">
                                Services
                            </h3>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-3 m-form__group-sub">
                                <label class="form-control-label">
                                    * Food Served:
                                </label>
                                <select class="form-control m-input selectpicker" name="food_served" required>
                                    <option value="">
                                        Select
                                    </option>
                                    <?php
                                    foreach ($food_serves as $food_served) {
                                        echo '<option value="' . $food_served['id'] . '"';
                                        if (isset($room['food_served']) && $food_served['id'] == $room['food_served']) {
                                            echo ' selected ';
                                        }
                                        echo '>' . $food_served['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="m-form__seperator m-form__seperator--dashed"></div>
                        <div class="m-form__section">
                            <div class="m-form__heading">
                                <h3 class="m-form__heading-title">
                                    Media Files
                                </h3>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4 m-form__group-sub">
                                    <label class="form-control-label">
                                        <i class="fa fa-image"></i> Thumbnail Image
                                    </label>
                                    <div class="m-dropzone dropzone m-dropzone--primary"
                                         action="inc/api/dropzone/upload.php" id="thumb-dropzone">
                                        <div class="m-dropzone__msg dz-message needsclick">
                                            <h3 class="m-dropzone__msg-title">
                                                Drop thumbnail here or click to upload.
                                            </h3>
                                            <span class="m-dropzone__msg-desc">
                                                Upload only 1 image
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 m-form__group-sub">
                                    <label class="form-control-label">
                                        <i class="fa fa-file-image-o"></i> Gallery
                                    </label>
                                    <div class="m-dropzone dropzone m-dropzone--info" action="inc/api/dropzone/upload.php"
                                         id="gallery-dropzone">
                                        <div class="m-dropzone__msg dz-message needsclick">
                                            <h3 class="m-dropzone__msg-title">
                                                Drop images here or click to upload.
                                            </h3>
                                            <span class="m-dropzone__msg-desc">
                                                Upload up to 5 images
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                        <button type="button" class="btn btn-secondary" onclick="window.location.href='<?=site_url('owner/room')?>'">
                            Cancel
                        </button>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->
    </div>
</div>