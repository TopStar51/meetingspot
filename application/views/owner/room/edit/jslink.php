<script type="text/javascript">

	var BootstrapSwitch = function () {

        //== Private functions
        var demos = function () {
            // minimum setup
            $('[data-switch=true]').bootstrapSwitch();
        }

        return {
            // public functions
            init: function () {
                demos();
            }
        };
    }();

    jQuery(document).ready(function () {

        <?php if(empty($locations)) { ?>
            swal(
                '',
                'You must have at least one active location to create rooms',
                'warning'
            ).then((value)=>{
                window.location.href = '<?=site_url('owner/location/new_location')?>';
            });
            return;
        <?php } ?>
        
    	$('.timepicker').timepicker({
            defaultTime: '11:45 AM',
        });
        BootstrapSwitch.init();

        $('#form-room').submit(function(e) {
            e.preventDefault();

            var formData = new FormData();
            var form = this;
            for (var i = 0; i < form.length; i += 1) {
                if (form[i].type == 'checkbox') {
                    formData.append(form[i].name, form[i].checked);
                } else {
                    formData.append(form[i].name, form[i].value);
                }
            }

            process_form_data(
                $(this),
                '<?=site_url('owner/room/ajax_save')?>',
                formData,
                function(resp) {
                    window.location.href = '<?=site_url('owner/room')?>';
                },
                function (err) {
                    alert('error occurred');
                }
            );
        });
    });
</script>