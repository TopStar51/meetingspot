<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2/21/2019
 * Time: 11:36 AM
 */
?>
<script>
    //== Class definition

    var datatable;

    var DatatableRemoteAjaxDemo = function () {
        //== Private functions

        // basic demo
        var demo = function () {

            datatable = $('.m_datatable').mDatatable({
                // datasource definition
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            // sample GET method
                            method: 'GET',
                            url: '<?=site_url('/owner/room/ajax_table')?>',
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            },
                        },
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                },

                // layout definition
                layout: {
                    scroll: false,
                    footer: false
                },

                // column sorting
                sortable: true,

                pagination: true,

                toolbar: {
                    // toolbar items
                    items: {
                        // pagination
                        pagination: {
                            // page size select
                            pageSizeSelect: [10, 20, 30, 50, 100],
                        },
                    },
                },

                search: {
                    input: $('#generalSearch'),
                },

                // columns definition
                columns: [
                    {
                        field: 'pos',
                        title: '#',
                        sortable: false, // disable sort for this column
                        width: 40,
                        selector: false,
                        textAlign: 'center',
                    }, {
                        field: 'name',
                        title: 'Name',
                    }, {
                        field: 'capacity',
                        title: 'Capacity'
                    }, {
                        field: 'location',
                        title: 'Location',
                    }, {
                        field: 'created_at',
                        title: 'Date Registered',
                        type: 'date',
                        format: 'YYYY-MM-DD HH:MM:SS',
                    }, {
                        field: 'Actions',
                        width: 160,
                        title: 'Actions',
                        sortable: false,
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                            return '\
                            <a href="<?=site_url('owner/room/edit/')?>' + row.id + '" class="btn btn-info" >Edit</a>\
                            <button type="button" class="btn btn-danger" onclick="on_delete('+ row.id +')">Delete</button>\
                            ';
                        },
                    }],
            });

            $('#m_form_status').on('change', function () {
                datatable.search($(this).val().toLowerCase(), 'Status');
            });

            $('#m_form_type').on('change', function () {
                datatable.search($(this).val().toLowerCase(), 'Type');
            });

            $('#m_form_status, #m_form_type').selectpicker();

        };

        return {
            // public functions
            init: function () {
                demo();
            },
        };
    }();

    function on_edit(id) {
        window.location.href = '<?=site_url('/owner/location/edit/')?>' + id;
    }

    function on_delete(id) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then(function(result){
            if (result.value) {
                $.ajax({
                    url: '<?=site_url('/owner/room/ajax_delete')?>',
                    method: 'post',
                    datatype: 'json',
                    data: {
                        id: id
                    },
                    success: function (resp) {
                        swal(
                            'Deleted!',
                            'Your room has been deleted.',
                            'success'
                        );
                        datatable.reload();
                    }
                });
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
            } else if (result.dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your room is safe :)',
                    'error'
                )
            }
        });
    }

    jQuery(document).ready(function () {
        DatatableRemoteAjaxDemo.init();
    });
</script>
